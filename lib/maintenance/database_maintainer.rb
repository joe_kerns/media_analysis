#$LOAD_PATH << File.dirname(__FILE__) unless $LOAD_PATH.include?(File.dirname(__FILE__))
class DatabaseMaintainer
	include AuthorizationsHelper
	
	def initialize(batch_size = 400)
		@batch_size = batch_size
	end 

	def clean_urls
		total_changed = 0
		if $redis.get("clean_urls_batch_start")
			batch_start = $redis.get("clean_urls_batch_start").to_i
		else 	
			batch_start = 0
		end 
		
		Article.find_each(batch_size: @batch_size, start: batch_start) do |article|
			original_url = article.url
			new_url = Article.clean_url(original_url) if original_url
			if original_url != new_url
				Rails.logger.info("Original: #{original_url}")
				Rails.logger.info("Cleaned: #{new_url}")
				article.update_attributes(url: new_url)
				total_changed += 1
			end 
		end 
		$redis.set("clean_urls_batch_start", Article.last.id)
		Rails.logger.info("Total urls cleaned: #{total_changed}")
	end 

    def delete_redis_keys(key_string)
    	$redis.keys.each {|key| $redis.del(key) if key.include?(key_string)}
    end
	
	def destroy_duplicate_articles
		total_articles_deleted = 0 
		if $redis.get("destroy_duplicate_articles_batch_start")
			batch_start = $redis.get("destroy_duplicate_articles_batch_start").to_i
		else 	
			batch_start = 0
		end 
		Article.find_each(batch_size: @batch_size, start: batch_start) do |article| 
			url = article.url 
			articles = Article.where(url: url)
			dupes = articles.size
			if dupes > 1
				master = articles[0]
				articles_to_delete = articles[1..-1]
				Rails.logger.info("#{master.id} kept")
				articles_to_delete.each do |art_to_delete|
					art_to_delete.delete_self_in_favor_of_duplicate_article(master)
					total_articles_deleted += 1
					Rails.logger.info("#{art_to_delete.id} Deleted")
				end 
			end 
		end 
		$redis.set("destroy_duplicate_articles_batch_start", Article.last.id)
		Rails.logger.info("TOTAL: #{total_articles_deleted}")
	end 

	def destroy_duplicate_logs
		if $redis.get("destroy_duplicate_logs_batch_start")
			batch_start = $redis.get("destroy_duplicate_logs_batch_start").to_i
		else 	
			batch_start = 0
		end 
		total_reading_logs_deleted = 0
		User.find_each do |user|
			user.reading_logs.find_each do |log|
				logs = user.reading_logs.where(article_id: log.article_id)
				if logs.size > 1 
					logs[1..-1].each do |log_to_delete|
						Rails.logger.info("#{user.first_name} had a duplicate log deleted")
						total_reading_logs_deleted += 1
						log_to_delete.destroy
					end 
				end 
				
			end 
		end 
		$redis.set("destroy_duplicate_articles_batch_start", User.last.id)
		Rails.logger.info("TOTAL: #{total_reading_logs_deleted}")
	end 

	def delete_non_articles
		Article.domains_to_ignore.each do |domain|
			articles = Article.where("url like ?", "%#{domain}%")
			delete_articles(articles)
		end 
	end  

	def delete_articles(articles)
		articles.each do |article|
			article.destroy
		end 
	end 

	def fix_reading_logs_counter 
		if $redis.get("article_reading_logs_counter_batch_start")
			batch_start = $redis.get("article_reading_logs_counter_batch_start").to_i
		else 	
			batch_start = 0
		end 
		
		Article.find_each(batch_size: @batch_size, start: batch_start) do |article| 
			original_count = article.reading_logs.size
			proper_count = article.reading_logs.length
			if original_count != proper_count
				article.update_attributes(reading_logs_count: proper_count)
				Rails.logger.info("ReadingLogsCounter changed from #{original_count} to #{proper_count}")
			end 
		end 
		$redis.set("article_reading_logs_counter_batch_start", Article.last.id)

		User.find_each do |user|
			original_count = user.reading_logs.size
			proper_count = user.reading_logs.length
			if original_count != proper_count
				user.update_attributes(reading_logs_count: proper_count)
				Rails.logger.info("Changed User reading logs count from #{original_count} to #{proper_count}")
			end
		end 
	end

	def redo_topic_tags
		Article.find_each(batch_size: @batch_size) do |article|
			article.create_topic_tags
		end 
	end 

	def deal_with_unclaimed_profiles
		oauth_token = ENV['TWITTER_TIM_OAUTH_TOKEN']
		oauth_secret = ENV['TWITTER_TIM_OAUTH_SECRET']
		consumer_key = ENV['TWITTER_API_KEY']
		secret_key = ENV['TWITTER_SECRET_KEY']
		request_url = "https://api.twitter.com/1.1/users/show.json"
		request_method = "GET"
			
		unclaimed_users = User.where(claimed_profile: false)
		unclaimed_users.each do |user| 
			if user.authorizations.find_by_provider("twitter")
				auth = user.authorizations.find_by_provider("twitter")
				screen_name = auth.name
				params = {screen_name: screen_name} 
				options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
				twitter_response = oauth_request(options)
				if twitter_response['profile_image_url']
					twitter_response['profile_image_url'].slice!("_normal")
					avatar_url = twitter_response['profile_image_url']
					Resque.enqueue(UserAvatarUploader, user.id, avatar_url)
				end 
			end 
		end 
	end 

end 