  # Adapted by Carel for heroku-api
# Code from http://blog.leshill.org/blog/2011/04/03/using-resque-and-resque-scheduler-on-heroku.html
# Based on the ideas from: http://blog.darkhax.com/2010/07/30/auto-scale-your-resque-workers-on-heroku
require 'platform-api'

# Scale workers on Heroku automatically as your Resque queue grows.
# Mixin the +AutoScaling+ module into your models to get the behavior.
#
#   class MyModel < ActiveRecord::Base
#     extend HerokuAutoScaler::AutoScaling
#   end
#
# And configure in an initializer +config/initializers/heroku_workers.rb+:
#
#   HerokuAutoScaler.configure do
#     scale_by {|pending| }
#   end
#
# The default scaling is non-linear:
# * 1 job => 1 worker
# * 15 jobs => 2 workers
# * 25 jobs => 3 workers
# * 40 jobs => 4 workers
# * 60+ jobs => 5 workers
module HerokuAutoScaler
  module AutoScaling
    def after_perform_scale_down(*args)
      HerokuAutoScaler.scale_down!
    end

    def after_enqueue_scale_up(*args)
      HerokuAutoScaler.scale_up!
    end

    def on_failure(e, *args)
      Rails.logger.info("Resque Exception for [#{self.to_s}, #{args.join(', ')}] : #{e.to_s}")
      HerokuAutoScaler.scale_down!
    end
  end

  extend self

  attr_accessor :ignore_scaling

  def ignore_scaling
    $redis.get("heroku_ignore_scaling")
  end

  def clear_resque
    Resque::Worker.all.each {|w| w.unregister_worker} 
  end

  def configure(&block)
    instance_eval(&block) if block_given?
  end

  def scale_by(&block)
    self.scaling_block = block
  end

  def scale_down!
    return if ignore_scaling
    ping_web_dyno if self.ping_required
    if job_count == 0 && heroku.rate_limit.info['remaining'] > 0 && current_heroku_workers == 1
      # Resque.workers.each {|w| w.unregister_worker if w.processing['run_at'] && Time.now - w.processing['run_at'].to_time > 600 }
      self.heroku_workers = 0
    end
  end

  def ping_web_dyno
    $redis.set("web_dyno_last_pinged", Time.now)
    HTTParty.get("https://roundview.co")
    number_of_pings = $redis.get("web_dyno_ping_counter").to_i + 1
    $redis.set("web_dyno_ping_counter", number_of_pings)
  end 

  def scale_up!
    return if ignore_scaling
    pending = job_count
    self.heroku_workers = 1 if pending > 0
  end

  def ping_required
    last_pinged = $redis.get("web_dyno_last_pinged").to_time 
    last_pinged + 1.hour < Time.now
  end 

  private

  attr_accessor :scaling_block

  

  def heroku
    if ENV['HEROKU_API_KEY'] && ENV['HEROKU_APP']
      @@heroku ||= PlatformAPI.connect_oauth(ENV['HEROKU_API_KEY'])
    else
      false
    end
  end

  def heroku_workers=(qty)
    if heroku && worker_change_needed(qty)
      begin 
        if heroku.rate_limit.info['remaining'] > 0 
          heroku.formation.update(ENV['HEROKU_APP'], 'worker', {"quantity"=> qty}) 
          $redis.set("current_heroku_workers", qty)
        end 
      rescue StandardError =>e 
        Rails.logger.error("heroku autoscaler error in method: 'heroku_workers=', error: #{e}, first-line: #{e.backtrace[0]}")
      end
    end 
  end
  
  def current_heroku_workers 
    $redis.get("current_heroku_workers").to_i
  end 

  def worker_change_needed(qty)
    if qty != current_heroku_workers 
      return true 
    else 
      return false 
    end 
  end 

  def job_count
    Resque.info[:pending]
  end

  def resque_workers
    Resque.info[:working]
  end

  def scale_array 
    unless @scale_array 
      arr = [{workers: 1, job_count: 1}, {workers: 2, job_count: 2}, {workers: 3, job_count: 30}, {workers: 4, job_count: 80}]
      # i = 5
      # 80.times do 
      #   scale_info_hash ={ workers: i, job_count: i * 20 }
      #   arr.push(scale_info_hash)
      #   i += 1
      # end
      @scale_array = arr 
    end 
    return @scale_array
  end 

  def workers_for(pending_jobs)
    if scaling_block
      scaling_block.call(pending_jobs)
    else
      scale_array.reverse_each do |scale_info|
        # Run backwards so it gets set to the highest value first
        # Otherwise if there were 70 jobs, it would get set to 1, then 2, then 3, etc

        # If we have a job count greater than or equal to the job limit for this scale info
        if pending_jobs >= scale_info[:job_count]
          return scale_info[:workers]
        end
      end
    end
  end
end