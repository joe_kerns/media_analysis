namespace :random do
  desc "Random test tasks"

  task :user_query, [:query, :user_id] => :environment do |task, args|
  	# rake random:user_query['obama', 32]
  	query = args.query.downcase
  	user = User.find(args.user_id)
  	entities = user.articles.pluck(:person_entities, :company_entities, :place_entities, :job_title_entities, :other_entities).flatten
  	query_entities = Hash.new
  	entities.each do |entity|
  		unless entity.nil? 
	  		if entity.include?(query) || entity.include?(query.capitalize)
	  			query_entities[entity] ||= 0
	  			query_entities[entity] += 1
	  		end  
	  	end 
  	end 
  	debugger
  	articles = user.articles.where("'Clinton' like ANY (person_entities)")
  end 

  task encoded_urls: :environment do 
    number = 0 
    output_array = Array.new
    Article.find_each do |art|
      new_url = Article.decode_url(art.url)
      if new_url != art.url
        number += 1
        output_array.push(art.id)
        puts "new_url: #{new_url}"
        puts "old_url: #{art.url}"
        puts "" 
      end 
    end 
    puts "#{number}"
    puts output_array
  end  

  task test_raise: :environment do 
    test = nil
    begin 
        if test 
          puts "TEST!"
        else
            raise "Pocket Authorization Error"
        end  
    rescue Exception => e
        debugger 
        puts "RESCUED!"
    end 
  end 

  task entity_order: :environment do 
  	master_entities = Article.pluck(:person_entities, :company_entities, :place_entities, :job_title_entities, :other_entities).flatten.uniq
  	entities_hash = Hash.new
  	CSV.open("entities_lookup.csv", "a") do |csv|
  		csv << ["original", "lookup"]
	  	master_entities.each do |entity| 
	  		unless entity.nil?
				entities = master_entities.clone 
				entities.delete(entity)
				fuzzy = FuzzyMatch.new(entities)
				matched_entity = fuzzy.find(entity)	
		  		entities_hash[entity] = matched_entity
		  		csv << [entity, matched_entity]
		  		puts "#{entity} matched to #{matched_entity}"
		  	end 
	  	end 
  	end
  	debugger
  	puts "test"
  end 
end 