namespace :categorizer do
  desc "Use semantria to categorize"
  require 'semantria'

  task diffbot_semantria: :environment do
    5.times do 
      diffbot_token = ENV['DIFFBOT_TOKEN']
      
      diffbot_url = "http://api.diffbot.com/v3/article"
      fields = "tags,humanLanguage"
      
      semantriaKey = ENV['SEMANTRIA_KEY']
      semantriaSecret = ENV['SEMANTRIA_SECRET']
      Article.skip_callback(:update, :before, :update_reading_hashes)
      user = User.find(13) 
      session = Session.new(semantriaKey, semantriaSecret, 'TestApp', true)
      articles = user.articles.where(categorized_flag: false).order("time_read DESC").limit(300)
      documents_queued = 0
      articles.each do |article|
        begin 
          url = article.url
          puts "#{url}"
          encoded_url = URI.encode(url)
          
          if article.content.nil?
            diffbot_response = HTTParty.get(diffbot_url, :query => {token: diffbot_token, url: encoded_url, fields: fields, meta: true, timeout: 10000})
            if diffbot_response['error'] 
              content = nil
              article.update_attributes(categorized_flag: true)
            else 
              content = diffbot_response['objects'][0]['text']
              article.update_attributes(content: content)
            end 
          elsif article.content == "" 
            content = article.content
            article.update_attributes(categorized_flag: true)
          else 
            content = article.content
          end

          unless content.nil? || content == "" 
            doc = {'id' => article.id, 'text' => content.truncate(32000)}
            status = session.queueDocument(doc)
            documents_queued += 1 if status == 202
          end 
        rescue StandardError => e
          debugger
          article.update_attributes(categorized_flag: true)
          puts "Error"
        end 
      end 
      results = Array.new
      puts "Articles queued for processing: #{documents_queued}"
      catch (:done) do
        n = 0 
        while results.length < documents_queued
          sleep(10)
          status = session.getProcessedDocuments
          n += 1 
          status.is_a? Array and status.each do |object|
            results.push(object)
          end
          puts "Articles retrieved: #{results.length}"
          throw :done if n > 20
        end  
        throw :done
      end 
      
      results.each do |result|
        begin
          person_entities = Array.new
          company_entities = Array.new
          product_entities = Array.new
          place_entities = Array.new
          job_title_entities = Array.new
          other_entities = Array.new
          article_id = result['id']
          article_to_update = Article.find(article_id)
          summary = result['summary']

          if result['auto_categories']
            topic = result['auto_categories'][0]['title']
            if result['auto_categories'][0]['categories']
              subtopic = result['auto_categories'][0]['categories'][0]['title']
            else 
              subtopic = "Uncategorized"
            end
          else
            topic = "Uncategorized"
            subtopic = "Uncategorized"  
          end 

          if result['entities']
            result['entities'].each do |entity|
              entity_name = entity['title'].truncate(255)
              case entity['entity_type']
              when "Person"
                person_entities.push(entity_name) unless person_entities.include? entity_name
              when "Company"
                company_entities.push(entity_name) unless company_entities.include? entity_name
              when "Product"
                product_entities.push(entity_name) unless product_entities.include? entity_name
              when "Place"
                place_entities.push(entity_name) unless place_entities.include? entity_name
              when "Job Title"
                job_title_entities.push(entity_name) unless job_title_entities.include? entity_name
              else 
                other_entities.push(entity_name) unless other_entities.include? entity_name
              end 
            end 
          end 
          article_to_update.update_attributes(summary: summary, person_entities: person_entities, company_entities: company_entities, product_entities: product_entities, place_entities: place_entities, job_title_entities: job_title_entities, other_entities: other_entities, topic: topic, subtopic: subtopic, categorized_flag: true)
        rescue StandardError => e
          debugger
          puts "Error"
        end 
      end
    end 
  end 

  task delete_topics: :environment do
    articles = Article.where('topic IS NOT NULL')
    Article.skip_callback(:update, :before, :update_reading_hashes)
    articles.each do |article|
      article.update_attributes(topic: nil)
    end 
  end

  task semantria_discover_mode: :environment do 
    semantriaKey = ENV['SEMANTRIA_KEY']
    semantriaSecret = ENV['SEMANTRIA_SECRET']
    session = Session.new(semantriaKey, semantriaSecret, 'TestApp', true)
    user = User.find(32)
    articles = user.articles.where('content IS NOT NULL AND LENGTH(content) > ?', 0).order('time_read DESC').limit(1000);0
    # June 20 to August 20 
    article_text_array = Array.new
    articles.each { |article| article_text_array.push(article.content.truncate(32000))};0
    collection_id = rand(10 ** 10).to_s.rjust(10, '0')
    collection = {"id" => collection_id, "tag" => "test", "documents" => article_text_array};0
    status = session.queueCollection(collection)
    result = session.getCollection(collection_id)
    # 32000 character limit per document
    # 1000 document limit per collection -- this can be adjusted 
  end 

  task test_topic_tags: :environment do 
    User.find_each do |user|
      CSV.open("#{user.first_name}_#{user.last_name}_ranked_tags.csv", "a") do |csv|
        csv << ["topic_tag", "word_count"]
        topic_tags = user.articles.pluck(:topic_tags, :word_count)
        reading_hash = Hash.new(0)
        topic_tags.each do |tags_array, word_count| 
          if tags_array.nil?
            reading_hash['Uncategorized'] += word_count.to_i
          else 
            tags_array.each {|tag|reading_hash[tag] += word_count.to_i unless tag.nil? } 
          end 
        end 
        srh = reading_hash.sort_by{|topic, number| number}.reverse
        srh.each do |array|
          csv << array
        end 
      end
    end 
  end 

end