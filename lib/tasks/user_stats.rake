namespace :user_stats do
	desc "Create summary data for users"
  	

    task weekly_email: :environment do 
    	time_period = "last week"
    	one_week_ago = 1.week.ago
    	books_array = books_list
    	CSV.open("users_weekly_email.csv", "a") do |csv|
			###### Category in top decile 
			topics_hash = topic_percentile_hash(one_week_ago)


        	csv << ["user_id", "email","first_name", "last_name", "name" "total_articles", "total_words", "category_1_name", "category_1_articles", "category_1_weeks_in_a_row", "category_1_twitter_share_link", "category_2_name", "category_2_articles", "category_2_weeks_in_a_row", "category_2_twitter_share_link", "category_3_name", "category_3_articles", "category_3_weeks_in_a_row", "category_3_twitter_share_link", "top_decile_category","top_decile_category_words", "top_decile_category_percentile","top_decile_category_twitter_link","recommended_article_1_cover_image_url", "recommended_article_1_title", "recommended_article_1_domain", "recommended_article_1_url","recommended_article_2_cover_image_url", "recommended_article_2_title", "recommended_article_2_domain", "recommended_article_2_url","recommended_article_3_cover_image_url", "recommended_article_3_title", "recommended_article_3_domain", "recommended_article_3_url", "articles_read_others_ever_read", "articles_read_others_read_last_week_only", "number_of_users_with_articles_in_common_last_week", "top_user_with_common_reading_1_first_name", "top_user_with_common_reading_1_last_name", "top_user_with_common_reading_1_image_url","top_user_with_common_reading_1_profile_url", "top_user_with_common_reading_1_top_topic", "top_user_with_common_reading_1_top_topic_url", "top_user_with_common_reading_2_first_name", "top_user_with_common_reading_2_last_name", "top_user_with_common_reading_2_image_url","top_user_with_common_reading_2_profile_url", "top_user_with_common_reading_2_top_topic", "top_user_with_common_reading_2_top_topic_url", "book_title", "book_author", "book_words", "share_of_book_read", "book_twitter_share_link"]
        	
    		User.where(claimed_profile: true).find_each do |user|
	    			output_hash = {"user_id"=>"", "email"=>"", "first_name"=>"", "last_name"=>"", "name"=>"", "total_articles"=>"","total_words"=>"", "category_1_name"=>"", "category_1_articles"=>"", "category_1_weeks_in_a_row"=>"", "category_1_twitter_share_link"=>"", "category_2_name"=>"", "category_2_articles"=>"", "category_2_weeks_in_a_row"=>"","category_2_twitter_share_link"=>"", "category_3_name"=>"", "category_3_articles"=>"", "category_3_weeks_in_a_row"=>"","category_3_twitter_share_link"=>"", "top_decile_category" => "", "top_decile_category_words"=>"", "top_decile_category_percentile" => "", "top_decile_category_twitter_link"=>"","recommended_article_1_cover_image_url"=>"", "recommended_article_1_title"=>"", "recommended_article_1_domain"=>"", "recommended_article_1_url"=>"","recommended_article_2_cover_image_url"=>"", "recommended_article_2_title"=>"", "recommended_article_2_domain"=>"", "recommended_article_2_url"=>"","recommended_article_3_cover_image_url"=>"", "recommended_article_3_title"=>"", "recommended_article_3_domain"=>"", "recommended_article_3_url"=>"", "articles_read_others_ever_read"=>"", "articles_read_others_read_last_week_only"=>"", "number_of_users_with_articles_in_common_last_week"=>"", "top_user_with_common_reading_1_first_name"=>"", "top_user_with_common_reading_1_last_name"=>"", "top_user_with_common_reading_1_image_url"=>"","top_user_with_common_reading_1_profile_url"=>"", "top_user_with_common_reading_1_top_topic"=>"", "top_user_with_common_reading_1_top_topic_url"=>"", "top_user_with_common_reading_2_first_name"=>"", "top_user_with_common_reading_2_last_name"=>"", "top_user_with_common_reading_2_image_url"=>"","top_user_with_common_reading_2_profile_url"=>"", "top_user_with_common_reading_2_top_topic"=>"","top_user_with_common_reading_2_top_topic_url"=>"", "book_title"=>"", "book_author"=>"", "book_words"=>"", "share_of_book_read"=> "","book_twitter_share_link"=>""}

	    			output_hash['user_id'] = user.id
	    			output_hash['email'] = user.email
	    			output_hash['first_name'] = user.first_name
	    			output_hash['last_name'] = user.last_name
	    			output_hash['name'] = user.name
	    			
	    			output_hash['total_articles'] = user.total_articles(time_period, "All")
	    			number_of_words = user.total_word_count(time_period)
	    			output_hash['total_words'] = number_of_words
	    			top_topics = user.top_topics(time_period, 50)
	    			
	    			if top_topics && top_topics.length > 0
	    				
	    				catch(:done) do
			    			top_topics.each_with_index do |topic_hash, index|
			    				topic_tag = topic_hash['topic']
			    				####### Reading for top 3 categories 
			    				if index < 3
				    				category_number = index + 1
				    				output_hash["category_#{category_number}_name"] = topic_tag
				    				number_of_articles = user.total_articles(time_period, topic_tag)
				    				output_hash["category_#{category_number}_articles"] = number_of_articles
				    				topic_tag_consecutive_weeks = user.topic_consecutive_weeks(topic_tag)
				    				output_hash["category_#{category_number}_weeks_in_a_row"] = topic_tag_consecutive_weeks
				    				output_hash["category_#{category_number}_twitter_share_link"] = "https://twitter.com/share?url=" + URI.encode("https://roundview.co/#/users/#{user.id}/1/summary?topic_tag=#{topic_tag}") + "&text=" + URI.encode("I read #{number_of_articles} about #{topic_tag} last week") + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 
				    			end 

				    			#### Percentile for top category

				    			if topics_hash[topic_tag]
				    				topic_words = user.total_word_count(time_period, topic_tag)
									topic_percentile = percentile(topics_hash[topic_tag], topic_words).truncate
									if topic_percentile > 90 && topic_percentile > output_hash["top_decile_category"].to_i
										output_hash["top_decile_category"] = topic_tag
										output_hash["top_decile_category_words"] = topic_words
										output_hash["top_decile_category_percentile"] = topic_percentile
										output_hash["top_decile_category_twitter_link"] = "https://twitter.com/share?url=" + URI.encode("https://roundview.co/#/users/#{user.id}/1/summary?topic_tag=#{topic_tag}") + "&text=" + URI.encode("I was in the #{topic_percentile} percentile of readers about #{topic_tag} last week on Roundview") + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 
									end 
								end 

								throw :done	 if output_hash["top_decile_category"].length > 0 && index >= 2 
			    			end 
			    		end 

		    			###### MISSED ARTICLES FOR TOP TOPIC ###### 
			    		top_topic = top_topics[0]['topic']
			    		   ### Exclude articles that person EVER Read, look through ALL articles read this week 
			    		articles_read_ids = user.articles.where("time_read > ? AND ? = ANY(articles.topic_tags)", one_week_ago, top_topic).pluck(:id) 
			    		unread_articles = Article.where("created_at > ? AND id NOT IN (?) AND ? = ANY(articles.topic_tags) AND reading_logs_count > ?", one_week_ago, articles_read_ids, top_topic, 1)
			    		article_ids_to_recommend = []
			    		unread_articles.each do |article|
			    			include_as_option = false
			    			article.reading_logs.each do |log|
								include_as_option = true if log.time_read > one_week_ago
			    			end 
			    			article_ids_to_recommend.push(article.id) if include_as_option
			    		end 
			    		recommended_articles = Article.where("id IN (?)", article_ids_to_recommend).order("reading_logs_count DESC").limit(3)
			    		recommended_articles.each_with_index do |article, index|
			    			article_number = index + 1 
			    			output_hash["recommended_article_#{article_number}_cover_image_url"] = article.cover_image_url
			    			output_hash["recommended_article_#{article_number}_title"] = article.title
			    			output_hash["recommended_article_#{article_number}_domain"] = article.domain
			    			output_hash["recommended_article_#{article_number}_url"] = article.url

			    		end 
		    		end 
					

					#### Articles in common with others  
						article_ids_that_user_and_others_have_read = user.articles.where("time_read > ? AND reading_logs_count > ?", one_week_ago, 1).pluck(:article_id).uniq
						articles_others_read_last_week = ReadingLog.where("time_read > ?", one_week_ago).pluck(:article_id).uniq
						articles_in_common_last_week_only = (articles_others_read_last_week & article_ids_that_user_and_others_have_read)
						output_hash["articles_read_others_ever_read"] = article_ids_that_user_and_others_have_read.length
						output_hash["articles_read_others_read_last_week_only"] = articles_in_common_last_week_only.length
						
						users_with_articles_in_common_last_week = ReadingLog.where("time_read > ? AND article_id IN (?) and user_id != ?", one_week_ago, articles_in_common_last_week_only, user.id).pluck(:user_id).uniq
						output_hash["number_of_users_with_articles_in_common_last_week"] = users_with_articles_in_common_last_week.length
						
					#### Users with most words read in common
						common_reading_logs = ReadingLog.where("time_read > ? AND article_id IN (?) and user_id != ?", one_week_ago, articles_in_common_last_week_only, user.id)
						overlapping_reading_hash = {}
						common_reading_logs.each do |log|
							overlapping_reading_hash[log.user_id] ||= 0
							overlapping_reading_hash[log.user_id] += log.article.word_count.to_i
						end 
						users_sorted = overlapping_reading_hash.sort_by{|user_id, word_count| word_count}.reverse
						if users_sorted 
							first_user_id = users_sorted[0][0] if users_sorted[0]
							second_user_id = users_sorted[1][0] if users_sorted[1]
							users_arr = [first_user_id, second_user_id]
							users_arr.each_with_index do |user_id, index|
								if user_id && common_user = User.find(user_id)
									user_number = index + 1
									output_hash["top_user_with_common_reading_#{user_number}_first_name"] = common_user.first_name
									output_hash["top_user_with_common_reading_#{user_number}_last_name"] = common_user.last_name
									output_hash["top_user_with_common_reading_#{user_number}_image_url"] = common_user.avatar.url
									output_hash["top_user_with_common_reading_#{user_number}_profile_url"] = "https://roundview.co/#/users/#{common_user.id}/1/summary"
									
									
									common_article_ids = ReadingLog.where("time_read > ? AND (user_id = ? OR user_id = ?)", one_week_ago, user.id, common_user.id).pluck(:article_id).uniq
									topic_tags_array = Article.where("id IN (?)", common_article_ids).pluck(:topic_tags, :word_count)
									top_shared_topics = user.top_topics_array_from_tags(topic_tags_array)
									top_common_topic = top_shared_topics[0][:topic] if top_shared_topics && top_shared_topics[0]
									output_hash["top_user_with_common_reading_#{user_number}_top_topic"] = top_common_topic
									output_hash["top_user_with_common_reading_#{user_number}_top_topic_url"] = "https://roundview.co/#/users/#{common_user.id}/1/summary?topic_tag=#{top_common_topic}"
								end
							end 	
						end

						book_hash = match_book(books_array, number_of_words)
						output_hash['book_title'] = book_hash['title']
						output_hash['book_author'] = book_hash['author']
						output_hash['book_words'] = book_hash['words']
						output_hash['share_of_book_read'] = number_of_words.to_f / book_hash['words'].to_i
						output_hash['book_twitter_share_link'] = "https://twitter.com/share?url=" + URI.encode("https://roundview.co/#/users/#{user.id}/1/summary") + "&text=" + URI.encode("I read #{number_of_words} words online last week, which is like reading #{book_hash['title']} by #{book_hash['author']}") + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 		
				csv << output_hash.values
    		end 
    	end
    end 

    task badges: :environment do 
    	users = User.where("reading_logs_count > ?", 0).limit(100).order("RANDOM()")
		books_array = books_list
		timelines = users.first.time_periods
		
		percentiles_hash = {"last 12 weeks" => topic_percentile_hash(12.weeks.ago), "last week" => topic_percentile_hash(1.week.ago)}

		output_hash = {}
		CSV.open("test_user_badges.csv", "a") do |csv|
			timelines.each do |time_period, since_time|
				
				users.each do |user|
					if percentiles_hash[time_period]
						topics_hash = percentiles_hash[time_period]
					else
						since_time = user.last_sign_in_at 
						topics_hash = topic_percentile_hash(since_time) if since_time

					end 
					output_hash["user_id"] = user.id
					output_hash['user_email'] = user.email
					output_hash['user_first_name'] = user.first_name
					output_hash['user_last_name'] = user.last_name
					output_hash['time_period'] = time_period
					unless since_time == 0 
						articles = user.articles.where("time_read > ?", since_time)
						number_of_domains = articles.pluck(:domain).uniq.length
						output_hash['number_of_different_domains'] = number_of_domains
						number_of_articles = user.total_articles(time_period)
						output_hash['number_of_articles'] = number_of_articles
						number_of_words = user.total_word_count(time_period)
						output_hash['number_of_words'] = number_of_words
						top_topics = user.top_topics(time_period, 50)
			    		if top_topics && top_topics.length > 0	
		    				catch(:done) do
				    			top_topics.each do |topic_hash|
				    				topic_tag = topic_hash['topic']
					    			#### Percentile for top category
					    			if topics_hash && topics_hash[topic_tag]
										topic_percentile = percentile(topics_hash[topic_tag], user.total_word_count(time_period, topic_tag)) 
										if topic_percentile > 90 
											output_hash["top_decile_category"] = topic_tag
											output_hash["top_decile_category_percentile"] = topic_percentile
										end 
									end 

									throw :done	 if output_hash["top_decile_category"] 
				    			end 
				    		end 
					    end 
						book = match_book(books_array, number_of_words)
						output_hash.merge!(book)
					end
				    csv << output_hash.values 
				end 

	    	end 
		end
    	#Get list of popular books and their page coutns / word counts then find a book 
    	
    	# Timeline -- since you last logged in; in the last week; in the last month 
		# What:
		# --Number of different sites read from 
		# --Number of words read and their page / book equivalent (by topic) 
		# --Topic that you were in the top decile of reading for
		# --% of an issue read (e.g., I read 95% of last week's Economist) 

    end 
	
	def books_list
		books_array = Array.new
		csv_text = File.read("books.csv")
		csv = CSV.parse(csv_text, :headers => true)
		csv.each do |row|
			books_array.push(row.to_hash)
		end 
		return books_array
	end 

	def match_book(books_list, word_count)
		books_list.reverse.each_with_index do |book_hash, index|
			book_words = book_hash['words'].to_i
			if word_count > book_words
				book_hash['percent'] = word_count.to_f / book_words
				return book_hash
			end 
		end 

		book_hash = books_list.first
		book_words = book_hash['words'].to_i
		book_hash['percent'] = word_count.to_f / book_words
		return book_hash
	end 
    
	def percentile(array, value)
		length = array.length
		index = array.index(value)
		percentile = index.to_f / length * 100
	end 

	def topic_percentile_hash(since_time)
		###### Category in top decile 
		users_hash = Hash.new
		topics_hash = Hash.new

		logs = ReadingLog.where("time_read > ?", since_time)
		logs.each do |log|
			user_id = log.user_id
			if log.article
				word_count = log.article.word_count.to_i 
				topic_tags = log.article.topic_tags
				users_hash[user_id] ||= Hash.new(0)
				topic_tags.each {|topic| users_hash[user_id][topic] += word_count } if topic_tags
			end 
		end 

		users_hash.each do |user_id, user_topics_hash|
			user_topics_hash.each do |topic, word_count| 
				topics_hash[topic] ||= [] 
				topics_hash[topic].push(word_count)

			end 
		end 

		topics_hash.each do |topic, array|
			topics_hash[topic] = array.sort
		end 
		return topics_hash
	end 

end 