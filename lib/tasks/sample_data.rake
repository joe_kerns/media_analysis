namespace :db do
  desc "Fill database with sample data"
  #To populate the database, run the following two commands:
  # $ rake db:reset
  # $ rake db:populate

  task import_data: :environment do
    ### Populate Categories database with Alchemy Api used taxonomy 
    Category.import('./taxonomy.csv')
  end 

  task populate: :environment do
    ## 1 ## Initial User Creation
    #First user for our Testing
    User.create!(first_name: "Tim",
                 last_name: "Kalimov",
                 email: "tkalimov@gmail.com",
                 password: "password",
                 password_confirmation: "password",
                 )
    tim = User.first

    ## 2 ## User Authorizations

    auth = tim.authorizations.create(provider: "pocket", name: 'tkalimov', token: ENV['TIM_POCKET_TOKEN'])
    pocketUpdate(tim.id)

    auth = tim.authorizations.create(provider: "youtube", name: 'jmkerns', token: ENV['JOE_YOUTUBE_TOKEN']) ## JMK: Not sure about this code
    youtubeUpdate(tim.id) ## JMK: Not sure about this code

    ## 3 ## User Creation

    100.times do |n|
      email = "fake-#{n+1}@roundview.co"
      password  = "password"
      user = User.create!(first_name: Faker::Name.first_name,
                  last_name: Faker::Name.last_name,
                  email: email,
                  password: password,
                  password_confirmation: password
                  )

      ## 4 ## Tool Adaptation 

      ## POCKET
        #### For each user, I need to add 10 random articles from the first user's collection of articles
        articles_array = tim.articles
        10.times do |n|
          index = Random.new.rand(0..20)
          article = articles_array[index] 
          
          day_added = Random.new.rand(0..40)
          time_added = day_added.days.ago
          time_read = Random.new.rand(0..day_added).days.ago
          reading_source = "Pocket"
          user.reading_logs.create(:article => article, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
        end 
      ## YOUTUBE

      ## Create dummy video data extracted from "tim" video watching
      videos_array = tim.videos
      15.times do |n|
        index = Random.new.rand(0..40)
        video = videos_array[index]
        time_watched = Random.new.rand(0..40) ## need to check - likely incorrect
        
        ### User populate with dummy video data
        user.video_logs.create(:video => video, :time_watched => time_watched)
      end 
    end
  end


  def pocketUpdate(user_id)
    user = User.find_by_id(user_id)
    pocketAPI = 'https://getpocket.com/v3/get'    
    
    time_pocket_called = Time.now

    auth = user.authorizations.find_by_provider('pocket')
    if auth.time_data_updated
      since = auth.time_data_updated.to_time.to_i
    else 
      since = 0
    end 
    
    pocketReadArticles = HTTParty.post(pocketAPI, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], access_token: auth.token, state: 'archive', contentType: 'article', sort: 'newest', detailType: 'complete', since: since})
  
  
    if pocketReadArticles['list'].length > 0 
      pocketReadArticles['list'].values.each do |item|
        source_categories = Array.new
        if item['tags']
         source_categories.push(item['tags'].keys)
        else 
         source_categories = nil
        end 
        
        if item['resolved_title'].length > 255
          title = item['resolved_title'].truncate(255)
         else 
          title = item['resolved_title']
        end 
    
        word_count = item['word_count'].to_i
        url = item['resolved_url']
        time_added = Time.at(item['time_added'].to_i)
        time_read = Time.at(item['time_read'].to_i)
        encoded_url = URI.encode(url)
        domain = URI.parse(encoded_url).host
        reading_source = "Pocket"
        
        article = Article.where(title: title, url: url, word_count: word_count, domain: domain).first_or_create
        article.update_attributes(source_categories: source_categories)
        user.reading_logs.create(:article => article, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
      end 
    end 
    auth.update_attributes(time_data_updated: time_pocket_called)
  end 

  def youtubeUpdate(user_id)
    #### Look up proper authorization token for YouTube for this User and time since last update

    user = User.find_by_id(user_id)
      auth = user.authorizations.find_by_provider('youtube')

      time_youtube_called = Time.now
    index = 1

    ## Notes: videos that have been suspended no longer show duration or category so need to be accounted for

    catch (:done) do 
      while index < 500 do
        historyFeed = HTTParty.get('https://gdata.youtube.com/feeds/api/users/default/watch_history', {query: {v: 2, alt: 'json', access_token: auth.token, 'max-results' => 50, 'start-index' => index}})
        if historyFeed['feed']['entry']
            historyFeed['feed']['entry'].each do |entry|
              if auth.time_data_updated
                throw :done if user.videos.where(:time_watched => entry['published']['$t'], :source => 'YouTube').length > 0
              end   
              
              ## VIDEO TABLE DATA PULL
              title = entry['title']['$t']
              url = entry['link'][0]['href']
              category = entry['category'][0]['label']
              publisher = entry['media$group']['media$credit'][0]['yt$display']
              source_video_id = entry['media$group']['yt$videoid']['$t']
              source = 'Youtube'
              if entry['media$group']['yt$duration']
                length = entry['media$group']['yt$duration']['seconds'].to_i
              else
                length = 0
              end 
              
              ## VIDEO LOGS DATA PULL 
              time_watched = entry['published']['$t']
                
              ## VIDEO AND VIDEO LOGS DATA ENTRY  
                video = Video.where(title: title, url: url, source_video_id: source_video_id).first_or_create
                video.update_attributes(category: category, publisher: publisher, source: source, length: length)
                user.video_logs.create(:video => video, :time_watched => time_watched)
            end
          index += 50
        else
          auth.update_attributes(time_data_updated: time_youtube_called)
          throw :done 
        end  
      end 
    end 
  end
end