namespace :export do
  desc "Export articles data to CSV"
  #To populate the database, run the following two commands:
  # $ rake db:reset
  # $ rake db:populate
  
  task anonymize: :environment do 
    User.find_each do |user|
      
    end 
  end 
  task export_domains: :environment do
    CSV.open("category_lookup.csv", "wb") do |csv|
      Article.find_each do |article|
        count = Article.where(domain: article.domain).size
        csv << [article.title, article.domain, count.to_s]     
      end 
    end
  end
  
  task all_articles: :environment do
    CSV.open("all_articles.csv", "a") do |csv|
      csv << ["id", "title", "url", "word_count", "created_at", "domain", "content", "categorized_flag", "topic_tags"]
      Article.find_each do |article|
        content = article.content.truncate(500) unless article.content.nil?
        output_array = [article.id, article.title, article.url, article.word_count, article.created_at, article.domain, content, article.categorized_flag, article.topic_tags]
        csv << output_array
      end 
    end 
  end 

  task mistagged_articles: :environment do 
     articles = Article.where('? = ANY(topic_tags) AND ? = ANY(topic_tags) AND ? = ANY(topic_tags) AND ? = ANY(topic_tags)', 'Email_authentication', 'Spamming', 'Botnets', 'Security')
     CSV.open("mistagged_articles.csv", "a") do |csv|
      csv << ["id", "title", "url", "word_count", "created_at", "domain", "content", "categorized_flag", "topic_tags"]
      articles.each do |article|
        content = article.content.truncate(500) unless article.content.nil?
        output_array = [article.id, article.title, article.url, article.word_count, article.created_at, article.domain, content, article.categorized_flag, article.topic_tags]
        csv << output_array
      end 
    end
  end 

  task potential_dupes: :environment do
    content_hash = Hash.new
    Article.find_each do |article|
      unless article.content.nil?
        content = article.content 
        content_hash[content] ||= {}
        content_hash[content][:article_ids] ||= []
        content_hash[content][:count] ||= 0 
        content_hash[content][:article_ids].push(article.id)
        content_hash[content][:count] += 1
      end 
    end 
    all_ids_array = Array.new
    content_hash.each do |content, hash|
      all_ids_array.concat(hash[:article_ids]) if hash[:count] > 1 
    end 
    unique_ids = all_ids_array.uniq
    CSV.open("potential_dupes_articles.csv", "a") do |csv|
      csv << ["id", "title", "url", "word_count", "created_at", "domain", "content", "categorized_flag", "topic_tags"]
      Article.where('id IN (?)', unique_ids).find_each do |article|
        content = article.content.truncate(500) unless article.content.nil?
        output_array = [article.id, article.title, article.url, article.word_count, article.created_at, article.domain, content, article.categorized_flag, article.topic_tags]
        csv << output_array
      end 
    end 
  end 

  task articles_to_delete: :environment do 
    articles_to_delete = JSON.parse($redis.get("diffbotted_articles_to_delete")).uniq
    articles = Article.where('id IN (?)', articles_to_delete)
    CSV.open("diffbotted_articles_to_delete.csv", "a") do |csv|
      csv << ["id", "title", "url", "word_count", "created_at", "domain", "content", "categorized_flag", "topic_tags"]
      articles.each do |article|
        content = article.content.truncate(500) unless article.content.nil?
        output_array = [article.id, article.title, article.url, article.word_count, article.created_at, article.domain, content, article.categorized_flag, article.topic_tags]
        csv << output_array
      end 
    end 
  end 

  task export_entities: :environment do 
    user = User.find(13)
    CSV.open("jesse_all_entities_history.csv", "a") do |csv|
      attributes = Article.first.attributes
      attributes.delete("content")
      label_array = attributes.keys.concat(["time_read", "entity", "entity_type"])
      csv << label_array
      user.articles.where('summary IS NOT NULL').each do |article|
        time_read = article.reading_logs.where(user: user).first.time_read
        attributes = article.attributes
        attributes.delete("content")
        article.person_entities.each do |person|
          output_array = attributes.values.concat([time_read, person, "person"])
          csv << output_array
        end 
        article.company_entities.each do |person|
          output_array = attributes.values.concat([time_read, person, "company"])
          csv << output_array
        end 
        article.product_entities.each do |person|
          output_array = attributes.values.concat([time_read, person, "product"])
          csv << output_array
        end 
        article.place_entities.each do |person|
          output_array = attributes.values.concat([time_read, person, "place"])
          csv << output_array
        end 
        article.job_title_entities.each do |person|
          output_array = attributes.values.concat([time_read, person, "job_title"])
          csv << output_array
        end 
        article.other_entities.each do |person|
          output_array = attributes.values.concat([time_read, person, "other"])
          csv << output_array
        end 
      end 
    end 
  end 
  
  task calc_reading_level: :environment do
    CSV.open("Joe_reading2.csv", "a") do |csv|
      keys1 = Article.first.attributes.keys
      keys2 = ReadingLog.first.attributes.keys
      keys3 = ["calculated words", "readability words", "sentences", "syllables", "grade_level"]
      labels = keys1.concat(keys2).concat(keys3)
      csv << labels
      user = User.find(2)
      article_ids = []
      logs = user.reading_logs.where("article_id IN (?)", article_ids)
      logs.each do |log|
        array1 = log.attributes.values
        article = log.article
        array2 = article.attributes.values

        url = article.url
        parsed_object = Article.parse(url)
        text = parsed_object['content'] || ""
        readability_words = parsed_object['word_count'] || 0
        calculated_words = text.split(" ").size.to_f 
        sentences = text.split(/\.|\?|!/).reject{|s| s.strip.empty?}.size.to_f
        syllables = text.split(" ").inject(0){|sum, w| sum + syllable_count(w)}
        if (calculated_words > 0 && sentences > 0) 
          grade_level = (0.39 * (calculated_words / sentences) + 11.8 * (syllables / calculated_words) - 15.59).round(2)
        else 
          grade_level = 0 
        end 
        array_to_csv = array2.concat(array1).concat([calculated_words, readability_words, sentences, syllables, grade_level])

        csv << array_to_csv
        puts "#{article.id} successfully added to CSV"
      end 
    end 

  end 

  
  task all_user_articles: :environment do
    User.all.each do |user|
      CSV.open("#{user.first_name}_reading_logs", "a") do |csv|
        reading_logs = user.reading_logs
        reading_logs.each do |reading_log|
          time_added = reading_log.time_added
          time_read = reading_log.time_read
          reading_source = reading_log.reading_source
          favorite = reading_log.favorite
          article = reading_log.article
          title = article.title
          url = article.url
          word_count = article.word_count
          publisher = article.publisher
          domain = article.domain
          topic = article.topic
          subtopic = article.subtopic
          meta_keywords = article.meta_keywords
          output_array = [time_added, time_read, reading_source, favorite, title, url, word_count, publisher, domain, topic, subtopic, meta_keywords]
          csv << output_array
        end
      end
    end
  end  

  task specific_user_articles: :environment do
    user = User.find(2)
    CSV.open("#{user.first_name}_reading_logs.csv", "a") do |csv|
      csv << ["article_id", "reading_log_id", "title", "domain", "topic", "subtopic", "word_count", "url", "meta_keywords", "time_added_to_pocket", "time_read", "excerpt", "authors"]
      user.reading_logs.find_each do |reading_log|
        reading_log_id = reading_log.id
        time_added = reading_log.time_added
        time_read = reading_log.time_read
        reading_source = reading_log.reading_source
        favorite = reading_log.favorite
        article = reading_log.article
        article_id = article.id
        title = article.title
        url = article.url
        excerpt = article.excerpt
        word_count = article.word_count
        domain = article.domain
        topic = article.topic
        subtopic = article.subtopic
        meta_keywords = article.meta_keywords
        authors = article.authors
        output_array = [article_id, reading_log_id, title, domain, topic, subtopic, word_count, url, meta_keywords, time_added, time_read, excerpt, authors]
        csv << output_array
        puts "Article# #{article_id} exported"
      end
    end
  end  

  task wiki_categories: :environment do
    kim = HTTParty.get("https://www.kimonolabs.com/api/doht6gi2?apikey=baea0cea33df9ec23c38d115c5fb2480")
    kim['results']['collection1'].each do |item|
      words_array = item['categories']['text'].split("(")
      category_name = words_array.first
      number = words_array.last
    end 
  end 

  task twitter_pocket_users: :environment do 
    CSV.open("twitter_pocket_users.csv", "a") do |csv|
      csv << ["twitter_name", "twitter_username", "follower_count", "profile_url", "profile_description", "last_tweet_content", "number_of_pocket_related_tweets"]
      i = 0
      user_details_hash = JSON.parse($redis.get("twitter_user_details_hash")) 
      top_pocket_users_hash = JSON.parse($redis.get("top_pocket_users_hash"))
      user_details_hash.each do |username, user_info_hash|
        output_array = user_info_hash.values.push(top_pocket_users_hash[username])
        csv << output_array
      end 
    end 
  end 

  task params_urls: :environment do 
    CSV.open("params_urls.csv", "a") do |csv|
      articles = Article.where('url like ?', '%?%')
      csv << articles[0].attributes.keys
      articles.each do |article|
        csv << article.attributes.values
      end 
    end 
  end 

  task manual_parsed: :environment do 
    ids = ReadingLog.where("reading_logs.created_at > ? AND reading_source = ?", 11.days.ago, "Pocket").pluck(:article_id).uniq
      articles = Article.where("created_at > ? AND id IN (?)", 11.days.ago, ids)
      content_hash = Hash.new
        articles.each do |article|
          unless article.content.nil?
            content = article.content 
            content_hash[content] ||= {}
            content_hash[content][:article_ids] ||= []
            content_hash[content][:count] ||= 0 
            content_hash[content][:article_ids].push(article.id)
            content_hash[content][:count] += 1
          end 
        end 
        all_ids_array = Array.new
        content_hash.each do |content, hash|
          all_ids_array.concat(hash[:article_ids]) if hash[:count] > 1 
        end 
        duplicate_content_ids = all_ids_array.uniq
        articles_to_export = Article.where('id IN (?)', duplicate_content_ids)
      CSV.open("duplicate_content_manual_parse_articles.csv", "a") do |csv|
        csv << ["id", "title", "url", "word_count", "created_at", "domain", "content", "categorized_flag", "topic_tags"]
        articles_to_export.each do |article|
          content = article.content.truncate(1000) unless article.content.nil?
          output_array = [article.id, article.title, article.url, article.word_count, article.created_at, article.domain, content, article.categorized_flag, article.topic_tags]
          csv << output_array
        end 
      end 
  end 
  
  def syllable_count(word) # see https://stackoverflow.com/questions/1271918/ruby-count-syllables
    word.downcase!
    return 1 if word.length <= 3
    word.sub!(/(?:[^laeiouy]es|ed|[^laeiouy]e)$/, '')
    word.sub!(/^y/, '')
    word.scan(/[aeiouy]{1,2}/).size
  end

end