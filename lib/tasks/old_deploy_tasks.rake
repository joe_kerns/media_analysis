namespace :old_deploy do
	desc "Maintenance on production database for successful deployment"
  	require 'stuff-classifier'

  	task get_nyt_keywords: :environment do 
	    url = 'http://api.nytimes.com/svc/search/v2/articlesearch.json'
	    user = User.find(2)
	    user.articles.where("domain like ? AND publisher is NULL", "%nytimes%").each do |article|
	      begin 
	        query = article.title
	        api_key = ENV['NYT_ARTICLE_SEARCH_API_KEY']
	        nyt_response = HTTParty.get(url, :query => {q: query, 'api-key'=> api_key})
	        if nyt_response['response']['meta']['hits'] > 0
	          puts "successful response from API"
	          article_url = nyt_response['response']['docs'][0]['web_url']
	          excerpt = nyt_response['response']['docs'][0]['abstract']
	          word_count = nyt_response['response']['docs'][0]['word_count']
	          publisher = nyt_response['response']['docs'][0]['source']
	          ### Create keywords 
	            section = nyt_response['response']['docs'][0]['section_name']
	            sub_section = nyt_response['response']['docs'][0]['subsection_name']
	            keywords_array = nyt_response['response']['docs'][0]['keywords']
	            meta_keywords_array = [section, sub_section]
	            keywords_array.each do |keyword_hash|
	              meta_keywords_array.push(keyword_hash['value'])
	            end 
	            meta_keywords = meta_keywords_array.join(" ")

	          authors = Array.new 
	          unless nyt_response['response']['docs'][0]['byline'].nil?
		          unless nyt_response['response']['docs'][0]['byline'].length == 0
			          nyt_response['response']['docs'][0]['byline']['person'].each do |byline_hash|
			          	first_name = byline_hash['firstname'].to_s
			          	last_name = byline_hash['lastname'].to_s
			          	author_name = first_name + " " + last_name 
			          	authors.push(author_name)
			          end 
			      end 
			  end 
	          article.update_attributes(meta_keywords: meta_keywords, url: article_url, excerpt: excerpt, word_count: word_count, publisher: publisher, authors: authors)
	          puts "success!"
	        else 
	          puts "#{nyt_response}"
	        end 
	      rescue StandardError=>e 
	      	debugger
	        puts "Article (id: #{article.id}) title: #{article.title} failed because of #{e}"
	      end 
	    end 
  	end 	
	
	task get_authors: :environment do 
	    begin 
	      Article.where('authors IS NULL').each do |article|
	        parser_url = 'https://www.readability.com//api/content/v1/parser'
	        token = ENV['READABILITY_PARSER_KEY']
	        url = article.url
	        parsed_article = HTTParty.get(parser_url, :query => {url: url, token: token})
	        puts "Meta author: #{article.authors} and parsed author: #{parsed_article['author']}"
	        if parsed_article['author']
	          authors = [parsed_article['author'].downcase.titleize.truncate(255)]
	          article.update_attributes(authors: authors)
	        end 
	      end
	    rescue StandardError=>e 
	      puts e 
	    end 
	end

    task get_metadata: :environment do 
	    Article.where('meta_keywords IS NULL').each do |article|
	      begin
	        url = article.url 
	        page = MetaInspector.new(url, :allow_redirections => :all, warn_level: :warn, :timeout => 60) 
	      
	        if page
	          if page.meta['keywords']
	            keywords = page.meta['keywords']
	          end 
	          if page.meta['news_keywords']
	            news_keywords = page.meta['news_keywords']
	          end
	          if page.meta['author'] && page.meta['author'] != 'automatico'
	          	authors = [page.meta['author']]
	          end
	        end
	        best_keywords = keywords || news_keywords
			
			unless article.authors
				article.update_attributes(meta_keywords: best_keywords, authors: authors)
			else 	
				article.update_attributes(meta_keywords: best_keywords)
			end 
			puts "meta data got: #{best_keywords}"
		  rescue StandardError => e
	          puts e 
	      end 
	    end 
    end 

    task test_for_dupes: :environment do
		total_dupes = 0 
		Article.find_each do |article| 
			url = article.url 
			articles = Article.where(url: url)
			dupes = articles.size
			if dupes > 1
				total_dupes += 1
				puts "#{dupes} DUPLICATES DETECTED"
				articles.each do |duplicate|
					puts "Article id: #{duplicate.id}"
				end 	
			end 
		end 
		puts "TOTAL: #{total_dupes}"
	end 
	
	task clean_html_entities: :environment do 
		articles = Article.where('excerpt like ?', '%&%')
		articles.each do |article|
			clean_excerpt = Article.clean_html_text(article.excerpt)
			article.update_attributes(excerpt: clean_excerpt)
			puts "Excerpt cleaned: #{clean_excerpt}"
		end
	end
end 