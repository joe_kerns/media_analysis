namespace :db_clean do
	desc "Clean up redis as necessary"
  	require 'stuff-classifier'

    task clean_redis: :environment do 
    	$redis.keys.each do |key| 
    		unless key.include? "user_"
	    		keys_to_keep = ["clean_urls_batch_start", "ceonyc_last_logged_tweet", "sections", "last_logged_tweet", "facebook_avatar_time_check", "top_pocket_users_hash", "subsections", "twitter_user_details_hash"]
	    		$redis.del(key) unless keys_to_keep.include?(key)
    		end 
    	end 
    end 

end 