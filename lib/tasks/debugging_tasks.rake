namespace :debug do
  desc "create output that helps debug"
  require 'stuff-classifier'
	
	task articles_by_topic: :environment do 
		topics_array = Article.pluck(:topic).uniq
		i = 1
		while i < 8
			articles_since_time = i.days.ago.beginning_of_day()
			articles = Article.where('created_at > ?', articles_since_time)
			topics_hash ={}
			articles.each do |article|
				topics_hash[article.topic.to_s] ||= 0
				topics_hash[article.topic.to_s] += 1
			end 
			puts "#{i} days ago"
			topics_hash.sort.each do |topic, article_number|
				puts "#{topic},#{i} ,#{article_number}"
			end
			i += 1
		end 
	end 

end 