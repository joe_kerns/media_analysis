namespace :deploy do
	desc "Maintenance on production database for successful deployment"
  	require 'stuff-classifier'

  	task add_discover_scores: :environment do 
  		User.find_each {|user| user.set_discover_score}
  		users_to_hide = User.where("id IN (?)",[23, 24, 43, 12])
  		users_to_hide.each {|user| user.update_attributes(discover_score: 0, manual_discover_score: true)}
  	end 
	
	task clean_domains: :environment do
		Article.where(domain: nil).each do |article|
			domain = URI.parse(article.url).host
			new_domain = PublicSuffix.parse(domain).domain
			article.update_attributes(domain: new_domain)
		end 

		Article.find_each do |article|
			old_domain = article.domain
			new_domain = PublicSuffix.parse(old_domain).domain
			article.update_attributes(domain: new_domain)
		end 
	end 

	task clean_authors: :environment do 
		Article.where("'Hunterwalk' = ANY (authors)").each do |article|
			article.update_attributes(authors: ["Hunter Walk"])
		end  

		Article.find_each do |article|
			new_authors_array = Array.new
			unless article.authors.nil? 
				article.authors.each do |author|
					begin
					unless author.nil? 
						author_names = author.split
						new_author_string = ""
						author_names.each do |name|
							new_author_string.concat("#{name.downcase.capitalize} ")
						end 
						new_author_string.strip! 
						new_authors_array.push(new_author_string)
					end 
					rescue StandardError => e 
						debugger
						puts "test"
					end 
				end 
				article.update_attributes(authors: new_authors_array)
			end 
		end 
	end 
	
	task fix_twitter_auths: :environment do 
		include AuthorizationsHelper
		oauth_token = ENV['TWITTER_TIM_OAUTH_TOKEN']
		oauth_secret = ENV['TWITTER_TIM_OAUTH_SECRET']
		consumer_key = ENV['TWITTER_API_KEY']
		secret_key = ENV['TWITTER_SECRET_KEY']
		
		request_method = "GET"
		
		users = User.where("id > ? AND id < ?", 192, 241)
		users.each do |user|
			screen_name = user.email.split("@")[0]

			params = {screen_name: screen_name} 
			request_url = "https://api.twitter.com/1.1/users/show.json"
			options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
			twitter_response = oauth_request(options)
			if twitter_response['profile_image_url']
				twitter_response['profile_image_url'].slice!("_normal")
				avatar_url = twitter_response['profile_image_url']
				Resque.enqueue(UserAvatarUploader, user.id, avatar_url)
			end 
			
			request_url = "https://api.twitter.com/1.1/statuses/user_timeline.json"
			params = {screen_name: screen_name, count: "200", trim_user: true} 
			options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
			twitter_response = oauth_request(options)
			tweets = twitter_response.parsed_response
			last_time_read = user.reading_logs.order("time_read DESC").first.time_read
			catch (:done) do 
				tweets.each do |tweet|
					if tweet["created_at"].to_time == last_time_read
						user.authorizations.where(provider: "twitter").first_or_create(name: screen_name, last_logged_tweet: tweet['id'])
						throw :done
					end 
				end 
				throw :done
			end 
		end
	end 
end
