namespace :db_clean do
	desc "Task to clean the database as necessary"
  	require 'stuff-classifier'

    task create_first_mixpanel_event: :environment do 
   #  	tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 
   #  	User.find_each do |user|
			# tracker.import(ENV['MIXPANEL_API_KEY'],"#{user.id}", "User Created", {"time" => user.created_at.to_i})	
   #  	end 

    	# ReadingLog.where(reading_source: "chrome").each {|log| tracker.import(ENV['MIXPANEL_API_KEY'],"#{log.user_id}", "Article saved from Chrome Extension", {"user_id"=>log.user_id, "time" => log.time_read.to_i, "article_title" => log.article.title, "article_url" => log.article.url, "article_id"=> log.article_id})	} 
		
		tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 	
    	Authorization.find_each  do |auth| 
    		auth.token ? event = "Provider authorization successful" : event = "Provider authorization failed"
    		tracker.import(ENV['MIXPANEL_API_KEY'],"#{auth.user_id}", event, {"time" => auth.created_at.to_i, "provider" => auth.provider, "user_id"=> auth.user_id})
    	end 
    end 
    
	task urls: :environment do
		total_changed = 0
		Article.find_each do |article|
			original_url = article.url
			new_url = Article.clean_url(original_url) if original_url
			if original_url != new_url
				puts "Original: #{original_url}"
				puts "Cleaned: #{new_url}"
				article.update_attributes(url: new_url)
				total_changed += 1
			end 
		end 
		puts "Total urls updated: #{total_changed}"
	end 

	task identify_params: :environment do 
		articles = Article.where('url like ?', '%?%')
		hash = Hash.new
		articles.each do |article|
			split_url = article.url.split(/\?|=/)
			puts "article #{article.id} had multiple question marks" if split_url.length > 3
			hash[split_url[1]] ||= 0
			hash[split_url[1]] += 1 
		end
		sorted_params = hash.sort_by{|author, number| number}.reverse
		sorted_params[0..100].each {|param_array| puts "#{param_array[0]}  #{param_array[1]}"}
		debugger
		puts hash 
	end 


	task clean_article_dupes: :environment do
		total_deleted = 0 
		ReadingLog.skip_callback(:destroy, :before, :remove_from_reading_hash)
		Article.find_each do |article| 
			url = article.url 
			if url 
				articles = Article.where(url: url)
				dupes = articles.size
				if dupes >1
					master = articles[0]
					articles_to_delete = articles[1..-1]
					puts "#{master.id} kept"
					articles_to_delete.each do |art_to_delete|
						art_to_delete.reading_logs.each do |log|
							time_added = log.time_added
							time_read = log.time_read
							reading_source = log.reading_source
							favorite = log.favorite
							new_log = log.user.reading_logs.where(article: master).first_or_create(article: master, logged_flag: true,time_added: time_added, time_read: time_read, reading_source: reading_source, favorite: favorite)
						end
						art_to_delete.destroy
						total_deleted += 1
						puts "#{art_to_delete.id} Deleted"
					end 
				end 
			end 
		end 
		puts "TOTAL: #{total_deleted}"
	end 

	task clean_short_urls: :environment do 
		arts = Article.where("LENGTH(url) < ?", 40)
		arts.each do |article|
			begin 
				old_url = article.url 
				parsed_article = Article.parse(old_url)
				url = parsed_article['url']
				article.update_attributes(url: url)
				puts "#{old_url} turned into #{url}"
			rescue StandardError => e
				puts "Error with Article #{article.id}"
				puts "Error: #{e}"
			end 
		end 
	end

	task delete_non_articles: :environment do
		articles = Article.where('url like ? OR url like ? OR url like ? OR url like ? OR url like ? OR url like ?', '%instagram.com%', '%4sq.com%', "%vimeo.com%", "%youtube.com%", "%.pdf", "%foursquare.com%")
		number_deleted = 0
		articles.each do |article|
			article.destroy
			number_deleted += 1 
		end 
		puts "total deleted: #{number_deleted}"
	end    

	task fix_nil_articles: :environment do 
		Article.where(title: nil).each do |article|
			article.destroy
		end 
		topics_classifier = StuffClassifier::Bayes.new("Sections", :storage => StuffClassifier::RedisStorage.new("sections"))
		subtopics_classifier = StuffClassifier::Bayes.new("Subsections", :storage => StuffClassifier::RedisStorage.new("subsections"))
		
		Article.where(topic: nil).each do |article|
			begin 
				initial_topic = article.classify_topic(topics_classifier) 
				final_topic = article.classify_subtopic(initial_topic, subtopics_classifier)
				article.update_attributes(topic: final_topic)
			rescue StandardError => e
				puts "Error with categorizing article #{article.id}"
				puts "#{e} in #{e.backtrace[0]}"
			end 
		end 
	end 

	task fix_reading_logs_count: :environment do 
		Article.find_each do |article| 
			original_count = article.reading_logs.size
			proper_count = article.reading_logs.length
			if original_count != proper_count
				article.update_attributes(reading_logs_count: proper_count)
				puts "Changed count from #{original_count} to #{proper_count}"
			end 
		end 
	end 
end 