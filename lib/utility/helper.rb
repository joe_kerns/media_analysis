class Helper
	# require 'utility/helper'
	
	def initialize 
	end 

	def clean_previous_books_hash
		previously_read_books_hash = JSON.parse($redis.get("previously_read_books_hash"));0
		previously_read_books_hash.each do |user_id, books_array| 
			new_array = Array.new
			books_array.each_with_index {|value, index| new_array.push(value) if index < 2} 	
			previously_read_books_hash[user_id] = new_array
		end 
		$redis.set("previously_read_books_hash", previously_read_books_hash.to_json)
	end 

	def send_weekly_emails_data
    	time_period = "last week"
    	one_week_ago = 1.week.ago
    	books_array = books_list_descending_word_count
    	previously_read_books_hash = JSON.parse($redis.get("previously_read_books_hash"))
			###### Category in top decile 
		topics_hash = topic_percentile_hash(one_week_ago)
		base_url = "http://www.knowtify.io/api/v1"
		request_url = base_url + "/contacts/upsert"
		auth_header = "Token token=\"#{ENV['KNOWTIFY_API_TOKEN']}\"" 
		user_knowtify_email_start = $redis.get("user_knowtify_email_start").to_i
    	User.where("claimed_profile = ? AND reading_logs_count > ?", true, 0).find_each(start: user_knowtify_email_start) do |user|
    		top_line_hash = Hash.new
    		top_line_hash['email'] = user.email
			top_line_hash['name'] = user.name
			output_hash = Hash.new
			output_hash['user_id'] = user.id
			
			output_hash['first_name'] = user.first_name
			output_hash['last_name'] = user.last_name
			output_hash['total_articles'] = user.total_articles(time_period, "All")
			number_of_words = user.total_word_count(time_period)
			output_hash['total_words'] = number_of_words
			number_of_words_formatted = separate_comma(number_of_words)
			output_hash['total_words_formatted'] = number_of_words_formatted
			user_profile_url = "https://roundview.co/#/users/#{user.id}/1/summary"
			output_hash['user_profile_url'] = user_profile_url
			top_topics = user.top_topics(time_period, 50)
			
			if top_topics && top_topics.length > 0
				catch(:done) do
					output_hash['top_categories'] = []
	    			top_topics.each_with_index do |topic_hash, index|
	    				top_category_output_hash = {}
	    				topic_tag = topic_hash['topic']
	    				####### Reading for top 3 categories 
	    				if topic_tag && index < 3
		    				top_category_output_hash["category_name"] = topic_tag
		    				number_of_articles = user.total_articles(time_period, topic_tag)
		    				top_category_output_hash["category_articles_number"] = number_of_articles
		    				topic_tag_consecutive_weeks = user.topic_consecutive_weeks(topic_tag)
		    				top_category_output_hash["category_weeks_in_a_row"] = topic_tag_consecutive_weeks
		    				top_category_output_hash["category_twitter_share_link"] = "https://twitter.com/share?url=" + URI.encode(user_profile_url + "?topic_tag=#{topic_tag}") + "&text=" + URI.encode("I read #{number_of_articles} articles about #{topic_tag} last week on Roundview!") + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 
		    				output_hash['top_categories'].push(top_category_output_hash)
		    			end 

		    			#### Percentile for top category

		    			if topics_hash[topic_tag]
		    				topic_words = user.total_word_count(time_period, topic_tag)
							topic_percentile = percentile(topics_hash[topic_tag], topic_words).truncate
							if topic_percentile > 90 && topic_percentile > output_hash["top_decile_category"].to_i
								output_hash["top_decile_category"] = topic_tag
								output_hash["top_decile_category_words"] = topic_words
								output_hash["top_decile_category_percentile"] = topic_percentile
								output_hash["top_decile_category_percentile_ordinalized"] = topic_percentile.ordinalize
								output_hash["top_decile_category_twitter_link"] = "https://twitter.com/share?url=" + URI.encode("https://roundview.co/#/users/#{user.id}/1/summary?topic_tag=#{topic_tag}") + "&text=" + URI.encode("I was in the #{topic_percentile.ordinalize} percentile of readers about #{topic_tag} last week on Roundview") + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 
							end 
						end 

						throw :done	 if output_hash["top_decile_category"] && index >= 2 
	    			end 
	    		end 

    			###### MISSED ARTICLES FOR TOP TOPIC ###### 
	    		top_topic = top_topics[0]['topic']
	    		output_hash['top_topic'] = top_topic
	    		   ### Exclude articles that person EVER Read, look through ALL articles read this week 
	    		articles_read_ids = user.articles.where("? = ANY(articles.topic_tags)", top_topic).pluck(:id) 
	    		unread_articles = Article.where("created_at > ? AND id NOT IN (?) AND ? = ANY(articles.topic_tags)", one_week_ago, articles_read_ids, top_topic)
	    		article_ids_to_recommend = []
	    		unread_articles.each do |article|
	    			include_as_option = false
	    			article.reading_logs.each do |log|
						include_as_option = true if log.time_read > one_week_ago
	    			end 
	    			article_ids_to_recommend.push(article.id) if include_as_option
	    		end 
	    		recommended_articles = Article.where("id IN (?)", article_ids_to_recommend).order("reading_logs_count DESC").limit(3)
	    		output_hash['recommended_articles'] = []
	    		recommended_articles.each do |article|
	    			article_hash = {}
					
	    			article_hash["cover_image_url"] = article.cover_image_url
	    			article_hash["title"] = article.title
	    			article_hash["domain"] = article.domain
	    			article_hash["url"] = article.url
					output_hash['recommended_articles'].push(article_hash)
	    		end 
    		end 
			

			#### Articles in common with others  
			article_ids_that_user_and_others_have_read = user.articles.where("time_read > ? AND reading_logs_count > ?", one_week_ago, 1).pluck(:article_id).uniq
			articles_others_read_last_week = ReadingLog.where("time_read > ? AND user_id != ?", one_week_ago, user.id).pluck(:article_id).uniq
			articles_in_common_last_week_only = (articles_others_read_last_week & article_ids_that_user_and_others_have_read)
			output_hash["articles_read_others_ever_read"] = article_ids_that_user_and_others_have_read.length
			output_hash["articles_read_others_read_last_week_only"] = articles_in_common_last_week_only.length
			
			users_with_articles_in_common_last_week = ReadingLog.where("time_read > ? AND article_id IN (?) AND user_id != ? AND user_id != ?", one_week_ago, articles_in_common_last_week_only, user.id, 43).pluck(:user_id).uniq
			output_hash["number_of_users_with_articles_in_common_last_week"] = users_with_articles_in_common_last_week.length
			
			#### Users with most words read in common
			common_reading_logs = ReadingLog.where("time_read > ? AND article_id IN (?) AND user_id != ? AND user_id != ?", one_week_ago, articles_in_common_last_week_only, user.id, 43)
			overlapping_reading_hash = {}
			common_reading_logs.each do |log|
				overlapping_reading_hash[log.user_id] ||= 0
				overlapping_reading_hash[log.user_id] += log.article.word_count.to_i
			end 
			users_sorted = overlapping_reading_hash.sort_by{|user_id, word_count| word_count}.reverse
			if users_sorted 
				first_user_id = users_sorted[0][0] if users_sorted[0]
				second_user_id = users_sorted[1][0] if users_sorted[1]
				users_arr = [first_user_id, second_user_id]
				output_hash['users_with_most_common_reading'] = []
				users_arr.each do |user_id|
					if user_id && common_user = User.find(user_id)
						user_hash = {}
						user_hash["first_name"] = common_user.first_name
						user_hash["last_name"] = common_user.last_name
						
						if common_user.avatar.url == "/images/profile.png" 
							user_hash["image_url"] = "https://s3.amazonaws.com/roundview4/static_assets/profile.png"
						else 
							user_hash["image_url"] = common_user.avatar.url
						end 
						user_hash["profile_url"] = "https://roundview.co/#/users/#{common_user.id}/1/summary"
						
						
						common_article_ids = ReadingLog.where("time_read > ? AND (user_id = ? OR user_id = ?)", one_week_ago, user.id, common_user.id).pluck(:article_id).uniq
						topic_tags_array = Article.where("id IN (?)", common_article_ids).pluck(:topic_tags, :word_count)
						top_shared_topics = user.top_topics_array_from_tags(topic_tags_array)
						top_common_topic = top_shared_topics[0][:topic] if top_shared_topics && top_shared_topics[0]
						user_hash["top_common_topic"] = top_common_topic
						user_hash["top_common_topic_profile_url"] = "https://roundview.co/#/users/#{common_user.id}/1/summary?topic_tag=#{top_common_topic}"
						output_hash['users_with_most_common_reading'].push(user_hash)
					end
				end 	
			end

			if previously_read_books_hash["user_#{user.id}"]
				previously_read_books = previously_read_books_hash["user_#{user.id}"]
			else 
				previously_read_books = []
			end 
			
			book_hash = match_book(books_array, number_of_words, previously_read_books)
			previously_read_books.push(book_hash['book_title']) 
			previously_read_books_hash["user_#{user.id}"] = previously_read_books
			$redis.set("previously_read_books_hash", previously_read_books_hash.to_json)
			output_hash.merge!(book_hash)
				
			output_hash['book_twitter_share_link'] = "https://twitter.com/share?url=" + URI.encode("https://roundview.co/#/users/#{user.id}/1/summary") + "&text=" + URI.encode("I read #{number_of_words_formatted} words last week. That is like reading #{book_hash['book_title']}") + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 
			if output_hash['top_categories'] && output_hash['top_categories'].length > 0 
				output_hash['has_top_categories'] = true 
			else 
				output_hash['has_top_categories'] = false 
			end 
			top_line_hash['data'] = output_hash	
			response = HTTParty.post(request_url, :body => {contacts: [top_line_hash]}.to_json, :headers => {'Authorization' => auth_header})
			user_knowtify_email_start = $redis.set("user_knowtify_email_start", user.id)
		end 
	end 

  	def books_list_descending_word_count
		books_array = Array.new
		csv_text = File.read("books.csv")
		csv = CSV.parse(csv_text, :headers => true)
		csv.each { |row| books_array.push(row.to_hash)}
		sorted_array = books_array.sort_by { |hash| hash["words"].to_i }.reverse
		return sorted_array
	end 

	def match_book(books_list, word_count, books_to_avoid = [])
		books_list.each do |book_hash|
			book_words = book_hash['book_words'].to_i
			if word_count > book_words && !books_to_avoid.include?(book_hash['book_title'])
				share_of_book_read = (word_count.to_f / book_words).round(1)
				share_of_book_read = 1 if share_of_book_read > 0.75 && share_of_book_read < 1.25
				book_hash['share_of_book_read'] = share_of_book_read
				share_of_book_read >= 1 ? book_hash['show_book_stat'] = true  : book_hash['show_book_stat'] = false
				book_hash['book_stat_to_show'] = "#{share_of_book_read} #{'time'.pluralize(share_of_book_read)}"
				return book_hash
			end 
		end 
		
		books_list.reverse.each do |book_hash| 
			if !books_to_avoid.include?(book_hash['book_title'])
				book_words = book_hash['book_words'].to_i
				share_of_book_read = (word_count.to_f / book_words).round(1)
				share_of_book_read = 1 if share_of_book_read > 0.75 && share_of_book_read < 1.25
				book_hash['share_of_book_read'] = share_of_book_read
				share_of_book_read >= 1 ? book_hash['show_book_stat'] = true  : book_hash['show_book_stat'] = false
				book_hash['book_stat_to_show'] = "#{share_of_book_read} #{'time'.pluralize(share_of_book_read)}"
				return book_hash
			end 
		end 
	end 
    
	def percentile(array, value)
		length = array.length
		index = array.index(value)
		percentile = index.to_f / length * 100
	end 

	def topic_percentile_hash(since_time)
		###### Category in top decile 
		users_hash = Hash.new
		topics_hash = Hash.new

		logs = ReadingLog.where("time_read > ?", since_time)
		logs.each do |log|
			user_id = log.user_id
			if log.article
				word_count = log.article.word_count.to_i 
				topic_tags = log.article.topic_tags
				users_hash[user_id] ||= Hash.new(0)
				topic_tags.each {|topic| users_hash[user_id][topic] += word_count } if topic_tags
			end 
		end 

		users_hash.each do |user_id, user_topics_hash|
			user_topics_hash.each do |topic, word_count| 
				topics_hash[topic] ||= [] 
				topics_hash[topic].push(word_count)

			end 
		end 

		topics_hash.each do |topic, array|
			topics_hash[topic] = array.sort
		end 
		return topics_hash
	end 

	def to_excel
		CSV.open("users_weekly_email_test.csv", "a") { |csv| contacts.each { |contact| csv << [contact['name'], contact['email']].concat(concat['data'].values) }}
			
	end 

	def create_books_read_hash
		books_array = books_list_descending_word_count
		previously_read_books_hash = Hash.new
		start_time = Time.at(1415023266)
		User.where("claimed_profile = ? AND reading_logs_count > ?", true, 0).each do |user|
			number_of_words = user.articles.where("time_read < ? AND time_read > ?", start_time, start_time - 1.week).sum(:word_count)
			book_hash = match_book(books_array, number_of_words)
			if book_hash['show_book_stat']
				previously_read_books_hash["user_#{user.id}"] = [book_hash['book_title']]
			end 
		end 
		return previously_read_books_hash
	end 

	def books_read_to_excel(previously_read_books_hash)
		CSV.open("previously_read_books.csv", "a") do |csv|
			previously_read_books_hash.each do |user_id, books_array|
				csv << [user_id, books_array]
			end 
		end 
	end 

	def import_books_read_from_excel
		csv_text = File.read("previously_read_books.csv")
		csv = CSV.parse(csv_text)
		previously_read_books_hash = Hash.new
		csv.each do |row| 
			previously_read_books_hash[row[0]] = JSON.parse(row[1])
		end 
		return previously_read_books_hash
	end 

	def send_to_knowtify(contacts)
		base_url = "http://www.knowtify.io/api/v1"
		request_url = base_url + "/contacts/upsert"
		auth_header = "Token token=\"#{ENV['KNOWTIFY_API_TOKEN']}\"" 
		number_of_contacts = contacts.length 
		start_index = 0 
		end_index = 0 
		while end_index < number_of_contacts
			end_index = start_index + 199
			contacts_to_send = contacts[start_index..end_index]
			response = HTTParty.post(request_url, :body => {contacts: contacts_to_send}.to_json, :headers => {'Authorization' => auth_header})
			start_index += 200
		end 
	end 

	def separate_comma(number)
	  number.to_s.chars.to_a.reverse.each_slice(3).map(&:join).join(",").reverse
	end
end 	