class DataExporter
	# require 'utility/data_exporter'
	
	def initialize 
	end 

	def export		
		auth = {:username =>  ENV['RELATEIQ_API_KEY'], :password => ENV['RELATEIQ_SECRET_KEY']}

		# lists = HTTParty.get("https://api.relateiq.com/v2/lists", :basic_auth => auth)
		# list_id = "546504dce4b011f70953d076"
		# add_to_list = HTTParty.post("https://api.relateiq.com/v2/lists/#{list_id}/listitems", :body => {"id" => add_contact['id']}, :basic_auth => auth)

		CSV.open("user_relate_iq.csv", "a") do |csv|
			csv << ["user.id", "user.name", "user.email", "experience", "company_name", "job_title", "address"]
			User.where(claimed_profile: true).find_each do |user|
				begin 
					contact = {name: [{value:user.name}], email: [{value:user.email}]}
					add_contact = HTTParty.post("https://api.relateiq.com/v2/contacts", :body => {"properties" => contact}.to_json, :basic_auth => auth, :headers => { 'Content-Type' => 'application/json' })
					# contact_id = add_contact['id']
					# get_contact = HTTParty.get("https://api.relateiq.com/v2/contacts/#{contact_id}", :basic_auth => auth)
					if add_contact['properties'] && add_contact['properties']['experience'] && add_contact['properties']['experience'].length > 0
						experience = add_contact['properties']['experience'][0]['value']
						company_name = add_contact['properties']['experience'][0]['metadata']['company_name']
						job_title = add_contact['properties']['experience'][0]['metadata']['job_title']
					else 
						experience = "unknown"
						company_name = "unknown"
						job_title = "unknown"
					end 

					if add_contact['properties'] && add_contact['properties']['address']
						address = add_contact['properties']['address'][0]['value']
					else 
						address = "unknown"
					end 
					csv << [user.id, user.name, user.email, experience, company_name, job_title, address]
				rescue StandardError => e 
					debugger
					puts "error"
				end 
			end 
		end 
	end 

	def full_contact
		contact = HTTParty.get("https://api.fullcontact.com/v2/person.json", :query => {email: "jens.kohl@gmail.com", apiKey: "ee19ab26c3b553c5"})
	end 

	def mixpanel_events_to_excel
		client = Mixpanel::Client.new(api_key: ENV['MIXPANEL_PRODUCTION_API_KEY'], api_secret: ENV['MIXPANEL_PRODUCTION_SECRET_KEY'])
		events = client.request('export', from_date: '2014-4-1', to_date: '2014-11-13')
		keys_array = ["name"]
		
		CSV.open("mixpanel_events.csv", "a") do |csv|
			events.each do |event|
				name = event['event']
				output_array = [name]
				event['properties']['time'] = Time.at(event['properties']['time'])
				event['properties'].each do |key, value| 
					keys_array.push(key) unless keys_array.include?(key) 
					key_index = keys_array.index(key)
					output_array[key_index] = value
				end 
				csv << output_array
			end 
			csv << keys_array
		end 
	end 

	def mixpanel_people_to_excel
		client = Mixpanel::Client.new(api_key: ENV['MIXPANEL_PRODUCTION_API_KEY'], api_secret: ENV['MIXPANEL_PRODUCTION_SECRET_KEY'])
		people = client.request('engage', {})
		keys_array = ["user_id"]
		CSV.open("mixpanel_people.csv", "a") do |csv|
			people['results'].each do |person|
				user_id = person['$distinct_id']
				output_array = [user_id]
				person['$properties'].each do |key, value|
					keys_array.push(key) unless keys_array.include?(key) 
					key_index = keys_array.index(key)
					output_array[key_index] = value
				end 
				csv << output_array
			end 
			csv << keys_array
		end 
	end 

	def growth_metrics
		output_hash = Hash.new
		User.where(claimed_profile: true).find_each do |user|
			week_created = user.created_at.strftime("%W")
			output_hash['all_users'] ||= Hash.new
			output_hash['all_users'][week_created] ||= 0
			output_hash['all_users'][week_created] += 1 
			
			if user.authorizations.where("provider IN (?)", ["instapaper", "pocket", "readability"]).size > 0 
				output_hash['reading_app_connected'] ||= Hash.new
				output_hash['reading_app_connected'][week_created] ||= 0
				output_hash['reading_app_connected'][week_created] += 1
				
				if user.sign_in_count > 1 
					output_hash['reading_app_connected_and_been_back'] ||= Hash.new
					output_hash['reading_app_connected_and_been_back'][week_created] ||= 0
					output_hash['reading_app_connected_and_been_back'][week_created] += 1
				end 


				if user.reading_logs_count > 0 
					output_hash['reading_app_connected_and_has_reading_logs'] ||= Hash.new
					output_hash['reading_app_connected_and_has_reading_logs'][week_created] ||= 0
					output_hash['reading_app_connected_and_has_reading_logs'][week_created] += 1
					if user.sign_in_count > 1 
						output_hash['reading_app_connected_and_has_reading_logs_and_been_back'] ||= Hash.new
						output_hash['reading_app_connected_and_has_reading_logs_and_been_back'][week_created] ||= 0
						output_hash['reading_app_connected_and_has_reading_logs_and_been_back'][week_created] += 1
					end 
				end 
			else
			 	if user.reading_logs_count > 0 
					output_hash['no_reading_app_but_reading_logs'] ||= Hash.new
					output_hash['no_reading_app_but_reading_logs'][week_created] ||= 0
					output_hash['no_reading_app_but_reading_logs'][week_created] += 1
					if user.sign_in_count > 0 
						output_hash['no_reading_app_but_reading_logs_and_been_back'] ||= Hash.new
						output_hash['no_reading_app_but_reading_logs_and_been_back'][week_created] ||= 0
						output_hash['no_reading_app_but_reading_logs_and_been_back'][week_created] += 1
					end 
				end 
			end  
		end 
		return output_hash
	end 

	def metrics_to_excel(output_hash)
		CSV.open("growth_metrics.csv", "a") do |csv|
			output_hash.each do |metric_type, data_hash|
				output_array = Array.new(46)
				data_hash.each do |week, user_number|
					output_array[week.to_i] = user_number
				end 
				output_array.insert(0, metric_type)
				csv << output_array
			end 
		end 
	end 

	def lookup_table
		CSV.open("users_all_info.csv", "a") do |csv|
			csv << User.first.attributes.keys
			User.find_each {|user| csv << user.attributes.values}
		end 
	end 
end 