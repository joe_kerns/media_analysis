class StatsExporter
# require 'utility/stats_exporter'
	def initialize
	end 

	def reading_logs_per_week
		users_reading_hash= Hash.new 
		first_date = Time.at(1216909442)
		ReadingLog.where("time_read > ?", 10.years.ago).find_each do |log|
			week_of_reading = log.time_read.strftime("%W %G")
			users_reading_hash[log.user_id] ||= Hash.new(0) 
			users_reading_hash[log.user_id][week_of_reading] += 1
		end 
		return users_reading_hash
	end 

	def export_to_excel(users_reading_hash)
		
		weeks_array = Array.new
		users_reading_hash.each {|user_id, weeks_hash| weeks_array.concat(weeks_hash.keys)}
		weeks_array.uniq! 
		array_of_hashes = Array.new
		weeks_array.each  do |week| 
			arr = week.split(" ")
			hash = {week: arr[0], year: arr[1], key: week} if arr
			array_of_hashes.push(hash) 
		end 
		sorted_weeks_hashes = array_of_hashes.sort{ |a, b|[a[:year], a[:week]]<=> [b[:year], b[:week]]}

		lookup_hash = {} 
		sorted_weeks_hashes.each_with_index  {|hash, index| lookup_hash[hash[:key]] = index }

		CSV.open("users_weekly_reading_logs.csv", "a") do |csv|
			users_reading_hash.each do |user_id, weeks_hash|
				output_array = Array.new(330)
				weeks_hash.each do |week, articles_read|
					index = lookup_hash[week]
					output_array[index] = articles_read
				end 
				output_array.insert(0, user_id)
				csv << output_array
			end 
		end 
	end 
end 