class Metrics
	# require 'utility/metrics'
	# Metrics.user_growth
	def self.user_growth
		output_hash = {list_of_weeks: [], week_start_date: {}, cumulative_users: {}, cumulative_activated_users: {}, user_growth_from_previous_week: {}, activated_user_growth_from_previous_week: {}}
		time = User.first.created_at.to_date
		week_number = 0 
		reading_logs_threshold = 0 
		old_cumulative_users = nil
		old_cumulative_activated_users = nil
		while time < Time.now
			week_number += 1 
			time += 1.week
			output_hash[:list_of_weeks].push("week_#{week_number}".to_sym)
			output_hash[:week_start_date]["week_#{week_number}".to_sym] = time.beginning_of_week.to_s
			new_cumulative_users = User.where("claimed_profile = ? AND created_at < ?", true, time).count
			new_cumulative_activated_users = User.where("claimed_profile = ? AND created_at < ? AND reading_logs_count > ?", true, time, reading_logs_threshold).count
			output_hash[:cumulative_users]["week_#{week_number}".to_sym] = new_cumulative_users
			
			output_hash[:cumulative_activated_users]["week_#{week_number}".to_sym] = new_cumulative_activated_users
			if old_cumulative_users
				output_hash[:user_growth_from_previous_week]["week_#{week_number}".to_sym] = (new_cumulative_users- old_cumulative_users).to_f / old_cumulative_users 
				output_hash[:activated_user_growth_from_previous_week]["week_#{week_number}".to_sym] = (new_cumulative_activated_users - old_cumulative_activated_users).to_f / old_cumulative_activated_users
			end 
			old_cumulative_users = new_cumulative_users
			old_cumulative_activated_users = new_cumulative_activated_users
		end 
		return output_hash
	end 

	def self.old_metrics_calc
		# Get first week, go through 
		User.where(claimed_profile: true).group("date(created_at)")
		User.where(claimed_profile: true).find_each do |user|
			week_created = user.created_at.strftime("%W")
			output_hash['all_users'] ||= Hash.new
			output_hash['all_users'][week_created] ||= 0
			output_hash['all_users'][week_created] += 1 
			
			if user.authorizations.where("provider IN (?)", ["instapaper", "pocket", "readability"]).size > 0 
				output_hash['reading_app_connected'] ||= Hash.new
				output_hash['reading_app_connected'][week_created] ||= 0
				output_hash['reading_app_connected'][week_created] += 1
				
				if user.sign_in_count > 1 
					output_hash['reading_app_connected_and_been_back'] ||= Hash.new
					output_hash['reading_app_connected_and_been_back'][week_created] ||= 0
					output_hash['reading_app_connected_and_been_back'][week_created] += 1
				end 


				if user.reading_logs_count > 0 
					output_hash['reading_app_connected_and_has_reading_logs'] ||= Hash.new
					output_hash['reading_app_connected_and_has_reading_logs'][week_created] ||= 0
					output_hash['reading_app_connected_and_has_reading_logs'][week_created] += 1
					if user.sign_in_count > 1 
						output_hash['reading_app_connected_and_has_reading_logs_and_been_back'] ||= Hash.new
						output_hash['reading_app_connected_and_has_reading_logs_and_been_back'][week_created] ||= 0
						output_hash['reading_app_connected_and_has_reading_logs_and_been_back'][week_created] += 1
					end 
				end 
			else
			 	if user.reading_logs_count > 0 
					output_hash['no_reading_app_but_reading_logs'] ||= Hash.new
					output_hash['no_reading_app_but_reading_logs'][week_created] ||= 0
					output_hash['no_reading_app_but_reading_logs'][week_created] += 1
					if user.sign_in_count > 0 
						output_hash['no_reading_app_but_reading_logs_and_been_back'] ||= Hash.new
						output_hash['no_reading_app_but_reading_logs_and_been_back'][week_created] ||= 0
						output_hash['no_reading_app_but_reading_logs_and_been_back'][week_created] += 1
					end 
				end 
			end  
		end 
		return output_hash
	end 
end 