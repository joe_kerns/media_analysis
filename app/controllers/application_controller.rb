class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :redirect_to_apex if Rails.env.production?
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  after_filter :set_csrf_cookie_for_ng
  
  private

  def redirect_to_apex
      domain_to_redirect_to = 'roundview.co'
      domain_exceptions = ['roundview.co', 'www.roundview.co', 'roundview4-staging.herokuapp.com']
      should_redirect = !(domain_exceptions.include? request.host)
      new_url = "#{request.protocol}#{domain_to_redirect_to}#{request.fullpath}"
      redirect_to new_url, status: :moved_permanently if should_redirect
  end

  def set_csrf_cookie_for_ng
  	cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  def verified_request?
  	super || form_authenticity_token == 
  		request.headers['HTTP_X_XSRF_TOKEN']
  end


  def signed_in_root_path(resource_or_scope)
    if resource_or_scope.is_a?(User) && session[:extension] == "chrome"
      session[:extension] = nil
      extension_login_path
    else 
      super
    end 
  end 
  
  def after_sign_in_path_for(resource)
    if resource.is_a?(User)
      if session[:extension] == "chrome"
        extension_login_path
      else 
        authenticated_root_path
      end 
    else
      super
    end

    if resource.is_a?(Admin)
      admin_users_path
    else
      super
    end
  end

   def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up)  {|u| u.permit(:first_name, :last_name, :email, :password, :password_confirmation, :avatar)} 
    devise_parameter_sanitizer.for(:account_update)  {|u| u.permit(:first_name, :last_name, :email, :password, :password_confirmation, :avatar, :reading_archive_visible)} 
  end
end
