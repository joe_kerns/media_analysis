class WelcomeController < ApplicationController
	before_filter :authenticate_user!, only: [:dashboard]
	layout :choose_layout, only: [:index, :dashboard]
	skip_before_filter :verify_authenticity_token, only: :semantria
	# protect_from_forgery only: :semantria, with: :null_session
	respond_to :json

	def index
		@blog_posts = JSON.parse($redis.get("recent_blog_posts")) if $redis.get("recent_blog_posts")
	end

	def dashboard
		@user = current_user 
		authorization_options = Authorization.authorization_options
		@user.authorizations.each do |auth|
			authorization_options.each do |provider|
				if auth.provider == provider[:provider]
					job = Object.const_get(provider[:name] + "Updater")
        			Resque.enqueue(job, @user.id)
				end 
			end 
		end 
	end

	def about
	end

	def terms
	end

	def privacy
	end

	def faqs
	end 

	def semantria
		results = params['_json']
		puts "TIM: this is the results: #{results}"
		results.each do |result|
            begin
                person_entities = Array.new
                company_entities = Array.new
                product_entities = Array.new
                place_entities = Array.new
                job_title_entities = Array.new
                other_entities = Array.new
                semantria_categories = Array.new
                semantria_subcategories = Array.new
                semantria_topics = Array.new
                topic_tags = Array.new
                article_id = result['id']
                article_to_update = Article.find(article_id)
                summary = result['summary']

                if result['auto_categories']
                    semantria_categories = result['auto_categories'].sort_by {|k| k["strength_score"]}.reverse
                    semantria_categories.each do |auto_category|
                        if auto_category['categories']
                            auto_category['categories'].each do |subcategory|
                                semantria_subcategories.push(subcategory)
                            end 
                        end 
                    end 
                    semantria_subcategories.sort_by! {|k| k["strength_score"]}.reverse!
                end 

                if result['topics']
                    semantria_topics = result['topics'].sort_by {|k| k["strength_score"]}.reverse
                end 

                if result['entities']
                    result['entities'].each do |entity|
                        entity_name = entity['title']
                        unless entity_name.length > 35
                            case entity['entity_type']
                            when "Person"
                              person_entities.push(entity_name) unless person_entities.include? entity_name
                            when "Company"
                              company_entities.push(entity_name) unless company_entities.include? entity_name
                            when "Product"
                              product_entities.push(entity_name) unless product_entities.include? entity_name
                            when "Place"
                              place_entities.push(entity_name) unless place_entities.include? entity_name
                            when "Job Title"
                              job_title_entities.push(entity_name) unless job_title_entities.include? entity_name
                            else 
                              other_entities.push(entity_name) unless other_entities.include? entity_name
                            end 
                        end 
                  end 
                end 
				puts "HERE IS THE article being updated: #{article_to_update.id}"
                article_to_update.update_attributes(categorized_flag: true, summary: summary, person_entities: person_entities, company_entities: company_entities, product_entities: product_entities, place_entities: place_entities, job_title_entities: job_title_entities, other_entities: other_entities, semantria_categories: semantria_categories, semantria_subcategories: semantria_subcategories, semantria_topics: semantria_topics)
                article_to_update.create_topic_tags
            rescue StandardError => e
                Rails.logger.error("Semantria Retriever Error with article number: #{article_id}, error: #{e}")
                Rails.logger.error("Error source: #{e.backtrace[0]}")
            end 
        end
        render json: {status: :success}
	end

	def choose_layout
		user_signed_in? ? "angular" : "application"
	end

	def goodbye
	end 

	def chrome_extension_installed
		@user = current_user || nil
		if @user 
			render layout: "authenticated"
		else 
			render layout: "application"
		end 
	end 

end

