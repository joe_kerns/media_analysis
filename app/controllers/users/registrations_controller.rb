class Users::RegistrationsController < Devise::RegistrationsController
	before_filter :authenticate_user!, only: [:edit, :update, :add_email, :ask_for_email]
	layout :choose_layout
	
	def ask_for_email
		@user = current_user
	end 

	def add_email
		account_update_params = devise_parameter_sanitizer.sanitize(:account_update)
		if User.find_by_email(account_update_params[:email])
			flash[:email_duplicate] = "That email address is already taken. Please try again!"
			redirect_to ask_for_email_path
		else 
			@user = current_user
			@user.update_attributes(account_update_params)
			redirect_to "/#/connect_accounts"
		end 
		
	end 

	def update
	    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)
	    
	    # required for settings form to submit when password is left blank
	    if account_update_params[:password].blank?
	      account_update_params.delete("password")
	      account_update_params.delete("password_confirmation")
	    end

	    @user = User.find(current_user.id)
	    if @user.update_attributes(account_update_params)
	      set_flash_message :notice, :updated
	      # Sign in the user bypassing validation in case his password changed
	      sign_in @user, :bypass => true
	      redirect_to after_update_path_for(@user)
	    else
	      render "edit"
	    end
  	end

	def destroy
		AdminMailer.deleted_user(resource.id).deliver
	    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
	    set_flash_message :notice, :destroyed if is_flashing_format?
	    yield resource if block_given?
	    respond_with_navigational(resource){ redirect_to deleted_account_path }
	end


	def choose_layout
		user_signed_in? ? "authenticated" : "application"
	end


	def after_sign_up_path_for(resource)
	  if resource.is_a?(User)
	    "/#/connect_accounts"
	  else
	    super
	  end
	end

	def after_update_path_for(resource)
	  if resource.is_a?(User)
	    "/#/users/#{resource.id}"
	  else
	    super
	  end
	end 

end 