class Users::UsersController < Devise::SessionsController
	before_filter :authenticate_user!
	layout 'authenticated', :only => [:extension_login]
	respond_to :json
		
	def data_update_status
		reject_if_not_authorized_request!
		@user = current_user
		work_left = @user.articles_to_process.to_i - @user.articles_processed.to_i  
		if (work_left > 0) || (@user.articles.size == 0)
			mid_update = true 
		else 
			mid_update = false 
		end 
		render json: {articles_to_process: @user.articles_to_process.to_i, articles_processed: @user.articles_processed.to_i, mid_update: mid_update}
	end 

	def discover
		reject_if_not_authorized_request!
		page = params[:page].to_i
		per_page = 8
		offset = per_page * (page - 1)
		total_users = User.where('discover_score > ?', 0).size
		@users = User.where('discover_score > ?', 0).limit(per_page).offset(offset).order("discover_score DESC", :id)
		render json: @users,  :meta => {:total => total_users}
	end 

	def show 
		reject_if_not_authorized_request!
		user = User.find(params[:id])
		user == current_user ? signed_in_user = true : signed_in_user = false
		if signed_in_user || user.reading_archive_visible 
			show_topic_links = true 
		else 
			show_topic_links = false
		end 

		@user_hash = {
			id: user.id,
			first_name: user.first_name,
			last_name: user.last_name,
			reading_archive_visible: user.reading_archive_visible, 
			profile_photo: user.avatar.url(:medium), 
			signed_in_user: signed_in_user,
			show_topic_links: show_topic_links,
			claimed_profile: user.claimed_profile
		}
		render json: @user_hash
	end 

	def stats 
		reject_if_not_authorized_request!
		@user = User.find(params[:id])
		time_period = defined_time_period(params[:timePeriod])
		topic_tag = params[:topic_tag]
		total_articles = @user.total_articles(time_period, topic_tag)
		top_topics = @user.top_topics(time_period, 25)
		weekly_average = @user.average_weekly_word_count(time_period, topic_tag)
		render json: {total_articles: total_articles, weekly_average: weekly_average, top_topics: top_topics}	
	end 

	def common_articles
		reject_if_not_authorized_request!
		@user = User.find(params[:id])
		page = params[:page].to_i
		category = params[:category]
		time_period = defined_time_period(params[:timePeriod])
		topic_tag = params[:topic_tag]
		if @user == current_user 
			render json: {}
		else 
			case category 
			when "Longest"
				common_articles = current_user.common_articles_long(@user.id, page, time_period, topic_tag)
			when "Most recent"
				common_articles = current_user.common_articles_recent(@user.id, page, time_period, topic_tag)
			else 
				common_articles = nil
			end 
			total_common_articles = current_user.total_common_articles(@user, time_period, topic_tag)
			render json: common_articles, meta: {total_common_articles: total_common_articles}, meta_key: "meta", root: "common_articles", each_serializer: ReadingLogSerializer
		end	
	end 

	def summary 
		reject_if_not_authorized_request!
		@user = User.find(params[:id])
		topic_tag = params[:topicTag]
		time_period = defined_time_period(params[:timePeriod])
		top_authors = @user.top_authors(time_period, topic_tag)
		top_domains = @user.top_domains(time_period, topic_tag)
		user_chart_data = @user.reading_chart_data(topic_tag)
		if @user == current_user
			viewer_chart_data = nil
		else
			viewer_chart_data = current_user.reading_chart_data(topic_tag) 
		end 
		
		render json: {top_authors: top_authors, top_domains: top_domains, user_chart_data: user_chart_data,viewer_chart_data: viewer_chart_data}
		
	end 

	def index
		reject_if_not_authorized_request!
		search_term = params[:query].to_s
		users = User.search(search_term).sort_by { |a| -a.reading_logs_count}
		total_users = users.size
		if users.size > 0 
			render json: users, :meta => {:total => total_users}
		else 
			render json: users, status: 204
		end 

	end 


	def user_articles
		reject_if_not_authorized_request!
		@user = User.find(params[:id])
		topic_tag = params[:topic_tag]
		page = params[:page].to_i	
		time_period = defined_time_period(params[:timePeriod])
		domain = params[:domain]
		author = params[:author]

		if @user == current_user 
			logs = @user.reading_archive_for_self(topic_tag, page, time_period, domain, author)
		else 
			logs = @user.reading_archive_for_others(topic_tag, page, time_period, domain, author)
		end 

		total_articles = @user.calculate_total_articles_for_topic(time_period, topic_tag, author, domain)
		

		render json: logs, meta: {topic_tag: topic_tag, domain: domain, author: author, total_articles: total_articles}, meta_key: "meta", root: "articles", each_serializer: ReadingLogSerializer
	end 

	def extension_login
		@user = current_user || nil
	end 

	def is_user
		reject_if_not_authorized_request!
		render status: 200, 
			json: {
				success: !User.find_by_email(params[:email]).blank?
			}
	end

	def failure
		render status: 401,
			json: {
				success: false,
				info: "Unauthorized"
			}
	end

	private
		
	def defined_time_period(param)
		params_options = {"1" => "last week", "12" => "last 12 weeks", "All" => "lifetime"}
		params_options[param] ? params_options[param] : "last 12 weeks"
	end 

	def reject_if_not_authorized_request!
		warden.authenticate!(
			scope: resource_name,
			recall: "users/users#failure")
	end
end