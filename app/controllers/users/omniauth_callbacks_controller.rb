class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
	def twitter
		auth_hash = request.env["omniauth.auth"]
		uid = auth_hash['uid']
		name = auth_hash['info']['nickname']
		token = auth_hash['credentials']['token']
		secret = auth_hash['credentials']['secret']
		url = "http://twitter.com/#{name}"

		@user = current_user
		auth = @user.authorizations.where(provider: "twitter").first_or_create(uid: uid, token: token, secret: secret, name: name, url: url)
    		
	    Resque.enqueue(TwitterUpdater, @user.id)
	    redirect_to '/#/connect_accounts'
	end

	def facebook
		auth_hash = request.env["omniauth.auth"]
		uid = auth_hash['uid']
		name = auth_hash['info']['name']
		first_name = auth_hash['info']['first_name']
		last_name = auth_hash['info']['last_name']
		email = auth_hash['info']['email']
		auth = Authorization.find_by_provider_and_uid("facebook", uid)
		
		if auth
			## We already know about this user who is signing in with
			## the provider: just return the user associated with the Authorization
			user = auth.user
		else
			## Is a user signed in already? If not, then find or create
			## the user
			if current_user 
				user = current_user
			else 
				if user = User.find_by_email(email)
					user
				else 	
					user = User.create({first_name: first_name,last_name: last_name, password: Devise.friendly_token[0,8],email: email})
					first_time_sign_in = true 
				end
			end
		end 
		### CHeck if user has avatar and if not, use Facebook one 
		time = $redis.get("facebook_avatar_time_check")
		begin
			if user.avatar_updated_at.to_s < time && auth_hash['info']['image']
				avatar_url = auth_hash['info']['image']
				Resque.enqueue(UserAvatarUploader, user.id, avatar_url)
			end
		rescue StandardError => e 
			Rails.logger.error("Facebook omniauth error for user #{user.id}") if user
			Rails.logger.error("#{e}")
			e.backtrace.each {|error| Rails.logger.error("#{error}")}
		end  
			

		## Finally, create an authorization for the current user
		
		if user.authorizations.find_by_provider("facebook")
			auth = user.authorizations.find_by_provider("facebook")
		else 
			auth = user.authorizations.create(provider: "facebook", uid: uid, token: auth_hash['credentials']['token'], name: name, url: auth_hash['extra']['raw_info']['link'])
		end


		if first_time_sign_in && user.email
			sign_in user 
			redirect_to '/#/connect_accounts'
		elsif first_time_sign_in && user.email.nil?
			sign_in user
			redirect_to ask_for_email_path
		elsif user
			old_current, new_current = user.current_sign_in_at, Time.now.utc
	        user.last_sign_in_at     = old_current || new_current
	        user.current_sign_in_at  = new_current
	        user.sign_in_count += 1
			user.save
			sign_in_and_redirect user, :event => :authentification
		else 
			redirect_to :new_user_registration
		end
	end	

	# def youtube
	# 	auth_hash = request.env["omniauth.auth"]
	# 	uid = auth_hash['uid']
	# 	name = auth_hash['info']['name']
	#     @user = current_user

	# 	if @user.authorizations.find_by_provider("youtube")
	# 		auth = @user.authorizations.find_by_provider("youtube")
	# 	else 
	# 		auth = @user.authorizations.build(provider: "youtube", uid: uid)
	# 	end

	# 	auth.update({
	# 		uid: uid, 
	# 		token: auth_hash['credentials']['token'],
	# 		refresh_token: auth_hash['credentials']['refresh_token'],
	# 		secret: auth_hash['credentials']['secret'],
	# 		name: name, 
	# 		url: auth_hash['info']['urls'].values[0]
	# 	})	
	# 	Resque.enqueue(YoutubeUpdater, @user.id)
 #    	redirect_to '/#/connect_accounts'
	# end 

	# def google_books
	# 	auth_hash = request.env["omniauth.auth"]
	# 	uid = auth_hash['uid']
	# 	name = auth_hash['info']['name']
	#     @user = current_user

	# 	if @user.authorizations.find_by_provider("google_books")
	# 		auth = @user.authorizations.find_by_provider("google_books")
	# 	else 
	# 		auth = @user.authorizations.build(provider: "google_books", uid: uid)
	# 	end
		
	# 	auth.update({
	# 		uid: uid, 
	# 		token: auth_hash['credentials']['token'],
	# 		secret: auth_hash['credentials']['secret'],
	# 		refresh_token: auth_hash['credentials']['refresh_token'],
	# 		name: name, 
	# 		url: auth_hash['info']['urls'].values[0]
	# 	})		
	# 	Resque.enqueue(GoogleBooksUpdater, @user.id)
 #    	redirect_to '/#/connect_accounts'
	# end 

	# def vimeo
	# 	auth_hash = request.env["omniauth.auth"]
	# 	uid = auth_hash['uid']
	# 	name = auth_hash['info']['name']
	#     @user = current_user

	# 	if @user.authorizations.find_by_provider("vimeo")
	# 		auth = @user.authorizations.find_by_provider("vimeo")
	# 	else 
	# 		auth = @user.authorizations.build(provider: "vimeo", uid: uid)
	# 	end

		
	# 	auth.update({
	# 		uid: uid, 
	# 		token: auth_hash['credentials']['token'],
	# 		secret: auth_hash['credentials']['secret'],
	# 		refresh_token: auth_hash['credentials']['refresh_token'],
	# 		name: name, 
	# 		url: auth_hash['info']['urls']['vimeo']
	# 	})	

 #    	redirect_to '/#/connect_accounts'	
 #    end

 #    def goodreads
	# 	auth_hash = request.env["omniauth.auth"]
	# 	uid = auth_hash['uid']
	# 	name = auth_hash['info']['name']
	#     @user = current_user

	# 	if @user.authorizations.find_by_provider("goodreads")
	# 		auth = @user.authorizations.find_by_provider("goodreads")
	# 	else 
	# 		auth = @user.authorizations.build(provider: "goodreads", uid: uid)
	# 	end
		
	# 	auth.update({
	# 		uid: uid, 
	# 		token: auth_hash['credentials']['token'],
	# 		secret: auth_hash['credentials']['secret'],
	# 		refresh_token: auth_hash['credentials']['refresh_token'],
	# 		name: name, 
	# 		url: auth_hash.extra['raw_info'].link
	# 	})	
	# 	Resque.enqueue(GoodreadsUpdater, @user.id)
 #    	redirect_to '/#/connect_accounts'
 #    end
end