class Users::SessionsController < Devise::SessionsController
	before_filter :authenticate_user!, only: [:show_current_user]
	protect_from_forgery only: :show_current_user, with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
	respond_to :json
	
	def new 
		if params[:extension] == "chrome"
			session[:extension] = "chrome"
		end 
		super 
	end 

	def show_current_user
		reject_if_not_authorized_request!
		render status: 200,
		json: {
			success: true, 
			info: "Current user",
			current_user: current_user
		}
	end

	def failure
		render status: 401,
			json: {
				success: false,
				info: "Unauthorized"
			}
	end

	private

	def reject_if_not_authorized_request!
		warden.authenticate!(
			scope: resource_name, 
			recall: "users/users#failure")
	end
end