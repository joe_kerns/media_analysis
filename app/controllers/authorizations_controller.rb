class AuthorizationsController < ApplicationController
    before_filter :authenticate_user!
    layout :choose_layout
    include AuthorizationsHelper
    
    def instapaper_login
        @user = current_user
    end     

    def instapaper_authorization
        request_url = 'https://www.instapaper.com/api/1.1/oauth/access_token'  
        x_auth_username = params[:login]
        x_auth_password = params[:password]
        x_auth_mode = "client_auth"
        params = {x_auth_username: x_auth_username, x_auth_password: x_auth_password, x_auth_mode: x_auth_mode} 
        
        oauth_secret = ""
        oauth_token = ""
        
        consumer_key = ENV['INSTAPAPER_CONSUMER_KEY']
        secret_key = ENV['INSTAPAPER_SECRET_KEY']
        request_method = "POST"
        @user = current_user
        options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
        instapaper_auth = oauth_request(options)
        begin 
            if instapaper_auth.parsed_response.split(/[=&]/)[0] == "oauth_token_secret"
                oauth_secret = instapaper_auth.parsed_response.split(/[=&]/)[1]
                oauth_token = instapaper_auth.parsed_response.split(/[=&]/)[3]   
            else 
                oauth_token = nil
                oauth_secret = nil
                Rails.logger.error("INSTAPAPER AUTH ERROR: #{instapaper_auth}")
                raise "Instapaper Authorization Error"
            end 
        rescue Exception => e
            Rails.logger.error("INSTAPAPER AUTH ERROR: #{instapaper_auth}")
        end 
        if @user.authorizations.find_by_provider("instapaper")
            auth = @user.authorizations.find_by_provider("instapaper")
            auth.update_attributes(token: oauth_token, secret: oauth_secret, name: x_auth_username) if auth.token.nil? && oauth_token
        else 
            auth = @user.authorizations.create(provider: "instapaper", token: oauth_token, secret: oauth_secret, name: x_auth_username)
        end

        Resque.enqueue(InstapaperUpdater, @user.id)
        redirect_to '/#/connect_accounts'
    end 

    def user_list
        @user = current_user
        authorization_options = Authorization.authorization_options

        providers_array = Array.new()
        authorization_options.each do |option|
            if auth = @user.authorizations.find_by_provider(option[:provider])
                auth.token ? token = true : token = false 
                providers_array.push({provider: option[:name], authorized: true, url: option[:provider], token: token })
            else 
                token = false
                providers_array.push({provider: option[:name], authorized: false, url: option[:provider], token: token })
            end 
        end 
        render json: providers_array, :root => false
    end     

    def pocket_auth
        ##### Step 1: Obtain Unauthorized Request Token
            pocketTokenRequestURL = 'https://getpocket.com/v3/oauth/request'
            redirect_url = "http://#{request.host_with_port}/authorizations/pocket_middle"
            user_response = HTTParty.post(pocketTokenRequestURL, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], redirect_uri: redirect_url}, :headers => {'X-Accept' => 'application/json'})
            pocket_token = user_response["code"]
            
            $redis.set("user_#{current_user.id}_pocket_token", pocket_token)
            

        ##### Step Two Obtain User Authorization    
        url = "https://getpocket.com/auth/authorize?request_token=#{pocket_token}&redirect_uri=#{redirect_url}"
        redirect_to url
    end 

	def pocket_middle
        ##### Step Three Obtain Access Token
            pocketAuthorizeURL = 'https://getpocket.com/v3/oauth/authorize' 
            pocket_token = $redis.get("user_#{current_user.id}_pocket_token")
            user_response = HTTParty.post(pocketAuthorizeURL, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], code: pocket_token}, :headers => {'X-Accept' => 'application/json'})
            $redis.del("user_#{current_user.id}_pocket_token")
            token = user_response["access_token"]
            username = user_response["username"]
        ### Finally, create an authorization for the current user
    	    @user = current_user
    		if @user.authorizations.find_by_provider("pocket")
    			auth = @user.authorizations.find_by_provider("pocket")
                auth.update_attributes(token: token, name: username) if auth.token.nil? && token
    		else 
    			auth = @user.authorizations.create(provider: "pocket", name: username, token: token)
    		end
            
            begin 
                if user_response['access_token']
                  Resque.enqueue(PocketUpdater, @user.id)
                else
                    Rails.logger.error("Pocket Authorization Error for user #{current_user.id} and the inappropriate response from Pocket server was #{user_response}")
                    raise "Pocket Authorization Error"
                end  
            rescue Exception => e
                Rails.logger.error("Pocket Authorization Error for user #{current_user.id} and the inappropriate response from Pocket server was #{user_response}")
            end 
        
        
        redirect_to '/#/connect_accounts'
    end 

    def readability_auth
        ##### Step 1: Obtain Unauthorized Request Token
            request_url = 'https://www.readability.com/api/rest/v1/oauth/request_token'
            request_method = "POST"
            oauth_consumer_key = ENV['READABILITY_CONSUMER_KEY']
            secret_key = ENV['READABILITY_SECRET_KEY']
            oauth_secret = ""
            oauth_token = ""
            oauth_callback = "http://#{request.host_with_port}/authorizations/readability_middle"
            options = {params: {}, oauth_token: oauth_token, oauth_secret: oauth_secret, request_url: request_url, oauth_consumer_key: oauth_consumer_key, secret_key: secret_key, request_method: request_method, oauth_callback: oauth_callback}
        
            oauth_response = oauth_request(options)
            response_array = oauth_response.split(/&|=/)
            readability_secret_token = response_array[1]
            $redis.set("user_#{current_user.id}_readability_secret_token",readability_secret_token)
            oauth_token = response_array[3]
            
        ##### Step Two Send User to Readability to obtain permission
            url = "https://www.readability.com/api/rest/v1/oauth/authorize?oauth_token=#{CGI::escape(oauth_token)}"
            redirect_to url
    end 

    def readability_middle
        ##### Step Three Obtain Access Token 
        ### Define required parameters & variables
            oauth_verifier = params[:oauth_verifier] 
            oauth_token = params[:oauth_token]
            oauth_secret = $redis.get("user_#{current_user.id}_readability_secret_token")
            request_url = 'https://www.readability.com/api/rest/v1/oauth/access_token'
            oauth_consumer_key = ENV['READABILITY_CONSUMER_KEY']
            secret_key = ENV['READABILITY_SECRET_KEY']
            request_method = "POST"
            options = {params: {oauth_verifier: oauth_verifier}, oauth_token: oauth_token, oauth_secret: oauth_secret, request_url: request_url, oauth_consumer_key: oauth_consumer_key, secret_key: secret_key, request_method: request_method}    
            ### Make request

            access_token_response = oauth_request(options)
            $redis.del("user_#{current_user.id}_readability_secret_token")
            response_array = access_token_response.split(/&|=/)
            token = response_array[3]
            secret = response_array[1]
        ### Finally, create an authorization for the current user
            @user = current_user
            if @user.authorizations.find_by_provider("readability")
              auth = @user.authorizations.find_by_provider("readability")
              auth.update_attributes(token: token, secret: secret) if auth.token.nil? && token
            else 
              auth = @user.authorizations.create(provider: "readability", token: token, secret: secret)
            end

            begin 
                if response_array[3]
                    Resque.enqueue(ReadabilityUpdater, @user.id)
                else
                    raise "Readability Authorization Error"
                end  
            rescue Exception => e
            end 

            
        redirect_to '/#/connect_accounts'
    end 

    def feedly_auth
        feedly_authenticate_url = 'https://sandbox.feedly.com/v3/auth/auth'
        redirect_url = CGI::escape("http://#{request.host_with_port}/authorizations/feedly/callback")
        scope = CGI::escape('https://cloud.feedly.com/subscriptions')
                
        url = "#{feedly_authenticate_url}?response_type=code&client_id=#{ENV['FEEDLY_CONSUMER_KEY']}&redirect_uri=#{redirect_url}&scope=#{scope}"
        redirect_to url
    end 

    def feedly_callback
        feedly_code = params[:code]
        feedly_token_url = 'https://sandbox.feedly.com/v3/auth/token'
        redirect_url = "http://#{request.host_with_port}/authorizations/feedly/callback"
        user_response = HTTParty.post(feedly_token_url, :body => {code: feedly_code, client_id: ENV['FEEDLY_CONSUMER_KEY'], client_secret: ENV['FEEDLY_SECRET_KEY'], redirect_uri: redirect_url, grant_type: "authorization_code"}, :headers => {'X-Accept' => 'application/json'})
        ### Finally, create an authorization for the current user
            @user = current_user
            token = user_response["access_token"]
            refresh_token = user_response['refresh_token']

            if @user.authorizations.find_by_provider("feedly")
                auth = @user.authorizations.find_by_provider("feedly")
            else 
                auth = @user.authorizations.create(provider: "feedly", uid: user_response['id'], token: token, refresh_token: refresh_token)
            end
            
        Resque.enqueue(FeedlyUpdater, @user.id)
        redirect_to '/#/connect_accounts'
    end 

    def deauthorize 
        provider = params[:provider]
        @user = current_user
        if auth = @user.authorizations.find_by_provider(provider)
            auth.destroy
            render json: {success: true}
        else 
            render json: {success: false}
        end 
    end

    def choose_layout
        user_signed_in? ? "authenticated" : "application"
    end

end
    