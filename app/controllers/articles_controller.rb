class ArticlesController < ApplicationController
	before_filter :authenticate_user!

	def add_to_pocket
		user = current_user
		if user.authorizations.find_by_provider('pocket')
			article = Article.find(params[:article][:id])
			article.add_to_pocket(user)
		end 
		render status: 200, 
	    	json: {
	            success: true
			}
	end 

end 