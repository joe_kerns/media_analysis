class SharesController < ApplicationController
	before_filter :authenticate_user!
	respond_to :json

	def index 
		render json: []
	end 

	def create
		share_params = {from_user_id: current_user.id, article_id: params[:article_id], message: params[:message]}

		if to_user = User.find_by_email(params[:to_user])
			share_params[:to_user_id] = to_user.id 
		else
			share_params[:to_email] = params[:to_user] 
		end
	    if share = Share.create(share_params)
	    	ShareMailer.share_article(share.id).deliver
		end 
	    render status: 200, 
	    	json: {
	            success: share.persisted?,
	            share_id: share.id
			}
	end 
end
