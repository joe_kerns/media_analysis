class ReadingLogsController < ApplicationController
	before_filter :authenticate_user!
	protect_from_forgery only: :create, with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

	def create
		@user = current_user

		if params[:article][:id]
			article = Article.find(params[:article][:id])
		else
			article = Article.create_from_url(params[:article][:url], params[:article][:title])
		end 
		time_read = Time.now 
		time_added = nil 
		reading_source = params[:article][:source] || "Roundview"
		if article 
			log = @user.reading_logs.where(article_id: article.id).first_or_create(article: article, logged_flag: true, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
			tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 
			tracker.track("#{@user.id}", "Article saved from Chrome Extension", {"user_id"=> @user.id, "time" => time_read.to_i, "article_title" => article.title, "article_url" => article.url, "article_id"=> article.id})
			render json: {status: :success}
		else 
			render json: {status: :failure}
		end 
	end 

	def add_to_favorites
		@user = current_user
		favorite = params['article_favorite']
		article_id = params['article_id']
		time_read = Time.now 
		time_added = nil 
		reading_source = "Roundview"
		log = @user.reading_logs.where(article_id: article_id).first_or_create(article_id: article_id, :time_added => time_added, :time_read => time_read, reading_source: reading_source, favorite: favorite)
		render json: {user: @user, article_id: article_id, favorite: favorite}	
	end 

	def mark_hidden
		@user = current_user
		hidden = params['article_hidden']
		article_id = params['article_id']
		log = @user.reading_logs.find_by_article_id(article_id)
		if log.update_attributes(hidden: hidden)
			render json: {article_id: article_id, hidden: hidden, success: true}	
		else 
			render json: {success: false}
		end 
	end 

	def update
		user = current_user
		reading_log_id = params[:id]
		tag = params[:reading_log][:tag]
		tag_action = params[:reading_log][:tag_action]
		reading_log = ReadingLog.find(reading_log_id) 
		article = reading_log.article
		reading_log.user_modified_topics ? topic_tags = reading_log.topic_tags.clone : topic_tags = article.topic_tags.clone
		if tag_action == "add"
			topic_tags.push(tag)
			Resque.enqueue(ClassifierTrainer, article.id, tag)
		elsif tag_action == "delete"
			topic_tags.delete(tag)
		end 
		
		reading_log.update_attributes(topic_tags: topic_tags, user_modified_topics: true)	

    	render json: {}, status: 200
	end 


end 