class ReadingLogSerializer < ActiveModel::Serializer
  attributes :id, :time_read, :favorite, :url, :cover_image_url, :title, :domain, :excerpt, :article_id, :hidden, :topic_tags, :author, :word_count

  	def url
  		object.article.url
	end 

	def cover_image_url
		object.article.cover_image_url
	end 

	def title 
		object.article.title
	end 

	def domain
		object.article.domain.capitalize if object.article.domain
	end 

	def excerpt
		object.article.excerpt
	end

	def article_id
		object.article.id
	end 

	def topic_tags
		if object.user_modified_topics
			return object.topic_tags
		else 	
			return object.article.topic_tags
		end 
	end 

	def author
		object.article.authors[0] if object.article.authors 
	end 

	def word_count
		object.article.word_count
	end 
end
