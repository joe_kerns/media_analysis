class UserSerializer < ActiveModel::Serializer
  	attributes :id, :first_name, :last_name, :reading_logs_count, :avatar_url, :reading_archive_visible, :claimed_profile, :discover_score

	def avatar_url
		object.avatar.url(:medium)
	end 

	# def weekly_average
	# 	object.average_weekly_word_count("last 12 weeks", "All")
	# end 

	# def top_three_topics
	# 	if object.top_topics("last 12 weeks", 3)
	# 		object.top_topics("last 12 weeks", 3)[0..2].map {|item| item['topic']}
	# 	else
	# 		[]	 
	# 	end 
	# end 

	# def articles_last_12_weeks
	# 	object.total_articles("last 12 weeks", "All")
	# end 

end
