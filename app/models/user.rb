class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable
 devise :database_authenticatable, :registerable, :async,
         :recoverable, :rememberable, :trackable, :omniauthable
 before_save do |user| 
 	user.email = email.downcase if user.email
 	user.first_name = first_name.capitalize if user.first_name
 	user.last_name = last_name.capitalize if user.last_name
 	user.name = "#{user.first_name.to_s} #{user.last_name.to_s}"
 end 
 after_save {|user| Resque.enqueue(MixpanelSetter, user.id) if Rails.env.production?}
 after_destroy {|user| user.clean_redis_footprint}
 after_create {|user| Resque.enqueue(NewUserInitializer, user.id)}

 has_many :authorizations, dependent: :destroy
 
 has_many :reading_logs, dependent: :destroy
 has_many :articles, through: :reading_logs

 has_many :shares, foreign_key: 'from_user_id', dependent: :destroy
 has_many :shared_articles, through: :shares, source: :article
 has_many :reverse_shares, foreign_key: 'to_user_id', class_name: "Share", dependent: :destroy
 has_many :received_articles, through: :reverse_shares, source: :article
 
 scope :levenshtein, lambda {|search_term, comparison_term| where(["LEVENSHTEIN(LOWER(#{comparison_term}), LOWER(?)) < 2", search_term])}
 scope :dmetaphone, lambda {|search_term, comparison_term| where(["DMETAPHONE(#{comparison_term}) = DMETAPHONE(?)", search_term])}
 scope :nicknames, lambda {|search_term, comparison_term| where(["LOWER(#{comparison_term}) IN (?)", Nickname.for(search_term).map(&:name)])}

 has_attached_file :avatar, :styles => { :medium => "250x250>", :thumb => "100x100>", :mini => "50x50>" }, :default_url => "/images/profile.png"
 validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

 	@@articles_per_page = 10
	
	def new_user_emails
		AdminMailer.new_user(self.id).deliver
	end 

	def articles_per_page 
		@@articles_per_page
	end 

	def set_discover_score
		unless manual_discover_score 
			if total_articles("last 12 weeks") == 0
				discover_score = 0 
			elsif claimed_profile 
				discover_score = (total_articles("last 12 weeks") / 100.to_f).ceil
			else 
				discover_score = (total_articles("last 12 weeks") / 100.to_f).ceil * 10
			end 
			self.update_attributes(discover_score: discover_score)
		end 
	end 
	
	def reading_archive_for_self(topic_tag, page, time_period, domain = nil, author = nil)
		options = {topic_tag: topic_tag, domain: domain, author: author, order_by: "time_read DESC", hide_null_titles: true}
		query = reading_log_find_by_sql_query(page, time_period, options)
		ReadingLog.find_by_sql(query)
	end 

	def reading_archive_for_others(topic_tag, page, time_period, domain = nil, author = nil)
		if self.reading_archive_visible
			options = {topic_tag: topic_tag, domain: domain, author: author, hide_logs: true, order_by: "time_read DESC", hide_null_titles: true}
			query = reading_log_find_by_sql_query(page, time_period, options)
			return ReadingLog.find_by_sql(query)
		else 
			return nil 
		end 
	end 

	def reading_log_find_by_sql_query(page, time_period, options)
		since_time_read = define_since_time_read(time_period)
		limit = options[:per_page] || articles_per_page 
		offset = limit * (page - 1)
		user_id = options[:comparison_user_id] || self.id
		query = "SELECT reading_logs.* FROM reading_logs INNER JOIN articles ON articles.id = reading_logs.article_id WHERE reading_logs.user_id = ? AND reading_logs.time_read > ? "
		query.concat("AND ? = ANY(articles.topic_tags) ") if options[:topic_tag] && options[:topic_tag] != "All"
		query.concat("AND ? = ANY(articles.authors) ") if options[:author]  
		query.concat("AND articles.domain = ? ") if options[:domain]
		query.concat("AND reading_logs.hidden = ? ") if options[:hide_logs]
		query.concat("AND articles.title IS NOT NULL ") if options[:hide_null_titles]
		query.concat("AND reading_logs.article_id IN (?) ") if options[:comparison_user_id]
		query.concat("ORDER BY #{options[:order_by]} ") if options[:order_by]
		query.concat("LIMIT ? ")
		query.concat("OFFSET ?")
		query_array = [query, user_id, since_time_read]
		query_array.push(options[:topic_tag]) if options[:topic_tag] && options[:topic_tag] != "All"
		query_array.push(options[:author]) if options[:author]
		query_array.push(options[:domain].downcase) if options[:domain]
		query_array.push('f') if options[:hide_logs]
		query_array.push(public_multiread_articles) if options[:comparison_user_id]
		query_array.push(limit)
		query_array.push(offset)
		return query_array
	end 

	def article_where_query(time_period, options)
		since_time_read = define_since_time_read(time_period)
		query = "time_read > ? "
		query.concat("AND ? = ANY(articles.topic_tags) ") if options[:topic_tag] && options[:topic_tag] != "All"
		query.concat("AND ? = ANY(articles.authors) ") if options[:author]  
		query.concat("AND domain = ? ") if options[:domain]
		query.concat("AND reading_logs.hidden = ? ") if options[:hide_logs]
		query.concat("AND article_id IN (?) ") if options[:article_ids_subset]
		query.concat("AND articles.title IS NOT NULL ") if options[:hide_null_titles]
		query_array = [query, since_time_read]
		query_array.push(options[:topic_tag]) if options[:topic_tag] && options[:topic_tag] != "All"
		query_array.push(options[:author]) if options[:author]
		query_array.push(options[:domain].downcase) if options[:domain]
		query_array.push('f') if options[:hide_logs]
		query_array.push(options[:article_ids_subset])  if options[:article_ids_subset]
		return query_array
	end 

	def clean_redis_footprint
		$redis.keys("user_#{self.id}_*").each do |key| 
			$redis.del key
		end 
	end      

	def articles_to_process=(number)
		@articles_to_process = number
		if number == 0 
			$redis.del("user_#{self.id}_articles_to_process")
		else 
			$redis.set("user_#{self.id}_articles_to_process", @articles_to_process)
		end 
	end 

	def articles_to_process
		@articles_to_process ||= $redis.get("user_#{self.id}_articles_to_process")
	end 

	def articles_processed=(number)
		@articles_processed = number
		if number == 0 
			$redis.del("user_#{self.id}_articles_processed")
		else 
			$redis.set("user_#{self.id}_articles_processed", @articles_processed)
		end 
	end 

	def articles_processed
		@articles_processed ||= $redis.get("user_#{self.id}_articles_processed")
	end 

	def self.search(search_term)
	 	terms_array = search_term.split(" ")
	 	output = Array.new
	 	terms_array.each do |term|
	 		term.downcase
	    	output.concat(levenshtein(term, 'first_name') | dmetaphone(term, 'first_name') | nicknames(term, 'first_name') | levenshtein(term, 'last_name') | dmetaphone(term, 'last_name') | where("first_name like ?", "%#{term.capitalize}%") | where("last_name like ?", "%#{term.capitalize}%"))
	    end 
	    output.uniq
	end
	
	def common_articles_long(comparison_user_id, page, time_period, topic_tag)
   		options = {comparison_user_id: comparison_user_id, hide_logs: true, topic_tag: topic_tag, order_by: "word_count DESC", hide_null_titles: true}
   		query = reading_log_find_by_sql_query(page, time_period, options)
   		ReadingLog.find_by_sql(query)
	end
	
	def common_articles_recent(comparison_user_id, page, time_period, topic_tag)
		options = {comparison_user_id: comparison_user_id, hide_logs: true, topic_tag: topic_tag, order_by: "time_read DESC", hide_null_titles: true}
   		query = reading_log_find_by_sql_query(page, time_period, options)
   		ReadingLog.find_by_sql(query)
	end

	def total_common_articles(comparison_user, time_period, topic_tag)
		options = {hide_logs: true, hide_null_titles: true, article_ids_subset: public_multiread_articles, topic_tag: topic_tag}
		query = article_where_query(time_period, options)
		return comparison_user.articles.where(query).size
	end 

	def public_multiread_articles
		self.articles.where('reading_logs_count > ?', 1).pluck(:id)
	end 
	
	def top_authors(time_period, topic = "All", number = 5)
		end_index = number - 1
		# output = $redis.get("user_#{self.id}_#{time_period}_top_authors_#{topic}")
		# if output && JSON.parse(output).length > 0 
		# 	return JSON.parse(output)[0..end_index]
		# else 
			return calculate_top_authors_for_topic(time_period, topic)[0..end_index]
		# end
	end 

	def calculate_top_authors_for_topic(time_period, topic = "All")
		if self.reading_logs_count == 0 
			output_array = []
		else 
			options = {topic_tag: topic}
			query = article_where_query(time_period, options)
			plucked_authors = self.articles.where(query).pluck(:authors, :word_count)
			output_array = top_authors_array_from_plucked_array(plucked_authors)
		end 
		# $redis.set("user_#{self.id}_#{time_period}_top_authors_#{topic}", output_array.to_json)
		return output_array
	end

	def top_authors_array_from_plucked_array(plucked_authors)
		author_frequency_hash = Hash.new(0)
		authors_to_ignore = [nil, "Admin", "Automatico", "Staff Writer", "", "Andrew", "The New Yorker", "American Public Media", "The Daily Muse", "From The Print", "People", "Authors", "From Wikipedia, The Free", "Print", "Once A Month Meals", "View All Posts By Wait But Why →", "Bio", "Fox Sports", "Vanity Fair Magazine", "Netw", "Wikipedia Contributors", "Tinybop Inc.", "Hunterwalk", "Editor's Picks", "On Jezebel", "On Lifehacker", "Spiegel Online, Hamburg, Germany", "Gq Magazine", "First Round Review", "Guest", "Staff", "Photoel", "Wyyt"]

		plucked_authors.each do |authors_entity| 
			unless authors_entity[0].nil? 
				authors_entity[0].each do |author|
					author_name = author
					author_frequency_hash[author_name] += authors_entity[1].to_i unless authors_to_ignore.include?(author_name)
				end 
			end 
		end 
		sorted_authors = author_frequency_hash.sort_by{|author, number| number}.reverse
		output_array = Array.new
		sorted_authors[0..49].each do |author|
			output_hash = {author: author[0], words: author[1]}
			output_array.push(output_hash)
		end 
		return output_array
	end 
	
	def top_domains(time_period, topic = "All", number = 5)
		end_index = number - 1
		# output = $redis.get("user_#{self.id}_#{time_period}_top_domains_#{topic}")
		# if output && JSON.parse(output).length > 0 
		# 	return JSON.parse(output)[0..end_index]
		# else 
			return calculate_top_domains_for_topic(time_period, topic)[0..end_index]
		# end
	end 

	def calculate_top_domains_for_topic(time_period, topic = "All")
		if self.reading_logs_count == 0 
			output_array = []
		else 
			options = {topic_tag: topic}
			query = article_where_query(time_period, options)
			plucked_domains = self.articles.where(query).pluck(:domain, :word_count)
			output_array = top_domains_array_from_plucked_array(plucked_domains, time_period, topic)
		end 

		# $redis.set("user_#{self.id}_#{time_period}_top_domains_#{topic}", output_array.to_json)
		return output_array
	end

	def topics_for_stats_caching
		all_topics = top_topics("lifetime", 50) 
		all_topics.concat(top_topics("last 12 weeks", 50)) if all_topics && top_topics("last 12 weeks", 50)
		all_topics.concat(top_topics("last week", 50)) if all_topics && top_topics("last week", 50)

		if all_topics
			output = all_topics.map{|a| a['topic']}
			output.push("All")
			return output.uniq
		else 
			return nil
		end 
	end 

	def top_domains_array_from_plucked_array(plucked_domains, time_period, topic)
		domain_hash = Hash.new(0)
		plucked_domains.each {|arr| domain_hash[arr[0]] += arr[1].to_i} 
		sorted_domains = domain_hash.sort_by{|domain, number| number}.reverse
		output_array = Array.new
		total_reading = self.total_word_count(time_period, topic).to_f
		sorted_domains[0..49].each do |domain|
			if domain[0] && domain[1] > 0 
				percent_share = (domain[1] / total_reading * 100)
				domain = domain[0].capitalize
				output_hash = {domain: domain, percent_share: percent_share}
				output_array.push(output_hash)
			end 
		end 
		return output_array
	end 

	def total_articles(time_period, topic = "All")
		# output = $redis.get("user_#{self.id}_#{time_period}_total_articles_#{topic}")
		# if output && output.length > 0 
		# 	return output.to_i 
		# else 
			return calculate_total_articles_for_topic(time_period, topic)
		# end
	end

	def calculate_total_articles_for_topic(time_period, topic = "All", author = nil, domain = nil)
		options = {topic_tag: topic, author: author, domain: domain}
		query = article_where_query(time_period, options)
		total_articles = self.articles.where(query).size
		
		# $redis.set("user_#{self.id}_#{time_period}_total_articles_#{topic}", total_articles) unless author || domain 

		return total_articles
	end 

	def total_word_count(time_period, topic = "All")
		# output = $redis.get("user_#{self.id}_#{time_period}_total_word_count_#{topic}")
		# if output && output.length > 0 
		# 	return output.to_i 
		# else 
			return calculate_total_word_count_for_topic(time_period, topic)
		# end
	end 

	def calculate_total_word_count_for_topic(time_period, topic = "All")
		options = {topic_tag: topic}
		query = article_where_query(time_period, options)
		total_articles = self.articles.where(query).sum(:word_count)
		# $redis.set("user_#{self.id}_#{time_period}_total_word_count_#{topic}", total_articles)
		return total_articles
	end 

	def first_day_of_reading(topic = "All")
		# output = $redis.get("user_#{self.id}_first_day_of_reading_#{topic}")
		# if output && output.length > 0
		# 	return output.to_date
		# else 
			return calculate_first_day_of_reading(topic) 
		# end 
	end 

	def calculate_first_day_of_reading(topic = "All")
		options = {topic_tag: topic, order_by: "time_read ASC", per_page: 1}
		page = 1
		time_period = "lifetime"
		query = reading_log_find_by_sql_query(page, time_period, options)
		reading_logs_output = ReadingLog.find_by_sql(query)

		if reading_logs_output.length > 0
			date = reading_logs_output.first.time_read.to_date
			# $redis.set("user_#{self.id}_first_day_of_reading_#{topic}", date)
			return date
		else 
			return nil
		end 
	end 
	
	def average_weekly_word_count(time_period, topic = "All")
		# output = $redis.get("user_#{self.id}_#{time_period}_average_weekly_word_count_#{topic}")
		# if output && output.length > 0 
		# 	return output.to_i 
		# else 
			return calculate_average_weekly_word_count_for_topic(time_period, topic)
		# end
	end 

	def calculate_average_weekly_word_count_for_topic(time_period, topic = "All")
		date_today = Date.today
		since_time_read_date = define_since_time_read(time_period).to_date
		if first_day = first_day_of_reading(topic)
			total_days_reading = date_today - first_day 
			days_in_period = date_today - since_time_read_date
			days_reading = [total_days_reading, days_in_period].min
			weeks_reading = (days_reading.to_f / 7).ceil
			weeks_reading = 1 if weeks_reading == 0
			average = (self.total_word_count(time_period, topic) / weeks_reading).round
		else 
			average = 0
		end 
		# $redis.set("user_#{self.id}_#{time_period}_average_weekly_word_count_#{topic}", average)
		return average 
	end 

	def top_topics(time_period, number)
		end_index = number - 1
		# output = $redis.get("user_#{self.id}_#{time_period}_top_topics")
		# if output && JSON.parse(output)[0..end_index].length > 0
		# 	return JSON.parse(output)[0..end_index] 
		# else 
			return calculate_top_topics(time_period)[0..end_index]
		# end 
	end 

	def calculate_top_topics(time_period)
		if self.reading_logs_count == 0 
			output_array = []
		else 
			options = {}
			query = article_where_query(time_period, options)
			topic_tags_array = self.articles.where(query).pluck(:topic_tags, :word_count)
			output_array = top_topics_array_from_tags(topic_tags_array)
		end 
		# $redis.set("user_#{self.id}_#{time_period}_top_topics", output_array.to_json)
		return output_array
	end 

	def top_topics_array_from_tags(topic_tags_array)
		word_count_by_topic = Hash.new(0)
		total_words_counted = 0
		topic_tags_array.each do |tags_array, word_count|
			if tags_array
				total_words_counted += word_count.to_i
				tags_array.each do |topic_tag|
					if topic_tag 
						word_count_by_topic[topic_tag] += word_count.to_i
					end 
				end 
			end 
		end 
		topics_sorted = word_count_by_topic.sort_by{|topic, words_read| words_read}.reverse
		output_array = Array.new
		topics_sorted[0..49].each do |item| 
			percent_share = (item[1].to_f / total_words_counted * 100)
			output_hash = {topic: item[0], percent_share: percent_share}
			output_array.push(output_hash)
		end 
		return output_array
	end 

	def topic_consecutive_weeks(topic_tag)
		consecutive_weeks = 0 
		articles_read = 1 
		while articles_read > 0 
			articles_read = self.articles.where("time_read < ? AND time_read > ? AND ? = ANY(articles.topic_tags)", consecutive_weeks.weeks.ago, (consecutive_weeks + 1).weeks.ago, topic_tag).count
			consecutive_weeks += 1 if articles_read > 0 
		end 
		return consecutive_weeks
	end 

	def set_mixpanel
		if Rails.env.production? 
			tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 
			if self.authorizations.where("provider IN (?)", ["instapaper", "pocket", "readability"]).size > 0 
				reading_app_connected = true 
			else 
				reading_app_connected = false
			end 
			tracker.people.set("#{self.id}", {
			    "$first_name"=> self.first_name,
			    "$last_name"=> self.last_name,
			    "$created"=> self.created_at,
			    "$email"=> self.email,
			    "sign_in_count"=> self.sign_in_count,
			    "reading_logs_count" => self.reading_logs_count,
			    "reading_app_connected" => reading_app_connected
			})
		end 
	end 

	def set_heap
		if Rails.env.production? 
			heap_app_id = ENV['HEAP_APP_ID'] 
			heap_url = "https://heapanalytics.com/api/identify"
			if self.authorizations.where("provider IN (?)", ["instapaper", "pocket", "readability"]).size > 0 
				reading_app_connected = true 
			else 
				reading_app_connected = false
			end 

			self.reading_logs_count > 0 ? activated_user = true : activated_user = false
			data = {
				app_id: heap_app_id,
				identity: self.email,
				properties: {
					first_name: self.first_name,
					last_name: self.last_name,
					created_at: self.created_at,
					sign_in_count: self.sign_in_count,
					reading_logs_count: self.reading_logs_count,
					reading_app_connected: reading_app_connected,
					activated_user: activated_user,
					claimed_profile: self.claimed_profile,
					current_sign_in_at: self.current_sign_in_at,
					sign_in_count: self.sign_in_count,
					last_sign_in_at: self.last_sign_in_at
				}
			}
			HTTParty.post(heap_url, body: data.to_json, headers: { 'Content-Type' => 'application/json' }) if self.email && self.email != ""
		end 
	end 

	def subscribe_to_mailchimp
		require 'mailchimp'
		mailchimp = Mailchimp::API.new(ENV['MAILCHIMP_API_KEY']) 
		mailchimp.lists.subscribe("0f0459e020", {"email" => self.email},{"FNAME" => self.first_name, "LNAME" => self.last_name}, "html", false) if self.email
		
	end 

	def first_mixpanel_event
		tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 
		tracker.track("#{self.id}", "User Created")
	end 

	def first_heap_event
		if Rails.env.production? 
			heap_app_id = ENV['HEAP_APP_ID'] 
			heap_url = "https://heapanalytics.com/api/track"
			data = {
				app_id: heap_app_id,
				identity: self.email,
				event: "User Created"
			}
			HTTParty.post(heap_url, body: data.to_json, headers: { 'Content-Type' => 'application/json' }) if self.email && self.email != ""
		end 
	end 
	
	def calculate_chart_data_for_topic(topic = "All")
		lifetime_output_array = Array.new
		unless self.reading_logs.size < 1 
			words_per_minute = 250.to_f 		
			words_per_second = words_per_minute / 60
			words_per_millisecond = words_per_second / 1000
			if topic == "All"
				dates_hash = self.articles.group("date(reading_logs.time_read)").sum(:word_count).sort_by{|date, word_count| date}
			else
				dates_hash = self.articles.where("? = ANY (articles.topic_tags)", topic).group("date(reading_logs.time_read)").sum(:word_count).sort_by{|date, word_count| date}
			end 
			dates_hash.each do |arr|
				datetime = arr[0].to_time.to_f * 1000
				milliseconds_reading = arr[1] / words_per_millisecond
				lifetime_output_array.push([datetime, milliseconds_reading])	
			end  
		end 
		# $redis.set("user_#{self.id}_lifetime_reading_chart_data_#{topic}", lifetime_output_array.to_json) 
		return lifetime_output_array
	end 

	def update_all_cached_data
		time_periods.keys.each {|time_period| calculate_top_topics(time_period) }
		topics_for_stats_caching.each do |topic|
			update_cached_data_for_topic(topic)
		end 
	end 

	def update_cached_data_for_topic(topic)
		calculate_first_day_of_reading(topic)
		
		time_periods.keys.each do |time_period|
			calculate_total_articles_for_topic(time_period, topic)
			calculate_total_word_count_for_topic(time_period, topic)
			calculate_average_weekly_word_count_for_topic(time_period, topic)
			calculate_top_domains_for_topic(time_period, topic)
			calculate_top_authors_for_topic(time_period, topic)
		end 
		calculate_chart_data_for_topic(topic)
	end 

	def reading_chart_data(topic = "All")
		# output = $redis.get("user_#{self.id}_lifetime_reading_chart_data_#{topic}")
		# if output && JSON.parse(output).length > 0 
		# 	return JSON.parse(output) 
		# else 
			return calculate_chart_data_for_topic(topic)
		# end 
	end 

	def time_periods
		{"lifetime" => 10.years.ago, "last 12 weeks" => 12.weeks.ago, "last week" => 1.week.ago}
	end 

	def define_since_time_read(time_period)
		options = time_periods
		return options[time_period]
	end 

	def week_created
		self.created_at.strftime('%Y.%W')
	end 
	
end
