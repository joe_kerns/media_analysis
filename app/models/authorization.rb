class Authorization < ActiveRecord::Base
	belongs_to :user
	after_create {|auth| auth.mixpanel_initial_event if Rails.env.production?}
	after_save {|auth| auth.updated_mixpanel_event if Rails.env.production?}

	def refresh_google_token
		refresh_token = self.refresh_token 
		client_id = ENV['GOOGLE_CLIENT_ID']
		client_secret = ENV['GOOGLE_CLIENT_SECRET']
		grant_type = 'refresh_token'

		refresh = HTTParty.post('https://accounts.google.com/o/oauth2/token', body: {refresh_token: refresh_token, client_id: client_id, client_secret: client_secret, grant_type: grant_type})
		token = refresh['access_token']
		self.update_attributes(token: token)
	end 

	def self.authorization_options
		authorization_options = [{name: "Pocket", provider: "pocket"}, 
								{name: "Instapaper", provider: "instapaper"},
                                {name: "Readability", provider: "readability"}, 
                                {name: "Twitter", provider: "twitter"}]
	end 

	def mixpanel_initial_event
		tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 
		if self.token 
			tracker.track("#{self.user_id}", "Provider authorization successful", {"provider" => self.provider, "user_id"=> self.user_id})
		else 
			tracker.track("#{self.user_id}", "Provider authorization failed", {"provider" => self.provider, "user_id"=> self.user_id})
		end 
	end 
	
	def updated_mixpanel_event
		# If this is not the creation of the authorization as evidenced by provider was not nil AND the token changed, track as successful auth 
		if self.provider_was && self.token_changed?
			tracker = Mixpanel::Tracker.new(ENV['MIXPANEL_PROJECT_TOKEN']) 
			tracker.track("#{self.user_id}", "Provider authorization successful", {"provider" => self.provider, "user_id"=> self.user_id})
		end 
	end 
end
