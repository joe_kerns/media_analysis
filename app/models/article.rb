class Article < ActiveRecord::Base
    extend AuthorizationsHelper
	# ASSOCIATIONS 
	has_many :reading_logs, :dependent => :destroy
	has_many :users, through: :reading_logs
	has_many :shares

	# CALLBACKS
	# before_save :update_stats_for_users, if: :article_stats_change? 
	
	def add_to_pocket(user)
		pocket_add_api = 'https://getpocket.com/v3/add'
		auth = user.authorizations.find_by_provider('pocket')
		url = self.url
		HTTParty.post(pocket_add_api, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], access_token: auth.token, url:url})
	end 

	def classifier_words
	    words_string = String.new
	    parsed_url = self.url.split(/\W/).reject(&:empty?).join(" ")
	    if self.meta_keywords 
	      keywords = self.meta_keywords
	      words_string << " " + keywords 
	    end 

	    if self.source_categories
	      parsed_source_categories = self.source_categories.flatten.join(" ")
	      words_string << " " + parsed_source_categories
	    end 
	    if self.publisher
	      publisher = self.publisher 
	      words_string << " " + publisher
	    end 
	    if self.authors
	      authors = self.authors.join(" ")
	      words_string << " " + authors
	    end 
	    words_string << " " + parsed_url
	    words_string << " " + self.title.to_s
	    words_string << " " + self.excerpt.to_s
	    words_string << " " + self.content.to_s
	    return words_string
	end 

	def self.diffbot_parse(url)
		diffbot_token = ENV['DIFFBOT_TOKEN']
    	fields = "tags,humanLanguage"
    	diffbot_url = "http://api.diffbot.com/v3/analyze"
    	
        encoded_url = URI.encode(url)
        begin 
        	diffbot_response = HTTParty.get(diffbot_url, :query => {token: diffbot_token, url: encoded_url, fields: fields, meta: true, timeout: 30000})
        rescue Net::ReadTimeout => e 
        	diffbot_response = {"error" => true}
        end 
        total_articles_parsed = $redis.get("total articles parsed").to_i + 1
        $redis.set("total articles parsed", total_articles_parsed)

        if diffbot_response['error'] 
        	Rails.logger.info("URL: #{url} had a diffbot parser error. It was : #{diffbot_response['error']}")
        	articles_parsed_with_first_error = $redis.get("articles parsed with first error").to_i + 1 
        	$redis.set("articles parsed with first error", articles_parsed_with_first_error)
        	parsing_success = false
        elsif diffbot_response['type'] != 'article' && !diffbot_response['type'].nil?
        	return nil 
        elsif diffbot_response['objects'].nil?  || (diffbot_response['objects'] && diffbot_response['objects'].length < 1)
        	articles_parsed_with_first_error = $redis.get("articles parsed with first error").to_i + 1 
        	$redis.set("articles parsed with first error", articles_parsed_with_first_error)
        	parsing_success = false 
        else 
            parsing_success = true
        end

        if parsing_success 
            if diffbot_response['objects'] && diffbot_response['objects'][0]
            	parsed_article = diffbot_response['objects'][0]
		    	
                if diffbot_response['meta'] && diffbot_response['meta']['og'] && diffbot_response['meta']['og']['og:url']
                    new_url = Article.clean_url(diffbot_response['meta']['og']['og:url'])
                end  
                if diffbot_response['request']['resolvedPageUrl']
                    new_url = Article.clean_url(diffbot_response['request']['resolvedPageUrl'])
                end 
                
                new_url ||= url
                
		    	parsed_article['url'] = new_url
		    	parsed_article['domain'] = Article.get_domain(new_url)
		    	parsed_article['cover_image_url'] = parsed_article['images'][0]['url'] if parsed_article['images'] && parsed_article['images'][0]
				if parsed_article['text'] && parsed_article['text'].length > 0
					parsed_article['content'] = parsed_article['text']
					sentences = parsed_article['text'].split(/\.|\?|!/).reject{|s| s.strip.empty?}
					if sentences.length > 1 
						parsed_article['excerpt'] = sentences[0] + ". " + sentences[1] + "." 
					else 
						parsed_article['content'] = "Article text is not available"
					end 
					parsed_article['word_count'] = parsed_article['text'].split(" ").reject{|s| s.strip.empty?}.size
					parsed_article.delete('text')
					parsed_article.delete('html')
				else
				 	parsed_article['excerpt'] = "Article text is not available"
				 	parsed_article['content'] = "Article text is not available"
				 	parsed_article['word_count'] = 0
				end 
				parsed_article['meta_keywords'] = diffbot_response['meta']['keywords'] || diffbot_response['meta']['news_keywords'] if diffbot_response['meta']
            end 
        else 
        	parsed_article = {'excerpt' => "Article text is not available"}
			parsed_article = {'content' => "Article text is not available"}
        end
    	return parsed_article
    end 

    def self.instapaper_parse(bookmark_id, oauth_token, oauth_secret)

    	consumer_key = ENV['INSTAPAPER_CONSUMER_KEY']
        secret_key = ENV['INSTAPAPER_SECRET_KEY']
        request_method = "POST"
        request_url = "https://www.instapaper.com/api/1.1/bookmarks/get_content"
        params = {bookmark_id: bookmark_id}
        
        options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
        instapaper_response = oauth_request(options)
        bookmark_content = JSON.parse(instapaper_response)
		
        if bookmark_content['body'] && bookmark_content['body'].length > 0 
            parsing_success = true
        end

        if parsing_success 
        	parsed_article = Hash.new
	    	parsed_article['content'] = bookmark_content['body']
	    	parsed_article['publisher'] = bookmark_content['source_title'] if bookmark_content['source_title'] && bookmark_content['source_title'].length > 0
	    	parsed_article['author'] = bookmark_content['author'] if bookmark_content['author'] && bookmark_content['author'].length > 0 
	    	parsed_article['url'] = Article.clean_url(bookmark_content['url'])
	    	parsed_article['title'] = bookmark_content['title'] if bookmark_content['title'] && bookmark_content['title'].length > 0 
	    	parsed_article['word_count'] = bookmark_content['words'] if bookmark_content['words']
	    	parsed_article['excerpt'] = bookmark_content['first_text'] if bookmark_content['first_text'] && bookmark_content['first_text'].length > 0
	    	parsed_article['cover_image_url'] = bookmark_content['images'][0] if bookmark_content['images'] && bookmark_content['images'].length > 0
	    	parsed_article['domain'] = Article.get_domain(parsed_article['url'])
	    	parsed_article['cover_image_url'] = parsed_article['images'][0]['url'] if parsed_article['images'] && parsed_article['images'][0]
        else 
        	parsed_article = {'excerpt' => "Article text is not available"}
			parsed_article = {'content' => "Article text is not available"}
        end
    	return parsed_article
    end 

    def self.manual_parse(url)
    	begin  
    		mechanize = Mechanize.new.tap { |m| m.user_agent_alias = 'Mac Safari' }
    		page = mechanize.get(url)
    		if page && page.uri 
    			new_url = Article.clean_url(page.uri.to_s)
    		else 
    			new_url = nil
    		end 
    		source = page.content        	
        rescue Mechanize::ResponseCodeError
        	source = nil 
        rescue StandardError => e 
			source = nil
        end 
		if source 			
			doc = Readability::Document.new(source)
			if doc && doc.content && doc.content.length > 0 
				parsing_success = true 
			else 
				parsing_success = false
			end 
		else 
			parsing_success = false
		end 
		
		if parsing_success 	
        	parsed_article = Hash.new
			parsed_article['url'] = new_url || url 
	    	parsed_article['content'] = Article.clean_html_text(doc.content)
	    	parsed_article['title'] = doc.title
	    	parsed_article['author'] = doc.author if doc.author
			sentences = parsed_article['content'].split(/\.|\?|!/).reject{|s| s.strip.empty?}
			if sentences.length > 1 
				parsed_article['excerpt'] = sentences[0] + ". " + sentences[1] + "." 
				parsed_article['word_count'] = parsed_article['content'].split(" ").reject{|s| s.strip.empty?}.size
			else 
				parsed_article['content'] = "Article text is not available"
			end 
			
	    	parsed_article['domain'] = Article.get_domain(parsed_article['url'])
	    	parsed_article['cover_image_url'] = doc.images[0] if doc.images && doc.images[0]
        else 
			parsed_article = {'excerpt' => "Article text is not available"}
			parsed_article = {'content' => "Article text is not available"}
        end

    	return parsed_article
    end 

    def self.get_domain(url)
    	begin 
	    	encoded_url = URI.encode(url)
			domain = URI.parse(encoded_url).host
			cleaned_domain = PublicSuffix.parse(domain).domain.downcase
			return cleaned_domain	
		rescue PublicSuffix::DomainInvalid => e
		rescue StandardError => e 
			return nil
		end 
    end 

    def self.clean_html_text(text)
    	decoder = HTMLEntities.new
    	sanitizer = HTML::FullSanitizer.new
    	output = decoder.decode(sanitizer.sanitize(text))
    	return output
    end 

    def self.domains_to_ignore
    	domains_to_ignore = ['instagram.com', '4sq.com', "vimeo.com", "youtube.com", ".pdf", "foursquare.com", "youtu.be", "swarmapp.com", "pic.twitter.com", "vine.co", "https://twitter.com", "https://twitter.com", "twitpic.com", "path.com", "tumblr.com", "www.facebook.com", "//facebook.com", "www.foodspotting.com"]
    end 
	
    def self.create_from_url(url, title = nil)
    	if self.domains_to_ignore.any? { |domain| url.include?(domain) }
    		return nil
    	else 
    		clean_url = Article.clean_url(url)
			article = Article.where(url: clean_url).first_or_initialize(title: title)
				
			if article.new_record?
				article.save
				Resque.enqueue(ArticleParser, {"parser"=> "manual", "url"=> url, "article_id" => article.id})
			end 
			return article		
		end 
	end 

	def delete_self_in_favor_of_duplicate_article(original_article)
        self.reading_logs.each do |log|
        	unless original_article.reading_logs.find_by_user_id(log.user_id)
            	log.article = original_article
            	log.save 
            end 
        end
        self.destroy
        return original_article
	end 
	
	def check_for_duplicates_with_parsed_url(parsed_url)
		if self.url != parsed_url && Article.where(url: parsed_url).size > 0
		    return true  
		else 
			return false
		end 
	end 

	def update_from_parser(parsed_article)
        if self.authors.nil? || self.authors.length < 1
            self.authors = [parsed_article['author'].truncate(255)] if parsed_article['author']
        end
        self.title = parsed_article['title'].truncate(255)  if self.title.nil? && parsed_article['title']
        self.domain = parsed_article['domain'].truncate(255) if self.domain.nil? && parsed_article['domain']
        self.excerpt = parsed_article['excerpt'] if self.excerpt.nil? && parsed_article['excerpt']
        self.word_count = parsed_article['word_count'] if self.word_count.to_i < parsed_article['word_count'].to_i
        self.meta_keywords = parsed_article['meta_keywords'] if parsed_article['meta_keywords']
		self.content = parsed_article['content'] if (self.content.nil? || self.content == "") && parsed_article['content']
		self.url = parsed_article['url'] if parsed_article['url']
		if self.source_categories.nil? && parsed_article['tags']
			source_categories = Array.new
			parsed_article['tags'].each do |tag|
				source_categories.push(tag['label'].truncate(255)) if tag['label']
			end 
			self.source_categories = source_categories
		end 
        self.save
	end 

	def queue_semantria 
		require 'semantria'
		semantriaKey = ENV['SEMANTRIA_KEY']
        semantriaSecret = ENV['SEMANTRIA_SECRET']
        session = Session.new(semantriaKey, semantriaSecret, 'ArticleParser', true)
        content = self.content
        unless content.nil? || content == "" || content == "Article text is not available"
            doc = {'id' => self.id, 'text' => content.truncate(ENV['SEMANTRIA_CONTENT_TRUNCATION'].to_i)}
            status = session.queueDocument(doc)
        end 
	end 

    def self.clean_url(url)
		bad_params = ["?CMP=", "?_ga=", "?_php=", "?_r=", "?adxnnl=", "?all=", "?ana=", "?app_data=", "?autostart=", "?cc=", "?cmpid=", "?currentPage=", "?eref=", "?from=", "?hp", "?mbid", "?mg=", "?mid", "?mid=", "?mobify=", "?mobile", "?mod=", "?mod=", "?ncid=", "?nl=", "?nopager=", "?op=", "?pagewanted=", "?pagination=", "?printable=", "?ref=", "?seid=", "?single=", "?single_page=", "?smid=", "?source=", "?src=", "?srid=", "?utm", "?view=", "?wpisprc=", "?xid=", "?page=all", "?page=full", "?page=single", "?page=show", "?page=1", "?trk=", "?partner=", "?curator=", "?fsrc=", "?cid", "?cID", "?rref="]

		paramless_url = nil
		poundless_url = nil

		if url.include? "?"
			bad_params.each do |param|
				if url.include? param
					paramless_url = url.split(param)[0]
				end 
			end
		end 
		url = paramless_url if paramless_url

		if url.include? "#"
			poundless_url = url.split("#")[0]
		end 
		url = poundless_url if poundless_url
		url = url.sub("https://", "http://") if url.include?("https://")

		characters_to_remove = ["\n", "\t"]
		characters_to_remove.each do |char|
			url = url.delete(char) if url.include?(char)
		end 
		 
		if url.last == "/"
			url = url.chop
		end 

		if url.include?(" ")
			url = url.gsub(/\s+/, "")
		end 

		return url
    end 

    def self.decode_url(url)
    	begin 
	    	if url != URI.decode(url)
	    		url = URI.decode(url)
	    		decode_url(url)
	    	else 
	    		return url
	    	end
	    rescue StandardError => e 
	    	return url
	    end 
    end 

    def create_topic_tags 
    	semantria_categories = self.semantria_categories || []
    	semantria_subcategories = self.semantria_subcategories || []
    	person_entities = self.person_entities || [] 
    	company_entities = self.company_entities || []
    	place_entities = self.place_entities || []
		topic_tags_to_ignore = ["Business", "Internet", "Finance", "IT", "Facebook", "Google"]
		topic_tags_to_rename = {'Private_equity_secondary_market' => "Fundraising", "Equity_securities" => "Fundraising", "Capacitive_touchscreen_mobile_phones" => "Mobile Phones", "Mobile_Phones" => "Mobile Phones", "Social_media" => "Social Media", "Product_management" => "Product Management", "Photo_sharing" => "Photo Sharing", "Education_reform" => "Education Reform" }
    	topic_loop_number = 0
    	topic_tags = Array.new

        while topic_tags.length < 8 && topic_loop_number < 3
			if semantria_subcategories[topic_loop_number] 
				subcategory = semantria_subcategories[topic_loop_number]['title'] 
				subcategory = topic_tags_to_rename[subcategory] if topic_tags_to_rename[subcategory]
            	topic_tags.push(subcategory) 
            end 
			
            if person_entities[topic_loop_number]
                topic_tags.push(person_entities[topic_loop_number]) if person_entities[topic_loop_number].length < 26  && person_entities[topic_loop_number].length > 1
            end 

            if company_entities[topic_loop_number]
            	company_entity = company_entities[topic_loop_number]
            	if company_entity.length < 26 && company_entity.length > 1 && !topic_tags_to_ignore.include?(company_entity)
                	topic_tags.push(company_entity) 
                end 
            end 

            if semantria_categories[topic_loop_number]
                category = semantria_categories[topic_loop_number]['title']
                category = topic_tags_to_rename[category] if topic_tags_to_rename[category]
                unless topic_tags_to_ignore.include?(category)
                    topic_tags.push(category)
                end 
            end 

            if place_entities[topic_loop_number]
                topic_tags.push(place_entities[topic_loop_number]) if place_entities[topic_loop_number].length < 26 && place_entities[topic_loop_number].length > 1
            end 

            topic_loop_number += 1
        end 
        topic_tags = [] if self.domain == 'nber.org'
        self.update_attributes(topic_tags: topic_tags.uniq)
    end  
	
	def article_stats_change?
		self.topic_tags_changed? || self.word_count_changed? || self.domain_changed? || self.authors_changed?
	end 

	def update_stats_for_users
		self.users.each do |user|
			queue_user_stats_cacher(user)
		end 
	end 

    def queue_user_stats_cacher(user)
    	self.topic_tags ? topic_tags = self.topic_tags : topic_tags = []
    	self.topic_tags_was ? topic_tags_was = self.topic_tags_was : topic_tags_was = []
		potential_topics_to_queue = topic_tags_was.concat(topic_tags).uniq    	
		topics_to_cache = user.topics_for_stats_caching
		if topics_to_cache.nil?
			topics_to_queue = potential_topics_to_queue
		else 
			topics_to_queue = topics_to_cache & potential_topics_to_queue
		end 
		topics_to_queue.each do |topic|
			Resque.enqueue(UserStatsCacher, user.id, topic)
		end 
		Resque.enqueue(UserStatsCacher, user.id, "All")
    	
    end 
end