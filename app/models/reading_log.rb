class ReadingLog < ActiveRecord::Base
	# ASSOCIATIONS
	belongs_to :user, counter_cache: true
	belongs_to :article, counter_cache: true
	
	# CALLBACKS
	# after_save :queue_user_stats_cacher
	# before_destroy :queue_user_stats_cacher
	default_scope {includes(:article)}

	def queue_user_stats_cacher
		self.article.queue_user_stats_cacher(self.user) if self.article
	end 

end
