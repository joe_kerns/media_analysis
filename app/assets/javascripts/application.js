// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require mixpanel
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require angular
//= require angular-ui-router
//= require angular-resource
//= require highstock
//= require drilldown
//= require highcharts-ng
//= require ui-bootstrap-tpls-0.11.0.min
//= require app/app
//= require app/controllers/nav_bar_controller
//= require_tree .

$(document).ready(function() {
	$("#open-email-modal").click(function() {
		event.preventDefault();
		$("#myModal").modal('show');
	})

	$(".close-email-modal").click(function() {
		$("#myModal").modal('hide');
	})
});
