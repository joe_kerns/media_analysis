angular.module('mediaAnalysis.services', ['ngResource'])

.factory("SessionService", ['$http', '$q', function($http, $q){
	var service = {
		getCurrentUser: function() {
			if (service.isAuthenticated()) {
				return $q.when(service.currentUser);
			} else {
				return $http.get('/api/current_user').then(function(resp) {
					return service.currentUser = resp.data;

				});
			}
		},
		currentUser: null, 
		isAuthenticated: function() {
			return !!service.currentUser;
		}
	};
	return service;
}])

.factory("ShowUserService", ['$http', '$q', function($http, $q) {
  return {
    getUser: function(userId) {
      var deferred = $q.defer();
      $http.get('/users/' + userId)
        .success(function(data) {
            deferred.resolve(data);
          }). 
          error(function(msg) {
            deferred.reject(msg);
            console.log("Something went wrong in the ShowUserService");
          });
          return deferred.promise
    }
  }
  return service; 
}])

.factory('Share', ['$resource', function($resource) {
	var Share = $resource('/shares/:id.json',
		{id: '@id'},
		{}
	);
	return Share;
}])

.factory('$FB',['$window',function($window){
  return {
    init: function(fbId){
      if(fbId){
        this.fbId = fbId;
        $window.fbAsyncInit = function() {
          FB.init({ 
            appId: fbId, 
            channelUrl: 'app/channel.html', 
            status: true, 
            xfbml: true 
          });
        };
        (function(d){
          var js,           
          id = 'facebook-jssdk', 
          ref = d.getElementsByTagName('script')[0];
          if (d.getElementById(id)) {
            return;
          }

          js = d.createElement('script'); 
          js.id = id; 
          js.async = true;
          js.src = "//connect.facebook.net/en_US/all.js";

          ref.parentNode.insertBefore(js, ref);

        }(document));
      }
      else{
        throw("FB App Id Cannot be blank");
      }
    }
  };

}])