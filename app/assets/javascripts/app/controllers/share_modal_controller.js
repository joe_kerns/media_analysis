angular.module('mediaAnalysis.controllers')

.controller('ShareModalController', ['$scope', '$modalInstance', 'article',
  function($scope, $modalInstance, article) {
    $scope.newShare = {recipient: ''}
    $scope.article = article

    $scope.closeShareModal = function() {
      $modalInstance.dismiss('cancel')
    }

    $scope.sendShare = function(newShare) {
    	$modalInstance.close(newShare)
    	$scope.newShare = {}
    	
    }
  }
])