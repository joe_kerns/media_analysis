angular.module('mediaAnalysis.controllers')

.controller('UserSearchResultsController', ['$scope', '$http', '$stateParams', '$state',
  function($scope, $http, $stateParams, $state) {
    $scope.query = $stateParams.query
    $scope.currentResultsPage = 1
    $scope.usersSearch = function(query) {
      $state.go('users_search',{query: query}, {reload: true})
      mixpanel.track("Users searched", {
        "search_term": query
      })
    }
    var getResults = function() {
      $http.get('/users', {params: {query: $scope.query}}).
      success(function(data, status) {
        $scope.total_users = data.meta.total  
        $scope.allUsers = data.users
        paginateResults();
        if (status === 200) {
          $scope.showResults = true 
        } else {
          $scope.showResults = false
        }
      }). 
      error(function(data, status) {
        console.log(status);
      })
    }

    // Pagination set-up 
    $scope.maxSize = 10;
    $scope.itemsPerPage = 8;
     
    var paginateResults = function() {
      page = $scope.currentResultsPage
      firstUser = (page - 1) * $scope.itemsPerPage
      lastUser = page * $scope.itemsPerPage 
      $scope.userSearchResults = $scope.allUsers.slice(firstUser, lastUser)
      debugger
    }

 
    getResults()

    $scope.resultsPageChanged = function() {
     paginateResults(); 
    };
  }
])