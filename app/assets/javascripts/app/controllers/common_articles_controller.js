angular.module('mediaAnalysis.controllers')

.controller('CommonArticlesController', ['$scope', '$http', '$interval', '$stateParams', '$rootScope', '$modal', '$timeout', '$window','$state',
  function($scope, $http, $interval, $stateParams, $rootScope, $modal, $timeout,$window, $state) { 
    /// Requested User profile from parameters 
    
    page = $stateParams.page || 1
    category = $stateParams.sortOrder || "Most recent"

    mixpanel.track("Common articles page visited", {
      'profile_viewed_id': userId,
      'current_user': $scope.current_user, 
      'topic': $scope.page_topic_tag,
      'time_period': $scope.timePeriod
    }); 
    
    $("html, body").animate({ scrollTop: 0 }, "slow");  // Make sure go to the top of the page 
    
    $scope.sortOptions = ["Most recent", "Longest"]
    $scope.sortOrder = category

    var getCommonArticles = function(category, page) {
      topic_tag = $scope.page_topic_tag
      timePeriod = $scope.timePeriod
      $http.get('/api/users/common_articles/' + userId, {params: {page: page, category: category, timePeriod: timePeriod, topic_tag: topic_tag}}).
          success(function(data) {
            $scope.totalCommonArticles = data.meta.total_common_articles
            $scope.common_articles = data.common_articles
            $scope.currentPage = page
            debugger
          }). 
          error(function(data) {
            console.log(data);
          })
    };
    getCommonArticles(category, page);

     // Pagination set-up 
    $scope.maxSize = 5;
    $scope.articlesPerPage = 10
    
    $scope.commonArticlesPageChanged = function(category, page) {
      $state.go("profile.common_articles", {page: page, sortOrder: category}, {reload: false});
      
      mixpanel.track("Profile common article page changed", {
      'profile_viewed_id': userId, 
      'current_user': $scope.current_user,
      'page': page,
      'sortOrder': category
    });  
    };
    
    $scope.articleLinkClicked = function(article) {
      article.viewerId = $scope.current_user.id
      article.showUserId = $scope.show_user.id
      mixpanel.track("Click article link from common articles page", article)
    }
    
}])