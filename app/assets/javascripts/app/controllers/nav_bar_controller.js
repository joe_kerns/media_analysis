angular.module('mediaAnalysis.controllers', [])

.controller('NavBarController', ['SessionService', '$rootScope', '$scope', '$http', '$state',
  function(SessionService, $rootScope, $scope, $http, $state) {
    SessionService.getCurrentUser().then(function(data) {
      $rootScope.current_user = data.current_user
      mixpanel.identify(data.current_user.id);
      heap.identify({email: data.current_user.email})
    })
    
  }
])