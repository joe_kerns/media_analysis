angular.module('mediaAnalysis.controllers')

.controller('ConnectAccountsController', ['$scope', '$http', function($scope, $http ) {
	$http.get('/api/users/authorizations').then(function(data) {
	    $scope.providers = data.data
	  //   arrayLength = $scope.providers.length
	  //   for (var i = 0; i < arrayLength; i++) {
			// auth = $scope.providers[i]
			// if (auth.authorized && auth.token) {
			// 	mixpanel.track("Provider authorization successful", {
			// 		"user_id": $scope.current_user.id,
			// 		"provider": auth.provider
			// 	})	
			// } else if (auth.authorized) {
			// 	mixpanel.track("Provider authorization failed", {
			// 		"user_id": $scope.current_user.id,
			// 		"provider": auth.provider
			// 	})
			// }	
	  //   }
	})
	
	if(window.chrome) {
		$scope.browserIsChrome = true
		debugger
		if (document.getElementById('roundview-extension-is-installed')) {
		 	$scope.chromeExtensionInstalled = true
		} 
		else {
			$scope.chromeExtensionInstalled = false
		}
	}
	
	$scope.installExtension = function() { 
		chrome.webstore.install("https://chrome.google.com/webstore/detail/baoichngodjibhnbdghdkhbjcmhkmfdc", 
			function() {
				mixpanel.track("Chrome extension installed", {
			        "user_id": $scope.current_user.id,
			        "user_name": $scope.current_user.name,
			        "user_email": $scope.current_user.email
		    	})
		    	$scope.chromeExtensionInstalled = true	
			}, 
			function() {
				mixpanel.track("Chrome extension installation failed", {
			        "user_id": $scope.current_user.id,
			        "user_name": $scope.current_user.name,
			        "user_email": $scope.current_user.email
		    	})	
			});

	}
	$scope.deauthorizeProvider = function(provider) {
		$http.delete('/authorizations/deauthorize/' + provider).success(function() {
			console.log("deauthorization successful")
			for (i = 0; i < $scope.providers.length; i++) {
				if($scope.providers[i].url == provider) {
					$scope.providers[i].authorized = false
				}
			} 
		}).error(function() {
			console.log("Something went wrong :(")
		})
	}
}])
