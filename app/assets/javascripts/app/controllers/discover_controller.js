angular.module('mediaAnalysis.controllers')

.controller('DiscoverController', ['$scope', 'session', 'SessionService', '$http', '$interval', '$stateParams', '$rootScope', '$modal', '$timeout', '$window', '$state',
  function($scope, session, SessionService,   $http, $interval, $stateParams, $rootScope, $modal, $timeout,$window, $state) { 
    var page = $stateParams.page || 1
    mixpanel.track("Discover page visited", {"page": page});  
    
    $scope.query = ""
    $scope.usersSearch = function(query) {
      $state.go('users_search',{query: query}, {reload: true})
      mixpanel.track("Users searched", {
        "search_term": query
      })
      $scope.query = ""
    }
    var discoverUsers = function(page) {
      $http.get('/api/users/discover', {params: {page: page}}).
        success(function(data) {
          debugger
          $scope.users = data.users
          $scope.totalUsers= data.meta.total
          $scope.currentPage = page;
        }).
        error(function(data) {
          console.log("Something went wrong")
        })
    }
    discoverUsers(page);

    // Pagination set-up 
      $scope.maxSize = 5;
      $scope.itemsPerPage = 8
      
      $scope.pageChanged = function() {
        $state.go("discover", {page: $scope.currentPage}, {reload: false});
        mixpanel.track("Discover users page changed");  
      };  
}])
