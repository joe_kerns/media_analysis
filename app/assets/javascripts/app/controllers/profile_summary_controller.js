angular.module('mediaAnalysis.controllers')

.controller('ProfileSummaryController', ['$scope', 'session', 'SessionService', '$http', '$interval', '$stateParams', '$rootScope', '$modal', '$timeout', '$state',
  function($scope, session, SessionService, $http, $interval, $stateParams, $rootScope, $modal, $timeout,$state) { 
    /// Requested User profile from parameters 
    $scope.$state = $state

    $scope.page_topic_tag = $stateParams.topic_tag || $scope.page_topic_tag

    mixpanel.track("Profile Summary view visited", {
      'user_id': userId
    });  
    
    // CHART LOADING 
    $scope.articleChartConfig = {
      loading: true
    }
    dataGrouping = {
                    units: [[
                        'week', // unit name
                        [1] // allowed multiples
                    ]]
                };      
    $scope.profileSummaryRequest = function() {
      $http.get('/api/users/summary/' + userId, {params: {timePeriod: $scope.timePeriod, topicTag: $scope.page_topic_tag}})
      .success(function(data, status) {
        $scope.top_domains = data.top_domains
        $scope.top_authors = data.top_authors
        $scope.user_chart_data = data.user_chart_data;
        setDailyChart();
        setChartDefaultTimePeriod($scope.timePeriod);
        if (!$scope.show_user.signed_in_user && data.viewer_chart_data.length > 0) {
          $scope.viewer_chart_data = data.viewer_chart_data;
          $scope.articleChartConfig.series.push({name: $scope.current_user.first_name + ' Hours reading', data: $scope.viewer_chart_data, dataGrouping: dataGrouping, color: '#55504E'});
        }
      }).error(function(data, status) {
        if(status === 412) {
          $timeout(profileSummaryRequest, 1000)
        } else {
          $scope.showError = true
          console.log('Something went wrong')
        }
      })
    }
    
    var setChartDefaultTimePeriod = function(timePeriod) {
      options  = {
        'All': 4,
        '12': 2,
        '1': 0
      }
      $scope.articleChartConfig.options.rangeSelector.selected = options[timePeriod]
    }
    $scope.profileSummaryRequest();

    $scope.$watch('articlesProcessed', function() {
      $scope.profileSummaryRequest();
    })

    var setDailyChart = function() {
      $scope.articleChartConfig = {
          useHighStocks: true,
          options: {
            chart: {
                type: 'column',
            },
            legend: {
                 enabled: true
            },
            tooltip: {
              enabled: true,
              formatter: function() {
                var d = new Date(this.x)
                var s = '<b>' + d.toDateString() + '</b>';
                $.each(this.points, function () {
                  var time = this.y;
                  var hours=parseInt(time/3600000);
                  var mins0=parseInt((parseInt(time%3600000))/60000);
                  if (mins0 < 10) {
                    mins = "0" + mins0.toString()
                  } else {
                    mins = mins0.toString()
                  }
                    s += '<br/>' + this.series.name + ': ' + hours + ':' + mins ;
                })
                
                return  s;
              }
            },
            rangeSelector: {
              enabled: true,
              buttons: [{
                type: 'week',
                count: 1,
                text: '1w'
              }, {
                type: 'week',
                count: 4,
                text: '4w'
              }, {
                type: 'week',
                count: 12,
                text: '12w'
              }, {
                type: 'ytd',
                text: 'YTD'
              }, {
                type: 'all',
                text: 'All'
              }], 
              selected: 0
            }
          },
          series: [
          {type: 'column', name: $scope.show_user.first_name + ' Hours reading', data: $scope.user_chart_data, dataGrouping: dataGrouping, color: '#5bc0de'}],
          title: {
              text: "Time spent reading"
          },
          // xAxis: {
          //       type: 'datetime'
          //   },
          yAxis: {
            title: {text: "Hours reading"},
            labels: {
              formatter: function(){
                    var time = this.value;
                    var hours=parseInt(time/3600000);
                    var mins1=parseInt((parseInt(time%3600000))/60000);
                    if (mins1 < 10) {
                      mins = "0" + mins1.toString()
                    } else {
                      mins = mins1.toString()
                    }
                    return hours + ':' + mins;
                }
            },
          },
          credits: {
              enabled: false
          },
          loading: false
      };
    };

    var resizeChart = function() {
      if($scope.articleChartConfig.useHighStocks && ($('#highcharts-4').width() < $('#daily-stats-chart').width() ) ) {

        console.log("chart resized")
        $scope.$broadcast('highchartsng.reflow');
        $interval.cancel(resizeChartPromise);
      } 
    }

    resizeChartPromise = $interval(resizeChart, 10)
}])
