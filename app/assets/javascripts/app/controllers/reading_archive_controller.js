angular.module('mediaAnalysis.controllers')

.controller('ReadingArchiveController', ['$scope', '$http', '$interval', '$stateParams', '$rootScope', '$modal', '$timeout', '$window', '$state',
  function($scope, $http, $interval, $stateParams, $rootScope, $modal, $timeout,$window, $state) { 
    /// Requested User profile from parameters 
    
    page = $stateParams.page || 1
    $scope.page_topic_tag = $stateParams.topic_tag || $scope.page_topic_tag
    $scope.page_author = $stateParams.author || null
    $scope.page_domain = $stateParams.domain || null
    // Pagination set-up 
    $scope.maxSize = 5;
    $scope.itemsPerPage = 10
    
    $("html, body").animate({ scrollTop: 0 }, "slow"); // Make sure go to the top of the page 
    
    mixpanel.track("Reading archive visited", {
      'user_id': userId,
      'topic': $scope.page_topic_tag,
      'time_period': $scope.timePeriod
    });  

    $scope.articlePageChanged = function() {
      page = $scope.currentArticlePage
      $state.go("profile.reading_archive", {page: page}, {reload: false});
      
      mixpanel.track("Profile article archive page changed", {"page": page});  
    };

    var getUserArticles = function(page) {
      var url = '/users/' + userId +'/articles'
      $http.get(url, {params: {page: page, topic_tag: $scope.page_topic_tag, timePeriod: $scope.timePeriod, author: $scope.page_author, domain: $scope.page_domain}}).
          success(function(data) {
            $scope.articles = data.articles
            $scope.currentArticlePage = page;
            debugger
            if (data.meta.total_articles != null) {
              $scope.total_articles = data.meta.total_articles
            }
          }). 
          error(function(data) {
            console.log(data);
          })
    }

    getUserArticles(page);
    
    // Editing Tags 
      $scope.deleteTag = function(article, tag) {
        index = article.topic_tags.indexOf(tag)
        if (index > -1) {
          article.topic_tags.splice(index, 1);
        }
        updateArticle(article, tag, "delete")
        article.deleted_tag = tag 
        mixpanel.track("Topic Tag added to article", article)
        article.deleted_tag = null
      }
      $scope.addTopicTag = function(article) {
        if (article.newTopicTag.length > 0) {
          tag = article.newTopicTag
          article.topic_tags.push(article.newTopicTag)
          updateArticle(article, tag, "add")
          mixpanel.track("Topic Tag added to article", article) 
          article.newTopicTag = null 
          article.showTagInputBox = false  
        }
      }

      var updateArticle = function(article, tag, tag_action) {
        url ='/reading_logs/' + article.id
        $http.put(url, {reading_log: {tag: tag, tag_action: tag_action}}).success(function(data, status) {
          console.log('success!')
        }).
        error(function(data, status) {
          console.log('errorroroororororroororror')
        })
      }

    // ADDING ARTICLE FROM ELSEWHERE
      $scope.addArticle = function (url) {
        $http.post('/reading_logs/new', {article: {url:url}})
          .success(function(data, status) {
            getUserArticles($scope.currentArticlePage)
            $scope.url = ''
          })
          .error(function(data,status) {     
          });  
        mixpanel.track("Article manually added");
      }
    $scope.status = {
      isopen: false
    };

    $scope.articleLinkClicked = function(article) {
      if ($scope.show_user.signed_in_user){
        article.showUserId = $scope.show_user.id
        mixpanel.track("Click article link from own archive", article)   
      } else {
        article.viewerId = $scope.current_user.id
        article.showUserId = $scope.show_user.id
        mixpanel.track("Click article link from other's archive", article)
      }
    }
}])
