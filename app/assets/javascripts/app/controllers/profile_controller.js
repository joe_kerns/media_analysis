angular.module('mediaAnalysis.controllers')

.controller('ProfileController', ['$scope', 'session', 'SessionService', '$http', '$interval', '$stateParams', '$rootScope', '$modal', '$timeout', '$window', '$state', 'showUser', 'ShowUserService',
  function($scope, session, SessionService,   $http, $interval, $stateParams, $rootScope, $modal, $timeout,$window, $state, showUser, ShowUserService) { 
    $scope.$state = $state
    /// Requested User profile from parameters 
    userId = $stateParams.userId
    $scope.show_user = showUser
    $scope.timePeriod = $stateParams.timePeriod || 12
    $scope.page_topic_tag = $stateParams.topic_tag || "All"
    $scope.time_period_display = {
      '12': "the last 12 weeks", 
      '1': "the last week",
      'All': "all time"
    }
    // 

    var profileStatsRequest = function() {
      $http.get('/api/users/stats/' + userId, {params: {timePeriod: $scope.timePeriod, topic_tag: $scope.page_topic_tag}})
      .success(function(data, status) {
        $scope.weekly_average = data.weekly_average    
        $scope.total_articles = data.total_articles 
        $scope.top_topics = data.top_topics
        if($scope.top_topics.length < 1) {
          $timeout(profileStatsRequest, 1000)
        }
      }).error(function(data, status) {
        console.log('Something went wrong')
      })
    }

    $scope.goBack = function() {
      debugger
      window.history.back();
    }
    
    profileStatsRequest();

    $scope.$watch('articlesProcessed', function() {
      profileStatsRequest();
    })
    // 
    $scope.timePeriodChanged = function(timePeriod) {
      mixpanel.track("Profile time period changed", {
        'user_id': userId,
        'time_period': timePeriod,
      });  
      $state.go($state.current.name, {userId: userId, timePeriod: timePeriod, topic_tag: $scope.page_topic_tag}, {reload: false});
    }


    $scope.switchProfileToTopic = function(topic) {
      mixpanel.track("Profile topic changed", {
        'user_id': userId,
        'topic': topic,
      }); 
      $state.go($state.current.name, {userId: userId, timePeriod: $scope.timePeriod, topic_tag: topic, page: 1}, {reload: false});
    }
    


    var checkStatus =  function() {
      $http.get('/api/users/data_update_status').
        success(function(data, status) {
          $scope.articles_updating = data.mid_update
          if (data.mid_update) {
            $scope.articlesToProcess = data.articles_to_process
            $scope.articlesProcessed = data.articles_processed   
            $timeout(checkStatus, 2000)
          }
        }).
        error(function(data, status) {
          console.log("ERROR")
        })
    }
    $scope.noConnectedAccounts = false

    var checkAuthorizations = function() {
      $http.get('/api/users/authorizations').
        success(function(data) {
          if (data[0].authorized == false && data[1].authorized == false) {
            $scope.noConnectedAccounts = true
          }
        }).
        error(function(data) {
          console.log("There was an error")
        })
    }
    
    checkStatus();


    // Article-related actions shared across archive, activities, etc
    $scope.clickFavorite = function (article) {
      article.favorite = !article.favorite
      $http({
        url: '/reading_logs/favorite',
        method: "POST",
        data: {article_favorite: article.favorite, article_id: article.article_id}
      })
      mixpanel.track("Article favorited", article)
    }

    $scope.markHidden = function (article) {
      article.hidden = !article.hidden
      $http({
        url: '/reading_logs/hidden',
        method: "POST",
        data: {article_hidden: article.hidden, article_id: article.article_id}
      })  
      
      if(article.hidden) {
        mixpanel_event = "Marked article private"
      } else {
        mixpanel_event = "Marked article public"
      }
      
      mixpanel.track(mixpanel_event, article)
    }
    
    $scope.openShareModal = function(article){
      mixpanel.track("Share article via email clicked", article)  
      var modalInstance = $modal.open({
        templateUrl: 'templates/share_article_modal.html',
        controller: 'ShareModalController',
        size: 'lg',
        resolve: {
          article: function( ) {
            return article;
          }
        }
      })
    $scope.newShare = {recipient: ''}

      modalInstance.result.then(function(newShare) {
        var share = new Share({
          article_id: article.id,
          from_user: $scope.current_user.id,
          to_user: newShare.recipient,
          message: newShare.message
        });
        share.$save();
        mixpanel.track("Article shared via email", article);  
      })  
    }
    
    $scope.shareTwitter = function(article) {
      // center the popup window
      var left = screen.width/2 - 200
      var top = screen.height/2 - 250
      var twitterUrl = "https://twitter.com/share?url=" + encodeURI(article.url) + "&text=" + encodeURI(article.title) + "&related=tkalimov%2Cjosephkerns" + "&via=roundview" + "&lang=en" 
      $window.open(twitterUrl, '', "top=" + top + ",left=" + left + ",width=400,height=500")
      mixpanel.track("Article shared via twitter", article);  
    }

}])
