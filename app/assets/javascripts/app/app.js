angular.module("mediaAnalysis", ['ngResource', 'mediaAnalysis.controllers', 'mediaAnalysis.services','mediaAnalysis.directives','ui.bootstrap', 'highcharts-ng', 'ui.router'])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /
  $urlRouterProvider.otherwise("/");
  //

  // Now set up the states
  $stateProvider
    .state('redirectState', {
      url: "/", 
      resolve: {
        session: function(SessionService) {
          return SessionService.getCurrentUser();
        },
        redirect: ['session', '$state', function(session, $state) {
          currentUser = session.current_user
          $state.go('profile.summary',{userId: currentUser.id, timePeriod: 12, topicTag: 'All'}, {reload: true});
        }]
      }
    })
    .state('discover', {
      url: "/discover?page", 
      templateUrl: "templates/discover.html",
      controller: 'DiscoverController',
      resolve: {
        session: function(SessionService) {
          return SessionService.getCurrentUser();
        }
      }
    })
    .state('profile', {
      abstract: true,
      url: '/users/:userId/:timePeriod?topic_tag',
      templateUrl: "templates/profile.html",
      controller: 'ProfileController',
      resolve: {
        session: function(SessionService) {
          return SessionService.getCurrentUser();
        }, 
        showUser: function($stateParams, ShowUserService) {
          return ShowUserService.getUser($stateParams.userId);
        } 
      }
    })
    .state('connect_accounts', {
    	url: "/connect_accounts",
    	templateUrl: "templates/connect_accounts.html",
    	controller: 'ConnectAccountsController'
    })
    .state('users_search', {
      url: '/users?query&page',
      templateUrl: "templates/users_search_results.html",
      controller: 'UserSearchResultsController'
    })	
    .state('profile.summary', {
      url: '/summary',
      templateUrl: "templates/profile_summary.html",
      controller: 'ProfileSummaryController'
    })  
    .state('profile.reading_archive', {
      url: '/reading_archive?page&domain&author',
      templateUrl: "templates/reading_archive.html",
      controller: 'ReadingArchiveController'
    })  
    .state('profile.common_articles', {
      url: '/common_articles?page&sortOrder',
      templateUrl: "templates/common_articles.html",
      controller: 'CommonArticlesController'
    })  
}])

.run(function($FB){
  $FB.init('1434745550113230');
})

.run(['$rootScope', '$location', '$window', function($rootScope, $location, $window){
     $rootScope
        .$on('$stateChangeSuccess',
            function(event){
                if (!$window.ga)
                    return;
                $window.ga('send', 'pageview', { page: $location.path() });
        });
}])