angular.module('mediaAnalysis.directives', [])

.directive('isUserOrEmail', ['$http', '$timeout', '$filter', '$q', function($http, $timeout, $filter, $q) {

	var isUser = function(input) {
		var d = $q.defer();

		if (input) {
			$http.post('/api/check/is_user', {email: input}).then(function(data) {
				if (data.status == 200){
					d.resolve(data.data);
				} else {
					d.reject(data.data);
				}
			});
		} else {
			d.reject("No input");
		}
		return d.promise;
	};

	var checking = null;
	var	emailRegex = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
	return {
		restrict: 'A', 
		require: 'ngModel', 
		link: function(scope, ele, attrs, ctrl) {
			scope.$watch(attrs.ngModel, function(v) {
				if (checking) clearTimeout(checking);

				var value = v;

				checking = $timeout(function() {
					isUser(value).then(function(data) {
						if(data.success) {
							checking = null;
							ctrl.$setValidity('isUserOrEmail', true);
						} else {
							if(emailRegex.test(value)) {
								checking = null;
								ctrl.$setValidity('isUserOrEmail', true);
							} else {
								checking = null;
								ctrl.$setValidity('isUserOrEmail', false);
							}
						}
					})
				}, 200);
			});
		}
	}
}])

.directive('facebook', ['$timeout','$http', function($timeout,$http) {
  return {
    scope: {
      shares: '=' 
    }, 
    transclude: true,
    template: '<div ng-transclude></div>',
    link: function(scope, element, attr) {
      if(attr.shares){
        $http.get('https://api.facebook.com/method/links.getStats?urls='+attr.url+'&format=json').success(function(res){
          scope.shares = res[0].share_count;
        }).error(function(){
          scope.shares = 0;
        });
      }
      $timeout(function(){
        element.bind('click',function(){
          FB.ui(
            {method: 'feed',
              name: attr.name,
              link: attr.url,
              picture: attr.pictureUrl,
              caption: attr.caption
          });
        });
      });
    }
  };
}])