class CustomDeviseMailer < Devise::Mailer  
	include Devise::Controllers::UrlHelpers
	default from: "Roundview <contact@roundview.co>"

end 