class AdminMailer < ActionMailer::Base
  include Resque::Mailer
  # default from: "Roundview <contact@roundview.co>"

  def new_user(user_id)
    @user = User.find(user_id)
    mail(to: ['tim@roundview.co', 'joe@roundview.co'], from: "Roundview <contact@roundview.co>", subject: "Roundview has a new user!")
  end 

  def deleted_user(user_id)
    @user = User.find(user_id)
    @articles = @user.articles
    mail(to: ['tim@roundview.co', 'joe@roundview.co'], from: "Roundview <contact@roundview.co>", subject: "Roundview has lost a user!")
    Resque.enqueue(UserDestroyer, user_id)
  end 

  def welcome_user(user_id)
    @user = User.find(user_id)

    ActionMailer::Base.smtp_settings[:user_name] = "joe@roundview.co"
    ActionMailer::Base.smtp_settings[:password] = ENV['JOE_EMAIL_PASSWORD']

    if @user.email
      mail(to: @user.email, from: "Joe Kerns <joe@roundview.co>", subject: "Hi there")
    end 

    ActionMailer::Base.smtp_settings[:user_name] = ENV['CONTACT_EMAIL']
    ActionMailer::Base.smtp_settings[:password] = ENV['CONTACT_EMAIL_PASSWORD']
  end 

  def goodbye_user(user_id)
    @user = User.find(user_id)
    ActionMailer::Base.smtp_settings[:user_name] = "joe@roundview.co"
    ActionMailer::Base.smtp_settings[:password] = ENV['JOE_EMAIL_PASSWORD']

    if @user.email
      mail(to: @user.email, from: "Joe Kerns <joe@roundview.co>", subject: "Super quick feedback?")
    end 
    ActionMailer::Base.smtp_settings[:user_name] = ENV['CONTACT_EMAIL']
    ActionMailer::Base.smtp_settings[:password] = ENV['CONTACT_EMAIL_PASSWORD']
  end 

end
