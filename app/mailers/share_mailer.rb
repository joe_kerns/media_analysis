class ShareMailer < ActionMailer::Base
	include Resque::Mailer
  default from: "Roundview <contact@roundview.co>"

  def share_article(share_id)
  	share = Share.find(share_id)
  	@from_user = share.from 
    if share.to 
      to_email = share.to.email
    else 
      to_email = share.to_email
    end 
  	@article = share.article
  	@message = share.message
  	mail(to: to_email, subject: "#{@from_user.first_name} has shared an article from Roundview")
  end 

end
