ActiveAdmin.register User do
   permit_params :name, :first_name, :last_name, :email, :claimed_profile, :discover_score, :manual_discover_score


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    id_column
    column :first_name
    column :last_name
    column :reading_logs_count
    column :email
    column :claimed_profile
    column :discover_score
    column :manual_discover_score
    actions
  end

  filter :first_name
  filter :email
  filter :last_name
  filter :discover_score
  filter :manual_discover_score
  filter :created_at
  filter :updated_at
  filter :sign_in_count
  filter :last_sign_in_at
  filter :reading_logs_count
end
