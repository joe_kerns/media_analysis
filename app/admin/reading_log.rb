ActiveAdmin.register ReadingLog do
  permit_params :article_id, :user_id, :time_added, :time_read, :reading_source, :favorite, :hidden, :user_modified_topics

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  filter :article_id
  filter :user_id
  filter :time_added
  filter :time_read
  filter :created_at
  filter :updated_at
  filter :reading_source
  filter :logged_flag
  filter :favorite
  filter :hidden
  filter :user_modified_topics
  
end
