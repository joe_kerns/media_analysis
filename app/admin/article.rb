ActiveAdmin.register Article do
  permit_params :title, :url, :domain, :excerpt, :authors, :topic_tags, :word_count

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  index do
    column :id
    column :title
    column :url
    column :domain
    column :word_count
    column :excerpt
    column :authors
    column :topic_tags
    column :semantria_categories
    column :semantria_subcategories
    column :semantria_topics
    column :person_entities
    column :company_entities
    column :place_entities
    column :product_entities
    column :job_title_entities
    column :meta_keywords
    column :other_entities
    column :created_at
    column :updated_at
    actions
  end
 
  filter :title
  filter :word_count
  filter :url
  filter :domain
  filter :created_at
  filter :updated_at
  filter :publisher
  filter :categorized_flag
  filter :excerpt
  filter :cover_image_url
  filter :meta_keywords
  filter :reading_logs_count
  filter :content
  filter :summary

end
