ActiveAdmin.register_page "User Growth" do
	controller do 
		def index 
			require 'utility/metrics'
			@output_hash = Metrics.user_growth
			last_week = @output_hash[:list_of_weeks].last 
			@weeks_for_overall_growth = [:week_1, :week_23, :week_24]
			@overall_growth_rates = {all_users: {}, activated_users: {}}
			@weeks_for_overall_growth.each do |week| 
				@output_hash[:cumulative_users][last_week] 
			end  
		end 
	end 
  content do
  	@user = "tim"
    render partial: "user_growth"
  end
end