class BlogPostFetcher
    require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :low
    @lock_after_execution_period = 170
    
    @retry_limit = 3
    @retry_delay = 180

	def self.perform
		wordpress_url = "https://public-api.wordpress.com/rest/v1/sites/blog.roundview.co/posts/"
        number = 3
        fields="URL,date,excerpt,title"

        recent_posts = HTTParty.get(wordpress_url, :query => {number: number, fields: fields})
        recent_posts['posts'].each do |post|
            post['excerpt'] = Article.clean_html_text(post['excerpt'])
            post['title'] = Article.clean_html_text(post['title'])
            post['date'] = post['date'].to_date.strftime('%B %e, %Y')
        end 

        $redis.set("recent_blog_posts", recent_posts['posts'].to_json)
	end 
end 