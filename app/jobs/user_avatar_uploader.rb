class UserAvatarUploader
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    
    @retry_limit = 3
    @retry_delay = 5

	def self.perform(user_id, avatar_url)
		user = User.find(user_id)
		user.avatar = URI.parse(avatar_url)
		user.save
	end 

end 