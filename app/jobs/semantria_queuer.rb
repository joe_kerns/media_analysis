class SemantriaQueuer
  require 'heroku_resque_autoscale'
  include Resque::Plugins::UniqueJob
  extend HerokuAutoScaler::AutoScaling
  extend Resque::Plugins::Retry
  @queue = :low
  @lock_after_execution_period = 50
  
  @retry_limit = 1
  @retry_delay = 180

	def self.perform
    articles_with_errors = 0
    Article.where("categorized_flag = ? AND content != ? AND content IS NOT ? AND content != ?", false, '', nil, "Article text is not available").find_each do |article|
        begin
          article.queue_semantria
        rescue StandardError => e
          Rails.logger.error("Error in SemantriaQueuer with article_id: #{article.id}, error: #{e}")
          e.backtrace.each {|error| Rails.logger.error("#{error}")}
        end 
    end
	end 
end 