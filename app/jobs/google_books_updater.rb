# class GoogleBooksUpdater
# 	@queue = :apis_update

# 	def self.perform(user_id)
# 		begin
# 			#### Look up proper authorization token for Google Books for this User and time since last update
# 		        user = User.find_by_id(user_id)
# 				auth = user.authorizations.find_by_provider('google_books')	
			
# 			#### Update access token using the refresh token 
# 				auth.refresh_google_token
				
# 			#### Get list of bookshelves to get the ID of the "Have Read" one 
# 				bookshelves_list = HTTParty.get('https://www.googleapis.com/books/v1/mylibrary/bookshelves', query: {access_token: auth.token})


# 				bookshelves_list['items'].each do |bookshelf|
# 					if bookshelf['title'] == "Have read"
# 						$have_read_bookshelf_id = bookshelf['id']
# 					end 
# 				end 
				
# 			#### Get the books on that bookshelf 
# 				read_books_list = HTTParty.get("https://www.googleapis.com/books/v1/mylibrary/bookshelves/#{$have_read_bookshelf_id}/volumes", query: {access_token: auth.token, projection: 'full'})
				
# 			#### Call the API and update the database 
# 				time_called = Time.now
# 				words_per_page = 250	
				
# 				catch (:done) do 
# 					if read_books_list['items'].length > 0 
# 			          	read_books_list['items'].each do |item|
# 			          		if auth.time_data_updated
# 			                    throw :done if user.articles.where(:title => item['volumeInfo']['title'], :reading_source => 'Google Books').size > 0
# 			                end   
# 							time_read = item['userInfo']['updated'].to_time ### Assumes Updated == Finished reading
# 							title = item['volumeInfo']['title']
# 							url = item['accessInfo']['webReaderLink']
# 							word_count = item['volumeInfo']['pageCount'] * words_per_page
# 							source_categories = item['volumeInfo']['categories'] 
# 							reading_source = "Google Books"
# 							domain = nil
# 							if item['volumeInfo']['publisher']
# 								publisher = item['volumeInfo']['publisher']
# 							else 
# 								publisher = nil
# 							end 
# 							article = Article.where(title: title, word_count: word_count, domain: domain, publisher: publisher).first_or_create
# 		      				article.update_attributes(source_categories: source_categories, url: url)
# 							user.reading_logs.create(:article => article, :time_read => time_read, reading_source: reading_source)
# 			         	end 
# 			         	throw :done
# 		        	end
# 		        end  	
# 		        auth.update(time_data_updated: time_called)
# 		        $redis.set("user_#{user.id}_google_books_data_updated", "true")
# 		        Resque.enqueue(UserReadingCalculator, user_id)
# 	    rescue StandardError=>e
# 	 		$redis.set("user_#{user.id}_google_books_data_updated", "true")
# 	 		puts "User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}"
#  		end	
# 	end 
# end 