class ReadabilityUpdater
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend AuthorizationsHelper
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    @lock_after_execution_period = 50

    @retry_limit = 3
    @retry_delay = 60

	def self.perform(user_id)
		user = User.find_by_id(user_id)
		begin
			#### Look up proper authorization token for Readability for this User and time since last update
		        auth = user.authorizations.find_by_provider('readability')
		        
		        if auth.time_data_updated
		          since = auth.time_data_updated.utc.strftime("%FT%TZ")
		        else 
		          since = Time.at(0).strftime("%FT%TZ")
		        end 
			
			#### Call the API and cycle through different pages, if there is more than one page of results
				request_url = 'https://www.readability.com/api/rest/v1/bookmarks'
				request_method = "GET"
				oauth_consumer_key = ENV['READABILITY_CONSUMER_KEY']
				secret_key = ENV['READABILITY_SECRET_KEY']
				time_readability_called = Time.now	
				page = 1 
				total_pages = 2
				user.articles_processed = 0
				while page < total_pages
					options = {params: {archive: 1, page: page, per_page: 50, updated_since: since, order: "-date_updated"}, oauth_token: auth.token, oauth_secret: auth.secret, request_url: request_url, oauth_consumer_key: oauth_consumer_key, secret_key: secret_key, request_method: request_method}
					
					bookmarks = oauth_request(options)
					if bookmarks && bookmarks['meta'] && bookmarks['meta']['num_pages']
						total_pages = bookmarks['meta']['num_pages'] + 1
						user.articles_to_process = bookmarks['meta']["item_count_total"]
					end 

					if bookmarks['bookmarks']
			          	bookmarks['bookmarks'].each do |item|
			          		url = Article.clean_url(item['article']['url'])
			          		article = Article.where(url: url).first_or_initialize
			          		if article.new_record? 
								title = item['article']['title']
								# words_read = (word_count * item['read_percent'].to_f).round
								domain = item['article']['domain']
								begin 
									cleaned_domain = PublicSuffix.parse(domain).domain.downcase
								rescue URI::InvalidURIError
								rescue PublicSuffix::DomainInvalid
								rescue StandardError => e  
									cleaned_domain = nil
								end 

								publisher = nil 
								cover_image_url = item['article']['lead_image_url']
								excerpt = item['article']['excerpt']
								authors = Array.new
								if item['article']['author']
									authors.push(item['article']['author'].truncate(255))
					    		end 
								
					      		word_count = item['article']['word_count'].to_i
					      		
					      		article.update_attributes(title: title, cover_image_url: cover_image_url, word_count: word_count, excerpt: excerpt, domain: cleaned_domain, authors: authors.uniq)
					      		Resque.enqueue(ArticleParser, {"parser"=> "manual", "url"=> url, "article_id" => article.id})
						    end 
							if item['tags'] && item['tags'].length > 0
								item['tags'].each {|tag| topic_tags.push(tag['text']) unless topic_tags.include?(tag['text']) }
								article.update_attributes(topic_tags: topic_tags)
							end 
						    
							reading_source = 'Readability'
							time_added = item['date_added'].to_time
							if item['date_archived']
								time_read = item['date_archived'].to_time ### Assumes Archiving == Finished reading
							else
							 	time_read = time_added
							end 
							time_read = Time.now if time_read > Time.now
							article_progress = item['read_percent'].to_f
							user.reading_logs.where(article: article).first_or_create(:article => article, logged_flag: true, :time_added => time_added, :time_read => time_read, reading_source: reading_source, article_progress: article_progress)	
							user.articles_processed += 1
			         	end 
		        	end 	
		        	page += 1
				end 
		    auth.update(time_data_updated: time_readability_called) 
		    user.articles_to_process = 0
		    user.articles_processed = 0
	 	rescue StandardError=>e
	 		Rails.logger.error("User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}")
	 		e.backtrace.each {|e| Rails.logger.error("#{e}")}
	 		user.articles_to_process = 0
		    user.articles_processed = 0
	 	end	
	end 
end 