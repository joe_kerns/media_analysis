class PocketUpdater
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    @lock_after_execution_period = 50
    
    @retry_limit = 3
    @retry_delay = 60
	
	### Architecture
	# POCKET UPDATER 
	#goes and gets articles read on pocket
	# Cleans URL and checks our database for that URL 
	# if article with that URL exists, then we have the article 
	# if no existing article, then we create an article using the Pocket data AND queue Diffbot parser

	# DIFFBOT PARSER
	# Diffbot parses article, if the resolved url from diffbot is different than original Pocket URL, then check database for the new resolved one
	# if resolved one exists, that means there was a preexisting article and we need to delete the new article we created at and reassociate its reading logs and we are DONE 
	# if resolved one does not exist OR two URLs are the same, then update the url of the article and everything else from diffbot, then queue Semantria
	
	def self.perform(user_id)
		user = User.find(user_id)
		begin 
		    pocketAPI = 'https://getpocket.com/v3/get'

		    auth = user.authorizations.find_by_provider('pocket')
		    if auth.time_data_updated
		      since = auth.time_data_updated.to_time.to_i
		    else 
		      since = 0
		    end 
		    count = 200
			offset = 0

			time_pocket_called = Time.now
		    pocketReadArticles = HTTParty.post(pocketAPI, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], access_token: auth.token, state: 'archive', contentType: 'article', sort: 'newest', detailType: 'complete', since: since, count: count, offset: offset})

		    pocketLength = HTTParty.post(pocketAPI, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], access_token: auth.token, state: 'archive', contentType: 'article', sort: 'newest', detailType: 'complete', since: since, count: 5000})
		    if pocketReadArticles['list']
		    	number_of_articles = pocketReadArticles['list'].length
		    else 
		    	number_of_articles = 0
		    end 
		    if pocketLength['list']
		    	user.articles_to_process = pocketLength['list'].length
			    if pocketLength['list'].length > 1000
				    pocketLength = nil
				    GC.start
				end 
			else 
				user.articles_to_process = 0
			end 
			user.articles_processed = 0

			while number_of_articles > 0 
			    pocketReadArticles['list'].values.each do |item|
			    	### Parse the Pocket Response into what we need 
			    		url = Article.clean_url(item['resolved_url'])
						article = Article.where(url: url).first_or_initialize

		      			if article.new_record?
							authors = Array.new
							if item['authors']
					    		item['authors'].values.each do |author_hash|
					    			author_name = author_hash['name'].downcase.titleize
					    			authors.push(author_name)
					    		end  
					    	end 

					        title = item['resolved_title'].truncate(255)
					        encoded_url = URI.encode(url)
					        begin 
						        domain = URI.parse(encoded_url).host
								cleaned_domain = PublicSuffix.parse(domain).domain.downcase
							rescue URI::InvalidURIError
							rescue PublicSuffix::DomainInvalid
							rescue StandardError => e 
								cleaned_domain = nil
							end 
							cover_image_url = item['image']['src'] if item['image']
					        excerpt = item['excerpt']
				      		word_count = item['word_count'].to_i
				      		
				      		article.update_attributes(title: title, cover_image_url: cover_image_url, word_count: word_count, excerpt: excerpt, domain: cleaned_domain, authors: authors)
				      		Resque.enqueue(ArticleParser, {"parser"=> "manual", "url"=> url, "article_id" => article.id})
				      	end 

				      	if item['tags'] && item['tags'].length > 0
				        	topic_tags = article.topic_tags || Array.new	
				        	item['tags'].keys.each {|tag| topic_tags.push(tag) unless topic_tags.include?(tag) }
				        	article.update_attributes(topic_tags: topic_tags)
				        end

						reading_source = "Pocket"
						time_added = Time.at(item['time_added'].to_i)
				        time_read = Time.at(item['time_read'].to_i)
				        time_read = Time.now if time_read > Time.now
						user.reading_logs.where(article: article).first_or_create(logged_flag: true, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
						user.articles_processed += 1 
		      	end 
		      	offset += count 
		      	time_pocket_called = Time.now
		      	pocketReadArticles = HTTParty.post(pocketAPI, :body => {consumer_key: ENV['POCKET_CONSUMER_KEY'], access_token: auth.token, state: 'archive', contentType: 'article', sort: 'oldest', detailType: 'complete', since: since, count: count, offset: offset})
		      	if pocketReadArticles['list']
		      		number_of_articles = pocketReadArticles['list'].length
		      	else 
		      		number_of_articles = 0
		      	end 
		    end  
		    auth.update_attributes(time_data_updated: time_pocket_called)
		    user.articles_to_process = 0
		    user.articles_processed = 0
 		rescue StandardError=>e
 			Rails.logger.error("User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}")
	 		e.backtrace.each {|e| Rails.logger.error("#{e}")}
	 		user.articles_to_process = 0
	 		user.articles_processed = 0
		end
	end 
end 