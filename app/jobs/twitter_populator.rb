# class TwitterPopulator
# 	require 'heroku_resque_autoscale'
#     include Resque::Plugins::UniqueJob
#     extend HerokuAutoScaler::AutoScaling
#     extend Resque::Plugins::Retry
#     extend AuthorizationsHelper
#     @queue = :low
    
#     @retry_limit = 1
#     @retry_delay = 180
	
# 	def self.perform
		
# 		oauth_token = ENV['TWITTER_TIM_OAUTH_TOKEN']
# 		oauth_secret = ENV['TWITTER_TIM_OAUTH_SECRET']
# 		consumer_key = ENV['TWITTER_API_KEY']
# 		secret_key = ENV['TWITTER_SECRET_KEY']
		
# 		request_url = "https://api.twitter.com/1.1/friends/ids.json"
# 		request_method = "GET"
			
# 		params = {screen_name: "josephkerns"} 
# 		options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
# 		twitter_response = oauth_request(options)
# 		user_ids = twitter_response['ids']
# 		request_url = "https://api.twitter.com/1.1/users/show.json"
		
# 		user_ids.uniq.each do |user_id|
# 			begin
# 				params = {user_id: user_id} 
# 				options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
# 				twitter_response = oauth_request(options)
# 				unless twitter_response['followers_count'].to_i < 10000
# 					name = twitter_response['name']
# 					Rails.logger.error("Twitter handle: #{name} added from Joe's list of followed friends") 
# 					first_name = name.split.first
# 					last_name = name.split.last
# 					screen_name = twitter_response['screen_name']
# 					user = User.where(name: name).first_or_create(first_name: first_name, last_name: last_name, email: "#{screen_name}@roundview.co", password:"password", password_confirmation: "password", claimed_profile: false)
# 					if twitter_response['profile_image_url']
# 						twitter_response['profile_image_url'].slice!("_normal")
# 						avatar_url = twitter_response['profile_image_url']
# 						Resque.enqueue(UserAvatarUploader, user.id, avatar_url)
# 					end 
# 					user.authorizations.where(provider: "twitter").first_or_create(name: screen_name)
# 				end 
# 			rescue StandardError => e
# 			end 
# 		end 
		
# 	end 
# end 