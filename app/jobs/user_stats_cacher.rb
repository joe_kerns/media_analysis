class UserStatsCacher
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :high
    @lock_after_execution_period = 170
    
    @retry_limit = 1
    @retry_delay = 180

	def self.perform(user_id, topic = nil)
		user = User.find(user_id)
		
		if topic 
			["lifetime", "last 12 weeks", "last week"].each {|time_period| user.calculate_top_topics(time_period) }
			user.update_cached_data_for_topic(topic)
		else 
			user.update_all_cached_data
		end 
	end 
end 