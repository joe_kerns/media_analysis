# class GoodreadsUpdater
# 	extend AuthorizationsHelper	
# 	@queue = :api_updating

# 	def self.perform(user_id)
# 			#### Look up proper authorization token for Google Books for this User and time since last update
# 		        user = User.find_by_id(user_id)
# 				auth = user.authorizations.find_by_provider('goodreads')
# 		begin	
# 			#### Define the required parameters for the API call and Oauth 
# 				oauth_consumer_key = ENV['GOODREADS_KEY']
# 				secret_key = ENV['GOODREADS_SECRET']
# 				user_id = auth.uid
# 				shelf = "read"
# 				sort = "date_updated"
# 				request_url =  'https://www.goodreads.com/review/list'
# 				request_method = 'GET'
# 				time_called = Time.now

# 				options = {params: {v: '2', id: user_id, shelf: shelf, key: oauth_consumer_key, format: 'xml'}, oauth_token: auth.token, oauth_secret: auth.secret, request_url: request_url, oauth_consumer_key: oauth_consumer_key, secret_key: secret_key, request_method: request_method}
# 				goodreads_response = oauth_request(options)
# 			### Update the database 
# 				# if goodreads_response['GoodreadsResponse']['reviews']['end'] < goodreads_response['GoodreadsResponse']['reviews']['total']
# 				# end 			
# 				words_per_page = 250	
# 				catch (:done) do 
# 					if 	goodreads_response['GoodreadsResponse']['reviews']['review'].length > 0
# 						goodreads_response['GoodreadsResponse']['reviews']['review'].each do |item|
# 							if item['read_at']
# 							 	time_read = item['read_at']
# 							else 
# 							 	time_read = item['date_updated']
# 							end 

# 							if auth.time_data_updated
# 				                throw :done if user.articles.where(:time_read => time_read, :reading_source => 'Goodreads').size > 0
# 				            end 

# 							if item['started_at']
# 							 	time_added = item['started_at']
# 							else 
# 							 	time_added = item['date_added']
# 							end 

# 							if item['book']['num_pages']
# 								word_count = item['book']['num_pages'].to_i * words_per_page
# 							else 
# 								word_count = nil
# 							end 
# 							title = item['book']['title']
# 							url = item['book']['link']
# 							source_categories = nil
# 							domain = nil
# 							publisher = item['book']['publisher']
# 							reading_source = "Goodreads"
							
# 							article = Article.where(title: title, word_count: word_count, domain: domain, publisher: publisher).first_or_create
# 		      				article.update_attributes(source_categories: source_categories, url: url)
# 							user.reading_logs.create(:article => article, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
# 						end 
# 					end 
# 				end 
# 		        auth.update(time_data_updated: time_called) 
# 		        $redis.set("user_#{user.id}_goodreads_data_updated", "true")
# 		        Resque.enqueue(UserReadingCalculator, user_id)
# 		    rescue StandardError=>e
# 		 		$redis.set("user_#{user.id}_goodreads_data_updated", "true")
# 		 		puts "User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}"
# 	 		end	

# 	end 
# end 