class TwitterUpdater
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend AuthorizationsHelper
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    @lock_after_execution_period = 50
    
    @retry_limit = 3
    @retry_delay = 60

	def self.perform(user_id)
		user = User.find(user_id)
		auth = user.authorizations.find_by_provider('twitter')
		
		if auth.token 
			oauth_token = auth.token
			oauth_secret = auth.secret
		else
			oauth_token = ENV['TWITTER_TIM_OAUTH_TOKEN']
			oauth_secret = ENV['TWITTER_TIM_OAUTH_SECRET']
		end 
		
		consumer_key = ENV['TWITTER_API_KEY']
		secret_key = ENV['TWITTER_SECRET_KEY']
		request_url = "https://api.twitter.com/1.1/statuses/user_timeline.json"
		request_method = "GET"
		screen_name = auth.name

    	if auth.last_logged_tweet
	      since_id = auth.last_logged_tweet
	      params = {screen_name: screen_name, count: "200", trim_user: true, since_id: since_id}
	    else 
	      params = {screen_name: screen_name, count: "200", trim_user: true} 
	    end 
						
		options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
		
		twitter_response = oauth_request(options)
		tweets = twitter_response.parsed_response
		if tweets[0] && tweets[0]['id']
			last_logged_tweet = tweets[0]['id'] 
		elsif since_id 
			last_logged_tweet = since_id
		else
			last_logged_tweet = nil 
		end 
		number_of_tweets = tweets.length
		first_logged_tweet = 0 
		catch (:done) do  
			while number_of_tweets > 0 do 
				# first time just pass the count parameter 
				# So we're workign from most recent to oldest. Set the first tweet_id as our "last_logged_tweet" and then set every tweet we process thereafter as our "first_logged_tweet" 
				if tweets.is_a? Hash
					Resque.enqueue_in(15.minutes, TwitterUpdater, user_id)
					throw :done 
				end 
				tweets.each do |tweet|
					begin
						time_read = tweet["created_at"]
						time_read = Time.now if time_read > Time.now
						time_added = nil 
						reading_source = "Twitter"
						urls = tweet['entities']['urls']
						urls.each do |tweeted_link|
							article_url = tweeted_link['expanded_url'] || tweeted_link['url']
							article = Article.create_from_url(article_url)
							if article 
								user.reading_logs.where(article: article).first_or_create(:article => article, logged_flag: true, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
							end 
						end 
						first_logged_tweet = tweet["id"].to_i - 1 
					rescue StandardError => e
						Raven.capture_exception(e)
						first_logged_tweet = tweet["id"].to_i - 1 
						Rails.logger.error("Error with TwitterUpdater for User: #{user.id}, tweet: #{tweet['id']}, error: #{e}")
						Rails.logger.error("#{e.backtrace}") 
					end 
				end

				if since_id 
					params = {screen_name: screen_name, count: "200", trim_user: true, max_id: first_logged_tweet, since_id: since_id}
				else 
					params = {screen_name: screen_name, count: "200", trim_user: true, max_id: first_logged_tweet}
				end 

				options = {:params => params, :oauth_token => oauth_token, :oauth_secret => oauth_secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}

				twitter_response = oauth_request(options)
				Rails.logger.info("twitter request made")
				tweets = twitter_response.parsed_response
				
				number_of_tweets = tweets.length 
			end
			auth.update_attributes(last_logged_tweet: last_logged_tweet.to_s) if last_logged_tweet
			throw :done
		end 
		
		Rails.logger.info("TwitterUpdater done for User #{user.id}")
	end 
end 