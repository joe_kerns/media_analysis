class InstapaperUpdater
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend AuthorizationsHelper
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    @lock_after_execution_period = 50
    
    @retry_limit = 3
    @retry_delay = 60
	
	def self.perform(user_id)
		user = User.find(user_id)
		begin 
			auth = user.authorizations.find_by_provider('instapaper')

			consumer_key = ENV['INSTAPAPER_CONSUMER_KEY']
			secret_key = ENV['INSTAPAPER_SECRET_KEY']
			request_method = "POST"

			request_url = "https://www.instapaper.com/api/1.1/folders/list"	
			params = {}
			options = {:params => params, :oauth_token => auth.token, :oauth_secret => auth.secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}
			folders_list = oauth_request(options)
			if (folders_list.parsed_response && folders_list.parsed_response.is_a?(Array))
				folders_list.push({"folder_id"=> "archive"}) 
				folders_list.push({"folder_id"=> "unread"}) 

				request_url = "https://www.instapaper.com/api/1.1/bookmarks/list"
				have_ids = auth.instapaper_have_ids || ""
				folders_list.each do |folder|
					params = {folder_id: folder["folder_id"], limit: 500, have: have_ids}	
					options = {:params => params, :oauth_token => auth.token, :oauth_secret => auth.secret, :request_url => request_url, :oauth_consumer_key => consumer_key, :secret_key => secret_key, :request_method => request_method}	
					instapaper_response = oauth_request(options)
					if instapaper_response 
						bookmarks_list = JSON.parse(instapaper_response)['bookmarks']
					end 

					if bookmarks_list && bookmarks_list.length > 0
						user.articles_processed = 0
						user.articles_to_process = bookmarks_list.length
						bookmarks_list.each do |bookmark|
							if bookmark['progress'] > 0	
								unless bookmark['private_source'] && bookmark['private_source'].length > 0
									url = Article.clean_url(bookmark['url'])
									article = Article.where(url: url).first_or_initialize
									if article.new_record?
								        title = bookmark['title'].truncate(255)
								        encoded_url = URI.encode(url)
								        begin 
									        domain = URI.parse(encoded_url).host
											cleaned_domain = PublicSuffix.parse(domain).domain.downcase
										rescue StandardError => e 
											cleaned_domain = nil
										end 
										
										bookmark_id = bookmark['bookmark_id']

										if bookmark['description'] && bookmark['description'].length > 0
								        	excerpt = bookmark['description'] 
								        else 
								        	excerpt = nil
								        end 
								        
								   
							      		article.update_attributes(title: title, domain: cleaned_domain, excerpt: excerpt)
							      		Resque.enqueue(ArticleParser, {"parser"=> "instapaper", "bookmark_id"=> bookmark_id, "article_id" => article.id, "oauth_token" => auth.token, "oauth_secret" => auth.secret})
							      	end 

							      	reading_source = "Instapaper"
									time_added = Time.at(bookmark['time'].to_i) #  Time is either when it was saved to instapaper if it's in "Home" or when it was archived if it's in "Archive." 

							        time_read = Time.at(bookmark['progress_timestamp'].to_i)
							        time_read = Time.now if time_read > Time.now 
							        article_progress = bookmark['progress'].to_f  
									user.reading_logs.where(article: article).first_or_create(logged_flag: true, :time_added => time_added, :time_read => time_read, reading_source: reading_source, article_progress: article_progress)
								end 

							    if have_ids.length == 0 
							        have_ids = "#{bookmark['bookmark_id']}:#{bookmark['hash']}" 
							    else 
							        have_ids.concat(",#{bookmark['bookmark_id']}:#{bookmark['hash']}")
							    end 
							        
						    end 
						    user.articles_processed += 1 
						end 
					else 
						user.articles_to_process = 0 
					end 
				end 
		    	auth.update_attributes(instapaper_have_ids: have_ids)
		    end 
		    user.articles_to_process = 0
		    user.articles_processed = 0
 		rescue StandardError=>e
 			Rails.logger.error("User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}")
	 		e.backtrace.each {|e| Rails.logger.error("#{e}")}
	 		user.articles_to_process = 0
	 		user.articles_processed = 0
		end
	end 
end 