# class YoutubeUpdater
# 	@queue = :apis_update
	
# 	def self.perform(user_id)
# 		#### Look up proper authorization token for YouTube for this User and time since last update
# 		user = User.find_by_id(user_id)
# 		begin 
# 	    	auth = user.authorizations.find_by_provider('youtube')
# 			#### Update access token using the refresh token 
# 			auth.refresh_google_token
		    
# 		    time_youtube_called = Time.now
# 			index = 1
# 			## Notes: videos that have been suspended no longer show duration or category so need to be accounted for
# 			auth = user.authorizations.find_by_provider('youtube')

# 			catch (:done) do 
# 	          	while index < 1000 do
# 	            	historyFeed = HTTParty.get('https://gdata.youtube.com/feeds/api/users/default/watch_history', {query: {v: 2, alt: 'json', access_token: auth.token, 'max-results' => 50, 'start-index' => index}})
# 		            if historyFeed['feed']['entry']
# 		              	historyFeed['feed']['entry'].each do |entry|
# 				            if auth.time_data_updated
# 				                throw :done if user.video_logs.where(:time_watched => entry['published']['$t']).size > 0
# 				    		end   
# 			                begin
# 				                ## VIDEO TABLE DATA PULL
# 				                title = entry['title']['$t']
# 				                url = entry['link'][0]['href']
# 				                category = entry['category'][0]['label']
# 				                publisher = entry['media$group']['media$credit'][0]['yt$display']
				                
# 				                source_video_id = entry['media$group']['yt$videoid']['$t']
# 				                source = 'Youtube'
			                	
# 			                	## VIDEO LOGS DATA PULL 
# 								time_watched = entry['published']['$t']
# 								if entry['app$control'] 
# 									puts "Video #{source_video_id} missing information for #{auth.name} #{auth.provider}"
# 									puts entry['app$control']
# 								end 
# 								length = entry['media$group']['yt$duration']['seconds'].to_i
# 								thumbnail = entry['media$group']['media$thumbnail'][0]['url']
# 				                description = entry['media$group']['media$description']['$t']
# 							rescue StandardError=>e
# 								puts e 
# 							end 
			                  
# 			                ## VIDEO AND VIDEO LOGS DATA ENTRY  
# 		                    video = Video.where(source_video_id: source_video_id).first_or_create
# 		                    video.update_attributes(category: category, publisher: publisher, thumbnail: thumbnail, description: description, source: source, length: length, title: title, url: url)

# 		                    log = user.video_logs.where(video: video).first_or_create(:video => video)
# 		                    log.update_attributes(:time_watched => time_watched)
# 		          		end 
# 		  	       		index += 50
# 		            else
# 		            	### Put an end to it if we've run out of videos to log before getting to 1000 
# 		              auth.update_attributes(time_data_updated: time_youtube_called)
# 		              throw :done 
# 		            end   
# 		        end 
# 		        ### Put an end to it if we've logged 1000 videos
# 		        auth.update_attributes(time_data_updated: time_youtube_called)
# 		        throw :done 
# 		    end 
# 		    $redis.set("user_#{user.id}_youtube_data_updated", "true")
# 		rescue StandardError=>e
# 			$redis.set("user_#{user.id}_youtube_data_updated", "true")
# 			puts "User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}"
# 		end 
# 	end
# end 