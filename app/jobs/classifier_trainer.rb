class ClassifierTrainer
    require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
	@queue = :low
    @lock_after_execution_period = 170
    
    @retry_limit = 3
    @retry_delay = 180

	def self.perform(article_id, topic_tag)
       	topics_classifier = StuffClassifier::Bayes.new("user_generated_topic_tags", :storage => StuffClassifier::RedisStorage.new("user_generated_topic_tags"))
        article = Article.find(article_id)
        training_words = article.classifier_words
        topics_classifier.train(topic_tag, training_words)
        topics_classifier.save_state
	end 
end 