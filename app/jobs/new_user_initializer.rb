class NewUserInitializer
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    
    @retry_limit = 5
    @retry_delay = 10
	
	def self.perform(user_id)
		user = User.find(user_id)
		user.set_mixpanel 
		user.first_mixpanel_event
		user.first_heap_event
		user.new_user_emails
		user.subscribe_to_mailchimp
	end
end