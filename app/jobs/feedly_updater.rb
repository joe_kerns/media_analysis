# class FeedlyUpdater
# 	@queue = :api_updating

# 	def self.perform(user_id)
# 		user = User.find(user_id)
# 		begin
# 			$redis.del("user_#{user.id}_feedly_data_updated")

# 			#### Look up proper authorization token for Feedly for this User and time since last update
		        
# 		        auth = user.authorizations.find_by_provider('feedly')
# 		        auth_header = "OAuth #{auth.token}"
# 		        if auth.time_data_updated
# 		          since = auth.time_data_updated.to_time.to_i * 1000
# 		        else 
# 		          since = 0
# 		        end 
				
# 	        ##### Get list of entries that have been read in the last 30 days the first time (when time_data_update is nil) and since last pull thereafter 
# 		        markers_url = 'https://sandbox.feedly.com/v3/markers/reads'
# 		        time_feedly_called = Time.now

# 				read_entries_list = HTTParty.get(markers_url, :query => {newerThan: since}, :headers => {'Authorization' => auth_header})
				
# 				#### Refresh token if necessary
# 				if read_entries_list['errorCode'] == 401 
# 					token_refresh_url = 'https://sandbox.feedly.com/v3/auth/token'
# 					refresh_token = auth.refresh_token
# 					client_id = ENV['FEEDLY_CONSUMER_KEY']
# 					client_secret = ENV['FEEDLY_SECRET_KEY']
# 					grant_type = "refresh_token"
# 					token_response = HTTParty.post(token_refresh_url, :body => {refresh_token: refresh_token, client_id: client_id, client_secret: client_secret, grant_type: grant_type})
# 					if token_response.code == 200
# 						token = token_response['access_token']
# 						auth.update_attributes(token: token)
# 						auth_header = "OAuth #{auth.token}"
# 						read_entries_list = HTTParty.get(markers_url, :query => {}, :headers => {'Authorization' => auth_header})
# 					end 
# 				end 
				
# 				##### Get the detail on each entry and store it in database 

# 				iterations = (read_entries_list['entries'].length.to_f / 1000.to_f).to_f.ceil
# 				i = 0
# 				while i < iterations 
# 					start = 0 
# 					stop = 999
# 					ids = read_entries_list['entries'][start..stop]
# 					entry_content_url = 'https://sandbox.feedly.com/v3/entries/.mget'
# 					entries = HTTParty.post(entry_content_url, :body => {:ids => ids}.to_json, :headers => {'Authorization' => auth_header, 'Content-Type' => 'application/json'})
					
# 					time_midpoint_since_update = Time.at(since + (time_feedly_called.to_time.to_i - since) / 2)

# 			        if entries
# 				        entries.each do |item|
# 							time_added = Time.at(item['crawled'].to_i/1000)  ### Converting from Miliseconds to seconds 
# 							time_read = [Time.at(entries[0]['published'].to_i/1000), time_midpoint_since_update].max ### We don't have a tag for when they read it so we assume all articles in this pull were read midway through the time period since the last pull OR the time the article was published if it happened later than the midpoint
# 							if item['origin']
# 								domain = URI.parse(item['origin']['htmlUrl']).host 
# 								cleaned_domain = PublicSuffix.parse(domain).domain
# 							end 
# 							publisher = item['origin']['title'] if item['origin']
# 							resolved_url = Article.clean_url(item['originId'])
# 							text = item['content']['content'] if item['content']
# 							calculated_words = text.split(" ").size.to_i
# 							cover_image_url = item['visual']['url'] if item['visual']
# 							excerpt = item['summary']['content'] if item['summary']
# 							title = item['title']
# 							source_categories = item['keywords']
# 							reading_source = 'Feedly'

# 							### Save the fact that article was read to the database 
# 							article = Article.where(url: resolved_url).first_or_create(title: title, source_categories: source_categories, publisher: publisher, cover_image_url: cover_image_url, excerpt: excerpt, word_count: calculated_words, domain: cleaned_domain)
# 			      			user.reading_logs.where(article: article).first_or_create(:article => article, logged_flag: true, :time_added => time_added, :time_read => time_read, reading_source: reading_source)
# 				        end 
# 			        end 

# 			        start += 1000
# 			        stop += 1000
# 			        i += 1 
# 			    end 
# 		        auth.update(time_data_updated: time_feedly_called) 
# 		        $redis.set("user_#{user.id}_feedly_data_updated", "true")
# 	 	rescue StandardError=>e
# 	 			$redis.set("user_#{user.id}_feedly_data_updated", "true")
# 	 			puts "User #{user.id} (#{user.first_name} #{user.last_name}) had their #{auth.provider} API update fail due to error: #{e}"
# 	 	end 
# 	end
# end 