class AllApiUpdater
    require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
	@queue = :high
    @lock_after_execution_period = 50
    
    @retry_limit = 1
    @retry_delay = 60

	def self.perform
		authorization_options = Authorization.authorization_options
        authorization_options.each do |provider|
        	Authorization.where(provider: provider[:provider]).each do |auth|
        		user_id = auth.user_id
        		job = Object.const_get(provider[:name] + "Updater")
        		Resque.enqueue(job, user_id)
        	end 
        end
	end 
end 