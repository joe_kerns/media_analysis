class MixpanelSetter
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :critical
    @lock_after_execution_period = 20
    
    @retry_limit = 3
    @retry_delay = 30

	def self.perform(user_id)
		user = User.find(user_id)
		user.set_mixpanel
        user.set_heap
        user.set_discover_score
	end
end