class ArticleQueuer
    require 'heroku_resque_autoscale'
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :low
    @lock_after_execution_period = 170
    
    @retry_limit = 1
    @retry_delay = 180

	def self.perform
        # CODE FOR THE FIRST TIME IT RUNS 
        if $redis.get('article_ids to be queued by diffbot')
            article_ids = JSON.parse($redis.get('article_ids to be queued by diffbot'))
        else 
            content_hash = Hash.new
            Article.find_each do |article|
              unless article.content.nil?
                content = article.content 
                content_hash[content] ||= {}
                content_hash[content][:article_ids] ||= []
                content_hash[content][:count] ||= 0 
                content_hash[content][:article_ids].push(article.id)
                content_hash[content][:count] += 1
              end 
            end 
            all_ids_array = Array.new
            content_hash.each do |content, hash|
              all_ids_array.concat(hash[:article_ids]) if hash[:count] > 1 
            end 
            duplicate_content_ids = all_ids_array.uniq
            article_ids = Article.where('id IN (?) OR content IS ? OR content = ? AND content != ?', duplicate_content_ids, nil, "", "Article text is not available").pluck(:id).uniq
            $redis.set('article_ids to be queued by diffbot', article_ids.to_json)
            $redis.del("DESC_offset")
            $redis.del("ASC_offset")
        end 
        
        order = $redis.get("diffbot_order") || "id DESC"
        if order == "id DESC"
            $redis.set("diffbot_order", "id ASC")
            offset = $redis.get("DESC_offset").to_i || 0
        else     
            $redis.set("diffbot_order", "id DESC")
            offset = $redis.get("ASC_offset").to_i || 0
        end 
        
        articles = Article.where('id IN (?)', article_ids).order(order).limit(50).offset(offset)
        Rails.logger.info("#{articles.size} articles processing in #{order} order and offset by #{offset}")
        if articles.size > 0 
            articles.each do |article|
                url = Article.clean_url(article.url)
                Resque.enqueue(ArticleParser, {"parser"=> "manual", "url"=> url, "article_id" => article.id})
            end 
            offset += 50
            if order == "id DESC"
                $redis.set("DESC_offset", offset)
            else 
                $redis.set("ASC_offset", offset)
            end
        else 
            $redis.del("DESC_offset")
            $redis.del("ASC_offset")
            $redis.del('article_ids to be queued by diffbot')
        end   
	end 
end 