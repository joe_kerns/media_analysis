class UserDestroyer
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :low
    
    @retry_limit = 3
    @retry_delay = 180
	
	def self.perform(user_id)
		user = User.find(user_id)
		ReadingLog.skip_callback(:destroy, :before, :queue_user_stats_cacher)
		user.destroy
	end 
end 