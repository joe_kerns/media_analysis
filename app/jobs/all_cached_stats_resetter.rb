class AllCachedStatsResetter
	require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
	@queue = :high
    @lock_after_execution_period = 50
    
    @retry_limit = 1
    @retry_delay = 60

	def self.perform
	    array_of_user_ids = User.pluck(:id).uniq
        array_of_user_ids.each { |user_id| Resque.enqueue(UserStatsCacher, user_id)}
	end 
end 