class KnowtifyDataSender
    require 'heroku_resque_autoscale'
    require 'utility/helper'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :low
    @lock_after_execution_period = 50
    
    @retry_limit = 3
    @retry_delay = 60
        # Resque.enqueue(KnowtifyDataSender)
	def self.perform
        help = Helper.new
        help.send_weekly_emails_data
	end 
end 