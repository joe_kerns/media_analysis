class MaintenanceWorker
    require 'heroku_resque_autoscale'
    require 'maintenance'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :low
    @lock_after_execution_period = 50
    
    @retry_limit = 3
    @retry_delay = 60
        # Resque.enqueue(MaintenanceWorker, {clean_urls: false, delete_non_articles: true, destroy_duplicate_articles: false, destroy_duplicate_logs: false, redo_topic_tags: false, fix_reading_logs_counter: false})
	def self.perform(options)
        maintenance = DatabaseMaintainer.new()
        maintenance.clean_urls if options["clean_urls"]
        maintenance.delete_non_articles if options["delete_non_articles"]
        maintenance.destroy_duplicate_articles if options["destroy_duplicate_articles"]
        maintenance.destroy_duplicate_logs if options["destroy_duplicate_logs"]
        maintenance.redo_topic_tags if options["redo_topic_tags"]
        maintenance.fix_reading_logs_counter if options["fix_reading_logs_counter"]
        maintenance.deal_with_unclaimed_profiles if options["deal_with_unclaimed_profiles"]
	end 
end 