class ArticleParser
    require 'heroku_resque_autoscale'
    include Resque::Plugins::UniqueJob
    extend HerokuAutoScaler::AutoScaling
    extend Resque::Plugins::Retry
    @queue = :high
    @lock_after_execution_period = 50
    
    @retry_limit = 3
    @retry_delay = 60

	def self.perform(options)
        article_id = options['article_id']
        parser = options['parser']   
        article = Article.find(article_id)

        begin
            if parser == "diffbot"
                url = options['url']
                parsed_article = Article.diffbot_parse(url)
            elsif parser == "instapaper"
                bookmark_id = options['bookmark_id']
                oauth_token = options['oauth_token']
                oauth_secret = options['oauth_secret']
                parsed_article = Article.instapaper_parse(bookmark_id, oauth_token, oauth_secret)
            elsif parser == "manual"
                url = options['url']
                parsed_article = Article.manual_parse(url)
            end 

            if parsed_article
                if article.check_for_duplicates_with_parsed_url(parsed_article['url'])
                    original_article = Article.where(url: parsed_article['url']).first
                    article.delete_self_in_favor_of_duplicate_article(original_article)
                    need_semantria = false 
                elsif parsed_article['url']
                    if Article.domains_to_ignore.any? { |domain| parsed_article['url'].include?(domain) }
                        need_semantria = false
                        article.destroy  
                    else 
                        need_semantria = true
                    end 
                elsif parsed_article['content']
                    need_semantria = true
                else 
                    need_semantria = false
                end 
                if need_semantria
                    article.update_from_parser(parsed_article)
                    article.queue_semantria
                end 
            else 
                if article.title.nil? && article.word_count.nil? 
                    destroyed_articles = JSON.parse($redis.get("diffbot_destroyed_articles"))
                    destroyed_articles.push(url)
                    $redis.set("diffbot_destroyed_articles", destroyed_articles.to_json)
                    article.destroy
                end 
            end 
        rescue StandardError => e
            Rails.logger.info("diffbot parser error: #{e} with #{article.id}") 
            e.backtrace.each {|error| Rails.logger.info("#{error}")}
            if article.title.nil? && article.word_count.nil? 
                destroyed_articles = JSON.parse($redis.get("diffbot_destroyed_articles"))
                destroyed_articles.push(url)
                $redis.set("diffbot_destroyed_articles", destroyed_articles.to_json)
                article.destroy
            end 
        end 
	end 
end 