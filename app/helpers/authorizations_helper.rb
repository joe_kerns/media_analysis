module AuthorizationsHelper
    def oauth_request(options)
		### Define required parameters & variables
            
            oauth_nonce = SecureRandom.hex
            oauth_timestamp = Time.now.to_i
            request_url = options[:request_url]
            encoded_url = properly_encode(request_url)
            oauth_token = options[:oauth_token].to_s
            oauth_secret = options[:oauth_secret].to_s
            oauth_consumer_key = options[:oauth_consumer_key] 
            secret_key = options[:secret_key]
            request_method = options[:request_method]
            oauth_callback = options[:oauth_callback] || ""

        ### Normalize request parameters (alphabetize and URL encode)
        	standard_params = {oauth_nonce: oauth_nonce, oauth_timestamp: oauth_timestamp, oauth_consumer_key: oauth_consumer_key, oauth_token: oauth_token, oauth_signature_method: 'HMAC-SHA1', oauth_version: '1.0', oauth_callback: oauth_callback}
        	params_hash = standard_params.merge(options[:params])
     
            alphabetized_params = params_hash.keys.sort
			param_string = ""
            
            alphabetized_params.each do |param|
                if param == :q && params_hash[param] == "@pocket OR pocket.co"
                    param_string << "&#{properly_encode(param.to_s)}=%40pocket%20OR%20pocket.co"
                else  
                    param_string << "&#{properly_encode(param.to_s)}=#{properly_encode(params_hash[param].to_s)}"
                end 
            end 
			
		### Generate OAuth Signature		
            signature_string = "#{request_method}&#{encoded_url}&" + properly_encode(param_string[1..-1])  ###skip the first part of the param_string because of extra & upfront 
            key = properly_encode(secret_key) + '&' + properly_encode(oauth_secret)
            oauth_signature = Base64.encode64("#{OpenSSL::HMAC.digest('sha1', key, signature_string)}")[0..-2]
            
        ### Define OAuth Authorization Header
            auth_header = "OAuth oauth_callback=\"#{properly_encode(oauth_callback)}\", oauth_consumer_key=\"#{properly_encode(oauth_consumer_key)}\", oauth_nonce=\"#{properly_encode(oauth_nonce)}\", oauth_signature_method=\"HMAC-SHA1\", oauth_signature=\"#{properly_encode(oauth_signature)}\", oauth_timestamp=\"#{oauth_timestamp}\", oauth_token=\"#{properly_encode(oauth_token)}\", oauth_version=\"#{properly_encode('1.0')}\""

        ##### Get the detail on each entry and store it in database 

        case request_method
        when 'GET'
            HTTParty.get(request_url, :query => options[:params], :headers => {'Authorization' => auth_header})
        when 'POST'
            HTTParty.post(request_url, :body => options[:params], :headers => {'Authorization' => auth_header})
        end 
    end

    def properly_encode(string)
        # Based on: https://github.com/oauth-xx/oauth-ruby/blob/master/lib/oauth/helper.rb
        # https://dev.twitter.com/oauth/overview/percent-encoding-parameters
        reserved_characters = OAuth::RESERVED_CHARACTERS
        URI::escape(string.to_s, reserved_characters)
    rescue ArgumentError
        URI::escape(string.to_s.force_encoding(Encoding::UTF_8), reserved_characters)    
    end 

end