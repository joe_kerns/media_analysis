class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :url
      t.integer :word_count
      t.string :source_categories, array:true
      t.timestamps
    end
  end
end
