class AddFavoriteToVideoLog < ActiveRecord::Migration
  def change
    add_column :video_logs, :favorite, :boolean
  end
end
