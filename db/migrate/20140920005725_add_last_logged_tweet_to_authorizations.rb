class AddLastLoggedTweetToAuthorizations < ActiveRecord::Migration
  def change
    add_column :authorizations, :last_logged_tweet, :integer
  end
end
