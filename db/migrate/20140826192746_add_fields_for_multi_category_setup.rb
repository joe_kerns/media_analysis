class AddFieldsForMultiCategorySetup < ActiveRecord::Migration
  def change
  	add_column :articles, :semantria_categories, :json
  	add_column :articles, :semantria_subcategories, :json
  	add_column :articles, :semantria_topics, :json
  	add_column :articles, :topic_tags, :string, array:true 
  	add_column :reading_logs, :topic_tags, :string, array:true
  	add_column :reading_logs, :user_modified_topics, :boolean, default:false
  end
end
