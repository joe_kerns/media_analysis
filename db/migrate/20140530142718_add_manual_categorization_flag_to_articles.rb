class AddManualCategorizationFlagToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :manual_categorization, :boolean
  end
end
