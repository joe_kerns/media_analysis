class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :title
      t.text :url
      t.string :category
      t.string :publisher
      t.string :source_video_id
      t.string :source
      t.integer :length

      t.timestamps
    end
  end
end