class AddMinutesPassedToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :minutes_passed, :float
  end
end
