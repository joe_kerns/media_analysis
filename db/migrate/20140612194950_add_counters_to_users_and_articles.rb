class AddCountersToUsersAndArticles < ActiveRecord::Migration
  def change
  	add_column :articles, :reading_logs_count, :integer, default: 0
  	Article.find_each do |article|
  		Article.reset_counters(article.id, :reading_logs)
  	end 
  	add_column :users, :reading_logs_count, :integer, default: 0
  	User.find_each do |user|
  		User.reset_counters(user.id, :reading_logs)
  	end 
  end
end
