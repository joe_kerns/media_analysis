class CreateReadingLogs < ActiveRecord::Migration
  def self.up
    create_table :reading_logs do |t|
      t.integer :article_id
      t.integer :user_id
      t.datetime :time_added
      t.datetime :time_read
      t.string :reading_source
    end
  end

  def self.down
    drop_table :reading_logs
  end
end
