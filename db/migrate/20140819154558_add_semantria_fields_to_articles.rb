class AddSemantriaFieldsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :person_entities, :string, array:true
    add_column :articles, :company_entities, :string, array:true
    add_column :articles, :product_entities, :string, array:true
    add_column :articles, :place_entities, :string, array:true
    add_column :articles, :job_title_entities, :string, array:true
    add_column :articles, :other_entities, :string, array:true
    add_column :articles, :summary, :text
  end
end
