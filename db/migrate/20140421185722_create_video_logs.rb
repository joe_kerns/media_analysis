class CreateVideoLogs < ActiveRecord::Migration
  def change
    create_table :video_logs do |t|
  		t.integer :video_id
  		t.integer :user_id
    	t.datetime :time_watched
    	t.timestamps
    end
  end
end
