class AddClaimedFlagToUsers < ActiveRecord::Migration
  def change
    add_column :users, :claimed_profile, :boolean, default:true
  end
end
