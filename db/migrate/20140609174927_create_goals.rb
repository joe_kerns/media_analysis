class CreateGoals < ActiveRecord::Migration
  def change
    enable_extension "hstore"
    create_table :goals do |t|
      t.string :category
      t.integer :target_words
      t.string :intensity
      t.hstore :associated_topics, array:true
      t.integer :user_id

      t.timestamps
    end
  end
end
