class AddTimeDataUpdatedToAuthorizations < ActiveRecord::Migration
  def change
    add_column :authorizations, :time_data_updated, :datetime
  end
end
