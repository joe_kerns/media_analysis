class AddPrivacyFlagToReadingLogs < ActiveRecord::Migration
  def change
    add_column :reading_logs, :hidden, :boolean, default: false
  end
end
