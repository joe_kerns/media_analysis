class DropIndustriesTable < ActiveRecord::Migration
  def change
    drop_table :industries
	remove_column :users, :industry_id
  end
end
