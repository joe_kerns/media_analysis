class AddFieldsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :excerpt, :text
    add_column :articles, :cover_image_url, :text
  end
end
