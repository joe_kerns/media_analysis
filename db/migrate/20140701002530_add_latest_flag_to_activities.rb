class AddLatestFlagToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :latest_flag, :boolean, default: true
  end
end
