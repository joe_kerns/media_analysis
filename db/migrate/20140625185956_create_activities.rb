class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.integer :user_id
      t.string :activity_type
      t.string :subject
      t.integer :activity_count
      t.integer :activity_word_count

      t.timestamps
    end
  end
end
