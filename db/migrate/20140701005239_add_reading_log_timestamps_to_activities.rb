class AddReadingLogTimestampsToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :first_log_read, :datetime
    add_column :activities, :last_log_read, :datetime
  end
end
