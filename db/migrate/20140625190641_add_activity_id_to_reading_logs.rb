class AddActivityIdToReadingLogs < ActiveRecord::Migration
  def change
    add_column :reading_logs, :activity_id, :integer
  end
end
