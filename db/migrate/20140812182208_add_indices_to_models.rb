class AddIndicesToModels < ActiveRecord::Migration
  def change
  	add_index :articles, :id
  	add_index :articles, :topic
  	add_index :articles, :url
  	
  	add_index :authorizations, :provider
  	
  	add_index :reading_logs, :article_id
  	add_index :reading_logs, :user_id
  	add_index :reading_logs, :time_read

  	add_index :users, :id
  end
end
