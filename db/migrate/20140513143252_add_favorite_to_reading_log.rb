class AddFavoriteToReadingLog < ActiveRecord::Migration
  def change
    add_column :reading_logs, :favorite, :boolean
  end
end
