class DropCategoriesAndRelevances < ActiveRecord::Migration
  def change
  	drop_table :categories
  	drop_table :category_relevances
  	drop_table :keywords
  end
end
