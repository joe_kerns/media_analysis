class AddTimestampsToReadingLogs < ActiveRecord::Migration
  def change
    add_column(:reading_logs, :created_at, :datetime)
    add_column(:reading_logs, :updated_at, :datetime)
  end
end
