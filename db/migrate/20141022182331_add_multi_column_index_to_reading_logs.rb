class AddMultiColumnIndexToReadingLogs < ActiveRecord::Migration
  def change
  	add_index :reading_logs, [:user_id, :time_read]
  	remove_index :articles, :topic
  	remove_index :articles, :id
  	remove_index :users, :discover_score
  	remove_index :users, :id

  end
end
