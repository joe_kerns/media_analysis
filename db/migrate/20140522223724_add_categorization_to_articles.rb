class AddCategorizationToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :topic, :string
    add_column :articles, :subtopic, :string
  end
end
