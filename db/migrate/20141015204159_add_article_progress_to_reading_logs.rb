class AddArticleProgressToReadingLogs < ActiveRecord::Migration
  def change
    add_column :reading_logs, :article_progress, :float
  end
end
