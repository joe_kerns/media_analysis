class AddPrivacyFlagsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :reading_archive_visible, :boolean, default: true
  end
end
