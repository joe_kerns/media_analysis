class AddCategorizedFlagToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :categorized_flag, :boolean, default: false
  end
end
