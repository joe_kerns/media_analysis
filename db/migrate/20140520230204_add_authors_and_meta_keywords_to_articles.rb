class AddAuthorsAndMetaKeywordsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :authors, :string, array:true
    add_column :articles, :meta_keywords, :text
  end
end
