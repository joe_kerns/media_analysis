class AddLoggedFlagToReadingLogs < ActiveRecord::Migration
  def change
    add_column :reading_logs, :logged_flag, :boolean
  end
end
