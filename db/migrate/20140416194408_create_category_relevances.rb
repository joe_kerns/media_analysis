class CreateCategoryRelevances < ActiveRecord::Migration
  def change
    create_table :category_relevances do |t|
      t.integer :article_id
      t.integer :category_id
      t.float :confidence_score
      t.boolean :confidence_flag, default: true
      t.timestamps
    end
  end
end
