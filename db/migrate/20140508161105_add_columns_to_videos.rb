class AddColumnsToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :description, :text
    add_column :videos, :thumbnail, :text
  end
end
