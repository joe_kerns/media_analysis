class AddDiscoverScoreToUsers < ActiveRecord::Migration
  def change
    add_column :users, :discover_score, :integer, default: 0
    add_column :users, :manual_discover_score, :boolean, default:false
  end
end
