class ChangeLastLoggedTweetFormatInAuthorizations < ActiveRecord::Migration
  def change
  	change_column :authorizations, :last_logged_tweet, :string
  end
end
