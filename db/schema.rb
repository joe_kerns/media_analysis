# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141022182331) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "fuzzystrmatch"
  enable_extension "hstore"
  enable_extension "pg_stat_statements"

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activities", force: true do |t|
    t.integer  "user_id"
    t.string   "activity_type"
    t.string   "subject"
    t.integer  "activity_count"
    t.integer  "activity_word_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "latest_flag",         default: true
    t.datetime "first_log_read"
    t.datetime "last_log_read"
    t.float    "minutes_passed"
  end

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.boolean  "god_mode",               default: false
    t.boolean  "reports_only",           default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  add_index "admins", ["unlock_token"], name: "index_admins_on_unlock_token", unique: true, using: :btree

  create_table "articles", force: true do |t|
    t.string   "title"
    t.text     "url"
    t.integer  "word_count"
    t.string   "source_categories",                       array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "publisher"
    t.string   "domain"
    t.boolean  "categorized_flag",        default: false
    t.text     "excerpt"
    t.text     "cover_image_url"
    t.string   "authors",                                 array: true
    t.text     "meta_keywords"
    t.string   "topic"
    t.string   "subtopic"
    t.boolean  "manual_categorization"
    t.integer  "reading_logs_count",      default: 0
    t.text     "content"
    t.string   "person_entities",                         array: true
    t.string   "company_entities",                        array: true
    t.string   "product_entities",                        array: true
    t.string   "place_entities",                          array: true
    t.string   "job_title_entities",                      array: true
    t.string   "other_entities",                          array: true
    t.text     "summary"
    t.json     "semantria_categories"
    t.json     "semantria_subcategories"
    t.json     "semantria_topics"
    t.string   "topic_tags",                              array: true
  end

  add_index "articles", ["url"], name: "index_articles_on_url", using: :btree

  create_table "authorizations", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.string   "token"
    t.string   "secret"
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "time_data_updated"
    t.string   "refresh_token"
    t.string   "last_logged_tweet"
    t.text     "instapaper_have_ids"
  end

  add_index "authorizations", ["provider"], name: "index_authorizations_on_provider", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_relevances", force: true do |t|
    t.integer  "article_id"
    t.integer  "category_id"
    t.float    "confidence_score"
    t.boolean  "confidence_flag",  default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "goals", force: true do |t|
    t.string   "category"
    t.integer  "target_words"
    t.string   "intensity"
    t.hstore   "associated_topics", array: true
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "keywords", force: true do |t|
    t.integer  "category_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "nicknames", force: true do |t|
    t.string   "name"
    t.integer  "nickname_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reading_logs", force: true do |t|
    t.integer  "article_id"
    t.integer  "user_id"
    t.datetime "time_added"
    t.datetime "time_read"
    t.string   "reading_source"
    t.boolean  "logged_flag"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "favorite"
    t.integer  "activity_id"
    t.boolean  "hidden",               default: false
    t.string   "topic_tags",                           array: true
    t.boolean  "user_modified_topics", default: false
    t.float    "article_progress"
  end

  add_index "reading_logs", ["article_id"], name: "index_reading_logs_on_article_id", using: :btree
  add_index "reading_logs", ["time_read"], name: "index_reading_logs_on_time_read", using: :btree
  add_index "reading_logs", ["user_id", "time_read"], name: "index_reading_logs_on_user_id_and_time_read", using: :btree
  add_index "reading_logs", ["user_id"], name: "index_reading_logs_on_user_id", using: :btree

  create_table "shares", force: true do |t|
    t.integer  "from_user_id"
    t.integer  "to_user_id"
    t.string   "to_email"
    t.integer  "article_id"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email",                   default: ""
    t.string   "encrypted_password",      default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "reading_logs_count",      default: 0
    t.boolean  "reading_archive_visible", default: true
    t.boolean  "claimed_profile",         default: true
    t.integer  "discover_score",          default: 0
    t.boolean  "manual_discover_score",   default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "video_logs", force: true do |t|
    t.integer  "video_id"
    t.integer  "user_id"
    t.datetime "time_watched"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "favorite"
  end

  create_table "videos", force: true do |t|
    t.string   "title"
    t.text     "url"
    t.string   "category"
    t.string   "publisher"
    t.string   "source_video_id"
    t.string   "source"
    t.integer  "length"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description"
    t.text     "thumbnail"
  end

end
