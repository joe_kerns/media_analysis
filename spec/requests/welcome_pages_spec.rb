require 'spec_helper'

describe "Basic pages" do

  subject { page }

  shared_examples_for "all pages" do
    it { should have_title('Roundview Alpha') }

    describe "should not show user nav links when signed out" do 
      let(:user) { FactoryGirl.create(:user) }
      before do 
        visit root_path
        sign_in(user)
        click_link "Sign out"
      end   
      
      it { should have_link('Home') }
      it { should have_link('About Us') }
      it { should have_link('Contact Us') }
      it { should_not have_content('My Profile')}
      it { should_not have_content('Connect APIs')}
      it { should_not have_link('Sign out')}      
    end
  end 

  describe "Home page" do
    let(:user) { FactoryGirl.create(:user) }
      it_behaves_like "all pages"
      before {visit root_path}
      it {should have_content('Hello, friends!')} 
  end

  describe "sign up page" do 
    before { visit new_user_registration_path }
    it { should have_content('Sign up') }
    it { should_not have_content('Sign in')}
  end 

  describe "sign up flow" do
    before {visit new_user_registration_path}
    let(:submit) { "Sign up" }       

    describe "with valid information" do
      
      before do
        fill_in "First name",   with: "Example"
        fill_in "Last name",   with: "User"
        fill_in "Email",        with: "user@example.com"
        fill_in "Password",     with: "password", match: :prefer_exact
        fill_in "user_password_confirmation", with: "password"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end
      
      describe "after saving the user" do
        before { click_button submit }
        let(:user) { User.find_by(email: 'user@example.com') }
        
        it { should have_link('Sign out') }
        it { should have_content('My Profile') }  
        it { should have_content('Connect APIs') }
      end
    end

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end
      describe "after submission" do
        before { click_button submit }
        it { should have_content('Sign up') }
        it { should have_content('error') }
      end 
    end
  end
end 