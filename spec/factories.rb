FactoryGirl.define do
  factory :user do
    sequence(:first_name)  { |n| "first_name#{n}" }
    sequence(:email) { |n| "person_#{n}@example.com"}
    password "password"
    password_confirmation "password"
  end

  factory :admin do
    sequence(:email) { |n| "admin_#{n}@example.com"}
    password "password"
    password_confirmation "password"
  end

end