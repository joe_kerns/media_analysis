(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
// mixpanel.init("268d63b0ab7bf94cf4ddb3e83c6b0a02");
 var productionHost = 'roundview.co';

  var devToken = '7bcde24dafc133a5ccb83a0f3d1be754'; var prodToken = '268d63b0ab7bf94cf4ddb3e83c6b0a02';

  // If the hostname is anything other than your production domain, initialize the Mixpanel library with your Development Token

  if (window.location.hostname.toLowerCase().search(productionHost) < 0) { 
    mixpanel.init(devToken); 
  } else { 
    mixpanel.init(prodToken); 
  }