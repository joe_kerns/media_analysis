require 'resque'
require 'resque-retry'
require 'resque-retry/server'

MediaAnalysis::Application.routes.draw do

  resources :shares

  match '/semantria/webhook', to: 'welcome#semantria', via: 'post'
  
  match '/api/users/authorizations', to: 'authorizations#user_list', via: 'get'
  
  ### Articles controller 
  match '/articles/topics', to: 'articles#topics', via: 'get'
  match '/articles/add_to_pocket', to: 'articles#add_to_pocket', via: 'post'


  ## ReadingLogs controller 
  match '/reading_logs/new', to: 'reading_logs#create', via:'post'
  match '/reading_logs/favorite', to: 'reading_logs#add_to_favorites', via: 'post'
  match '/reading_logs/hidden', to: 'reading_logs#mark_hidden', via: 'post'
  match '/reading_logs/:id', to: 'reading_logs#update', via: 'put'
  

  #Rails routes that generate views  
  devise_for :users,
      :controllers => {
        :omniauth_callbacks => "users/omniauth_callbacks",
        :registrations => "users/registrations",
        :sessions => "users/sessions"
      }
  authenticated :user do
    root :to => "welcome#dashboard", :as => "authenticated_root" 
  end
  
  root to: 'welcome#index'

  # match '/about', to: 'welcome#about',     via: 'get'
  # match '/privacy', to: 'welcome#privacy',     via: 'get'
  # match '/terms', to: 'welcome#terms',     via: 'get'
  match '/faqs', to: 'welcome#faqs', via: 'get'
  match '/deleted_account', to: 'welcome#goodbye', via:'get', as: 'deleted_account' 
  match '/chrome_extension_installed', to: "welcome#chrome_extension_installed", via: 'get'
  devise_for :admins, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  authenticate :admin do 
    mount Resque::Server, :at => "/resque", as: 'resque'
  end
  
  ## Third party Authorization API calls
  match '/users/auth/pocket', to: 'authorizations#pocket_auth', via: 'get'
  match '/authorizations/pocket_middle', to: 'authorizations#pocket_middle', via: 'get'
  match '/users/auth/readability', to: 'authorizations#readability_auth', via: 'get'
  match '/authorizations/readability_middle', to: 'authorizations#readability_middle', via: 'get'
  match '/users/auth/feedly', to: 'authorizations#feedly_auth', via: 'get'
  match '/authorizations/feedly/callback', to: 'authorizations#feedly_callback', via: 'get'
  match '/authorizations/deauthorize/:provider', to: 'authorizations#deauthorize', via: 'delete'
  match '/users/auth/instapaper', to: "authorizations#instapaper_login", via: 'get'
  match '/users/auth/instapaper', to: "authorizations#instapaper_authorization", via: 'post'


  devise_scope :user do 
    get '/api/current_user' => 'users/sessions#show_current_user', as: 'show_current_user'
    get '/extension_login' => 'users/users#extension_login', as: 'extension_login'
    post '/api/check/is_user' => 'users/users#is_user', as: 'is_user'
    match '/users/:id', to: 'users/users#show', via: 'get'
    match '/users/:id/articles', to: 'users/users#user_articles', via: 'get'
    match '/api/users/discover', to: 'users/users#discover', via: 'get'
    match '/api/users/summary/:id', to: 'users/users#summary', via: 'get'
    match '/api/users/stats/:id', to: 'users/users#stats', via: 'get'
    match '/api/users/data_update_status', to: 'users/users#data_update_status', via: 'get'
    match '/api/users/common_articles/:id', to: 'users/users#common_articles', via: 'get'
    match '/users', to: 'users/users#index', via: 'get', as: 'users'
    match '/email_registration', to: 'users/registrations#ask_for_email', via: 'get', as: 'ask_for_email'
    match '/add_email', to: 'users/registrations#add_email', via: 'patch', as: 'add_email'
  end

  get '*path' => redirect('/')

end
