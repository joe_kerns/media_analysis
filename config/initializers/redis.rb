if Rails.env.development?
	$redis = Redis.new(:host => '127.0.0.1', :port => 6379)
end 

if ENV["REDISCLOUD_URL"]
    $redis = Redis.new(:url => ENV["REDISCLOUD_URL"])
    Resque.redis = $redis
end
