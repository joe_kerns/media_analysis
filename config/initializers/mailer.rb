ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
:address => "smtp.gmail.com",
:port => 587,
:domain => "roundview.co",
:user_name => ENV['CONTACT_EMAIL'],
:password => ENV['CONTACT_EMAIL_PASSWORD'],
:authentication => "plain",
:enable_starttls_auto => true
}
