# README

Creating Admin 
```ruby
Admin.create(:email => 'tkalimov@gmail.com', :password => 'password', :god_mode => true, :reports_only => false)
```

==Importing production database
```bash
heroku pgbackups:capture --expire --app roundview4
curl -o latest.dump `heroku pgbackups:url --app roundview4`
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U timurkalimov -d production_db latest.dump
```

==Exporting database into production 
```bash 
PGPASSWORD= pg_dump -Fc --no-acl --no-owner -h localhost -U timurkalimov production_db > mydb.dump
heroku pgbackups:restore DATABASE_URL 'https://s3.amazonaws.com/roundview4/other/mydb.dump' --app roundview4
```

==Clear Stale Resque Workers
```ruby
Resque.workers.each {|w| w.unregister_worker}
```

==Migrate production database into staging 
```bash 
heroku pgbackups:capture --expire --remote production
heroku pgbackups:restore DATABASE `heroku pgbackups:url --remote production` --remote staging
```

==Pushing local branch to Staging master
```bash
git push staging branch-name:master
```


==Queueing Maintenance Worker 
```bash 
options = {clean_urls: false, delete_non_articles: false, destroy_duplicate_articles: false, destroy_duplicate_logs: true, redo_topic_tags: false, fix_reading_logs_counter: true, deal_with_unclaimed_profiles: true}
Resque.enqueue(MaintenanceWorker, options)
Resque.enqueue(MaintenanceWorker, {destroy_duplicate_articles: true, destroy_duplicate_logs: true, fix_reading_logs_counter: true})
```

WORKERS:
rake environment resque:workers COUNT=2 TERM_CHILD=1 QUEUE='*'
rake environment resque:work QUEUE='*'

SHUTDOWN REDIS:
redis-cli shutdown
