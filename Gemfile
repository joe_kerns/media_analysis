source 'https://rubygems.org'
ruby '2.1.0'

### General setup 
	gem 'rake', :require => 'rake'
	gem 'rails', '4.1.0' # Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
	gem 'devise'  #Authentication 
	gem 'figaro' #Manage sensitive information in the Rails app 
	gem 'angularjs-rails', '>=1.2.0.rc1' 
	gem 'ngmin-rails'
	gem 'sass-rails', '~> 4.0.0' # Use SCSS for stylesheets
	gem 'uglifier', '>= 1.3.0' # Use Uglifier as compressor for JavaScript assets
	gem 'coffee-rails', '~> 4.0.0' # Use CoffeeScript for .js.coffee assets and views
	gem 'jquery-rails' # Use jquery as the JavaScript library
	gem 'pg' # Use postgres as the database for Active Record
	gem 'httparty' #Make calls to external APIs 
	gem 'htmlentities'	
	gem "active_model_serializers", "~> 0.8.0"
	gem 'public_suffix'
	gem 'fuzzy_match'
	gem 'activeadmin', github: 'activeadmin'

# Use for file uploading / avatars 
	gem "paperclip", "~> 4.1"
	gem 'aws-sdk'
	
### Omniauth gems
	gem 'omniauth'
	gem 'omniauth-twitter'
	# gem 'omniauth-google-oauth2'
	# gem 'omniauth-vimeo'
	# gem 'omniauth-goodreads'
	gem 'omniauth-facebook'
	# gem "koala", "~> 1.10.0rc"

### Redis & Resque setup 
	gem 'resque', "~> 1.25.2", :require => 'resque/server'
	gem 'redis', '3.1.0'
	gem 'resque_mailer' #to send mail in background 
	gem 'resque_solo' #to avoid duplicate jobs being enqueued
	gem 'devise-async' #for asynchronous mailing of devise reset password email
	gem 'resque-retry' #to retry jobs that failed
	gem 'platform-api'  #For autoscaling

### Searching for nicknames
	gem 'nickname', :git => 'git://github.com/bsimpson/Nickname.git'

### Naive bayes classifier
	# gem 'stuff-classifier', :path => "/Users/timurkalimov/Documents/rails_projects/stuff-classifier"
	gem 'stuff-classifier', :git => 'git://github.com/tkalimov/stuff-classifier.git'

### Use premailer to style the emails
	gem 'premailer-rails'
	gem 'mailchimp-api', '~> 2.0.6'

### Parsing
	gem 'mechanize'
	gem "ruby-readability", :require => 'readability'
	gem 'fastimage'


group :development do
	gem 'debugger'
	gem 'relateiq'
	gem 'mixpanel_client'
end 

group :test do
  gem 'selenium-webdriver', '2.41.0'
  gem 'capybara', '2.2.1'	
  gem 'factory_girl_rails', '4.4.1'
  # gem 'cucumber-rails', '1.4.0', :require => false
  gem 'database_cleaner', '1.2.0'
  gem 'rspec-rails', '2.14.2'
  gem 'guard-rspec', '4.2.9'
  gem 'spork-rails', '4.0.0'
  gem 'guard-spork', '1.5.0'
  gem 'childprocess', '0.5.3'
  gem 'growl', '1.0.3'
end


group :production do
	gem 'rails_12factor'  # Heroku specific deployment
	gem 'unicorn'  # Use unicorn as the app server
	
	### Analytics
	gem 'mixpanel-ruby'
	gem 'newrelic_rpm'
end